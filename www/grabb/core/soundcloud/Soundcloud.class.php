<?php

class SoundCloud  extends Video {

	function __construct($url) {
		$this->videoUrl = $url;
		$this->getStreamData($this->videoUrl);
	}

	private function  curl_http_get($url, $ssl = false) {
		$ch = curl_init($url);
		$headers = array(
			"User-Agent: curl/7.16.3 (i686-pc-cygwin) libcurl/7.16.3 OpenSSL/0.9.8h zlib/1.2.3 libssh2/0.15-CVS",
			"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
			"Accept-Language: en-us;q=0.5,en;q=0.3",
			"Keep-Alive: 115",
			"Connection: keep-alive"
		);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		if ($ssl) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		}
		$response = curl_exec($ch);
		$response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if($response_code != 200 && $response_code != 302 && $response_code != 304) {
			$response = false;
		}
		return $response;
	}

	public function getStreamData($url) {
		$return_value = false;
		$valid_url_pattern = '%\b(?:(?:http|https)://|www\.)[\d.a-z-]+\.[a-z]{2,6}(?::\d{1,5}+)?(?:/[!$\'()*+,._a-z-]++){0,10}(?:/[!$\'()*+,._a-z-]+)?(?:\?[!$&\'()*+,.=_a-z-]*)?%i';
		
		if(preg_match($valid_url_pattern, $url)) {
			//echo "so intrat in preg march";
			$scheme = parse_url($url, PHP_URL_SCHEME);
			if($scheme == "https") {
				$url = str_replace("http","http", $url);
			}
			$url = str_replace("https://m.", "http://", $url);
			$url = str_replace("http://m.", "http://", $url);

			$response = self::curl_http_get($url, false);

			$dom = new DOMDocument();
			@$dom->loadHTML($response);
			$xpath = new DOMXPath($dom); 
			//parsam imaginea
			$tags = $xpath->query('//img[@itemprop="image"]');
			$this->image = $tags->item(0)->attributes->item(0)->value;
				
			$return_value = array();
			if($response !== false) {
				//find anything between streamUrl":" and ",
				if(preg_match('/\"title\"\:\"(.*?)\"\,/', $response, $title)) {
					$this->videoTitle = $title[1];
				}

				if(preg_match('/\"username\"\:\"(.*?)\"\,/', $response, $title)) {
					$this->uploader = $title[1];
				}

				if(preg_match('/\"playback_count\"\:(.*?)\,/', $response, $seen)) {
					$this->seen = $seen[1];
				}

				if(preg_match('/\"duration\"\:(.*?)\,/', $response, $title)) {
					$this->duration = gmdate("i:s", "$title[1]");

				}

				if(preg_match('/stream_url\"\:\"(.*?)\"\,/', $response, $stream)) {
					$url = $stream[1]."?client_id=b22c81cc6ddd2ebe31f98439c25e9bea";
					$size = get_size_301($url);
					$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=audio/mp3&token='.$url;	
					$this->streamUrl = array("Audio" => array("url" => $msg, "size" => $size, "type"=> "audio/mp3"));
				}
			}
		} else {
			echo "Not valid match";
		}
		
	}

}
