<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
include "../inc/util.inc.php";

$lang = 'en';
$site = $_GET['site'];

switch ($lang) {
	case 'en':
		$fullpage = file_get_contents('../resource/english.xml');
		break;

	case 'vi':
		$fullpage = file_get_contents('../resource/vietnam.xml');
		break;

	case 'es':
		$fullpage = file_get_contents('../resource/spain.xml');
		break;

	case 'it':
		$fullpage = file_get_contents('../resource/italian.xml');
		break;

	case 'ro':
		$fullpage = file_get_contents('../resource/romana.xml');
		break;

	case 'ru':
		$fullpage = file_get_contents('../resource/rusa.xml');
		break;

	
	default:
		$fullpage = file_get_contents('../resource/english.xml');
		break;
}


$strings = simplexml_load_string($fullpage); //incarcam stringurile traduse


//lastest download
if ($site != "" )
	$sql = "SELECT * FROM `downloads` WHERE `genre` = 0 and `site` = '$site' ORDER BY id DESC LIMIT 0,100;";
else 
	$sql = "SELECT * FROM `downloads` WHERE `genre` = 0  ORDER BY id DESC LIMIT 0,100;";

if(!$result = $db->query($sql)){
die('There was an error running the query [' . $db->error . ']');
}

while($row = $result->fetch_assoc()) {  
$minutes = round((time() - strtotime($row['date'])) / 62.2);

if ($minutes == 0) {
  $time = $strings->strings->lessThenMinuteAgo;
} else

if ($minutes == 1) {
  $time = $strings->strings->aboutAMinuteAgo;
} else

if (($minutes > 1) && ($minutes < 60)) {
  $time = $minutes .$strings->strings->minutesAgo;
} else 

if ($minutes > 60) {
  $minutes = round($minutes / 60);
  if ($minutes == 1) {
    $time = $strings->strings->aboutAHourAgo;
  } else 

  if ($minutes > 1) {
    $time = $strings->strings->about." ".$minutes . " " .$strings->strings->hoursAgo;
  }
}   


?>
<div class="col-md-3 col-sm-6">
<div class="product-item">
    <div class="item-thumb">
        <div class="overlay">
            <div class="overlay-inner" onclick='download("<?=$row['link'];?>")'>
                <a href="#" class="view-detail"><?=$strings->buttons->download;?></a>
            </div>
        </div> <!-- /.overlay -->
        <img src="<?=$row['image'];?>" alt=""/>
    </div> <!-- /.item-thumb -->
    <div class="ellipsis">
       	<a onclick='download("<?=$row['link'];?>")'>                                
            <?=trim($row['title']);?>
        </a>
    </div> <!--/ellipsis-->
    <span><i class="fa fa-eye"></i> <em class=""><?=$row['seen'];?></em> - <i class="fa fa-download"></i><em class="price"><?=$row['counts'];?></em></span>
</div> <!-- /.product-item -->
</div> <!--/col-md-2 col-xs-6-->

<?php
$time = "";
}//end while


?>