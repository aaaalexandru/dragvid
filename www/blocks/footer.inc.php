 
    <div class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <span>
                    	Copyright &copy; 2015 <a href="#">dragvid.com</a>
                        
                    </span>

                </div> <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-6">
                    <ul class="social">
                        <li><a href="#" class="fa fa-facebook"></a></li>
                        <li><a href="#" class="fa fa-twitter"></a></li>
                        <li><a href="#" class="fa fa-instagram"></a></li>
                        <li><a href="#" class="fa fa-linkedin"></a></li>
                        <li><a href="#" class="fa fa-rss"></a></li>
                    </ul>
                </div> <!-- /.col-md-6 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.site-footer -->
</div> <!--/#wrapper-->


    <script src="/js/vendor/jquery-1.10.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
    <script src="/js/jquery.easing-1.3.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/plugins.js"></script>
    <script src="/js/main.js"></script>
    <script src="/owl-carousel/owl.carousel.js"></script>

    <style>
    #owl-demo .item{
        margin: 3px;
    }
    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }
    </style>


    <script>
        $(document).ready(function() {
          $("#owl-demo").owlCarousel({
            autoPlay: false,
            items : 6,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,3],
            scrollPerPage: true,
            slideSpeed: 1000,
            navigation: true,
            pagination: true
          });
           $("#popular-demo").owlCarousel({
            autoPlay: false,
            items : 6,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,3],
            scrollPerPage: true,
            navigation: true,
            slideSpeed: 1000
          });
           $("#latest-demo").owlCarousel({
            autoPlay: false,
            items : 6,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,3],
            scrollPerPage: true,
            navigation: true,
            slideSpeed: 1000

          });
             
           
        });
    </script>
    <script>
    
    $(document).ready(function () {
        $('ul.sites-list > li').click(function (e) {
            e.preventDefault();
            $('ul.sites-list > li').removeClass('active');
            $(this).addClass('active');                
        }); 
        
    });

</script>

</body>

</html>