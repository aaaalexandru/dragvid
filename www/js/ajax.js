   $(document).ready(function() {
            changeTip();
        });

        function download(link) {
            document.getElementById("videoLink").value = link;       
            document.getElementById("downloadForm").submit();
        }

      function changeTip(){
         var url = window.location.href;
         var res = url.split("/",5);
         //alert(res[4]);
         if(res[4] == 'music' || res[3] == 'music') {
            getDownloadedMusic('');
         }
         else if(res[4] == 'video' || res[3] == 'video'){
            getDownloaded('');
         }
      }

        function getDownloaded(site) {
            $.ajax({
              url: "/ajax/getDownloadedUrls.php",
              data: {"site":site},
              context: document.body
            }).done(function(data) {
                $("#response").html(data);
            });
        }

        function getDownloadedMusic(site) {
            $.ajax({
              url: "/ajax/getDownloadedMusic.php",
              data: {"site":site},
              context: document.body
            }).done(function(data) {
                $("#response").html(data);
            });
        }
