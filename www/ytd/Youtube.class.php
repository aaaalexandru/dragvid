<?php

class Youtube extends Video {
	protected $videoId;

	function __construct($url) {
		$this->videoUrl = $url;
		$this->videoId = $this->getId();
		$this->getVideoData();
	}

	function getVideoData() {
		$this->image = "http://i1.ytimg.com/vi/".$this->videoId."/0.jpg";
		$my_video_info = 'http://www.youtube.com/get_video_info?&video_id='. $this->videoId.'&asv=3&el=detailpage&hl=en_US'; //video details fix *1
		$my_video_info = (curlGet($my_video_info));

		$thumbnail_url = $title = $url_encoded_fmt_stream_map = $type = $url = '';
		parse_str($my_video_info, $ar);	

		if (!isset($ar['title'])) {
			trigger_error("Error parsing youtube", E_USER_ERROR);
		}

		$this->videoTitle = $ar['title'];
		$this->uploader = $ar['author'];
		$this->seen = $ar['view_count'];
		$this->uploaded = date("Y-M-d H:i", $ar['timestamp']);
		$this->duration = gmdate("i:s", $ar['length_seconds']);
		$this->description = $ar['keywords'];
		$videowithaudio = $ar['url_encoded_fmt_stream_map'];
		$videowithaudio = explode(",", $videowithaudio);
		foreach ($videowithaudio as $key => $value) {
			parse_str($value, $prs);

			$sizeOfFile = get_size($prs['url']);

			if ($sizeOfFile == 0) {
				$sizeOfFile = get_size_301($prs['url']);
			} 

			// if ($sizeOfFile == 0) {
			// 	$sizeOfFile = get_size($prs['url']);
			// }
			// if ($sizeOfFile == 0) {
			// 	$sizeOfFile = urldecode(get_size($prs['url']));
			// }

			$type = explode(";", $prs['type']);
			$audio = 0;
			if (is_array($type) && (count($type) > 1))
				$audio = count(explode(".", $type[1]));
			$msg = '../download.php?title='.urlencode($this->videoTitle).'&size='.$sizeOfFile.'&mime='.$type[0].'&token='.urlencode($prs['url']);
			$key = '';

				
  
			switch($prs['itag']) {
				case '5':
					$key = "240p";
				break;

				case '17':
					$key = "144p";
				break;

				case '18':
					$key = "360p";
				break;

				case '22':
					$key = "720p";
				break;

				case '34':
					$key = "360p FLV";
				break;

				case '35':
					$key = "480p";
				break;

				case '36':
					$key = "240p 3GP";
				break;

				case '37':
					$key = "1080p";
				break;

				case '38':
					$key = "Original Definition";
				break;

				case '43':
					$key = "360p WebM";
				break;

				case '44':
					$key = "480 WebM";
				break;

				case '45':
					$key = "720p WebM";
				break;
   

				case '46':
					$key = "1080p WebM";
				break;

				case '82':
					$key = "360p MP4 3D";
				break;

				case '84':
					$key = "720p MP4 3D";
				break;

				case '100':
					$key = "360p WebM 3D";
				break;

				case '102':
					$key = "720p WebM 3D";
				break;
			}
			if ($key == '')
				$key = $prs['quality'];
				
			$arra[$key] = array("url" => $msg,
								"type" => $type[0],
								"size" => $sizeOfFile);
		} 			
		
		//printout($videowithaudio);


		$adaptive = $ar['adaptive_fmts'];	
		//printout($ar);

		$formats = explode(",", $adaptive);
		//printout($formats);
		

		

		foreach ($formats as $key => $value) {

			parse_str($value, $prs);
			//printout($prs);
			$sizeOfFile = get_size($prs['url']);

			if ($sizeOfFile == 0) {
				$sizeOfFile = get_size_301($prs['url']);
			}

			// if ($sizeOfFile == 0) {
			// 	$sizeOfFile = urldecode(get_size($prs['url']));
			// }


			$type = explode(";", $prs['type']);
			$audio = count(explode(".", $type[1]));
			$msg = '../download.php?title='.urlencode($this->videoTitle).'&size='.$sizeOfFile.'&mime='.$type[0].'&token='.urlencode($prs['url']);
			
			if (isset($prs['size']))
				$key = $prs['size'];
			else
				$key = $type[0];

			if (!preg_match("/audio/", $key))
				$key .= "(no audio)";

				$arra[$key] = array("url" => $msg,
									"type" => $type[0],
									"size" => $sizeOfFile);
						
		}

		//printout($aray);

		
		

		
		$this->streamUrl = $arra;
		//printout($this->streamUrl);
	}


	function formatBytes($bytes, $precision = 2) { 
	    $units = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'); 
	    $bytes = max($bytes, 0); 
	    $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
	    $pow = min($pow, count($units) - 1); 
	    $bytes /= pow(1024, $pow);
	    return round($bytes, $precision) . '' . $units[$pow]; 
	} 
	function is_chrome(){
		$agent=$_SERVER['HTTP_USER_AGENT'];
		if( preg_match("/like\sGecko\)\sChrome\//", $agent) ){	// if user agent is google chrome
			if(!strstr($agent, 'Iron')) // but not Iron
				return true;
		}
		return false;	// if isn't chrome return false
	}

	function getId()  {
		 $my_id = NULL;
		 $url = parse_url($this->videoUrl);
                if( is_array($url) && count($url)>0 && isset($url['query']) && !empty($url['query']) ){
                        $parts = explode('&',$url['query']);
                        if( is_array($parts) && count($parts) > 0 ){
                                foreach( $parts as $p ){
                                        $pattern = '/^v\=/';
                                        if( preg_match($pattern, $p) ){
                                                $my_id = preg_replace($pattern,'',$p);
                                                break;
                                        }
                                }
                        }
                        // if( !$my_id ){
                        //         echo '<p>No video id passed in</p>';
                        //         exit;
                        // }
                }
         return $my_id;
	}

}