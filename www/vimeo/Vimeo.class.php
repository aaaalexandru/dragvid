<?php
/**
* This class helps to get video data by ID from Vimeo and print it on a page
* Created by Mila Daricheva
*/

include "inc/simple_html_dom.php";

class Vimeo extends Video
{
	private
		$apiLink,   // A Vimeo API link, an example is vimeo.com/api/v2/video/VIDEOID.xml
		$videoData, // A SimpleXMLElement Object created from XML which was pulled from Vimeo using API link  
		$DESCRIPTION_LENGTH = 300; // The length of description property
				
	public 		
		$videoXml,
		$videoLink = "https://vimeo.com/",
		$configUrl,
		$configVideo,
		$mobileLinke,
		$sdLink,
		$hdLink;
		
	// Constructor of the class
	function __construct($url){
		$this->videoUrl = $url;
		$id = explode("/", $url);
		$videoId = 0;

		if ( isset($id[5]) && is_numeric($id[5]))  {
			$videoId = $id[5];
		} else {
			foreach ($id as $value) {
				if (is_numeric($value)) {
					$videoId = $value;
				}
			}
		}
		
		$this->apiLink = 'http://vimeo.com/api/v2/video/' . $videoId . '.xml';
		$this->getData();
		$this->videoLink .= $videoId;
		$this->getVideo();
	}
  
	// cURL helps to get data from Vimeo
	private function getData() {
		$curl = curl_init($this->apiLink);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		$this->videoData = simplexml_load_string(curl_exec($curl));
		$this->description = $this->videoData->video->description;
		$this->uploaded = $this->videoData->video->upload_date;
		$this->uploader = $this->videoData->video->user_name;
		$this->seen = $this->videoData->video->stats_number_of_plays;
		$this->duration = gmdate("i:s", $this->videoData->video->duration);
		curl_close($curl);
	}

	private function getVideo() {
		//extragem pagina videoului
		$curl = curl_init($this->videoLink);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		$videoXml = curl_exec($curl);
		curl_close($curl);		
		//cautam in ea config url
		$html = str_get_html($videoXml);
		$configUrl = $html->find("div[class=player js-player]",0)->getAttribute('data-config-url');
		$configUrl = str_replace("&amp;","",$configUrl); //stergem &amp; din link
		
		//cautam linkurile de download la vidos
		$curl = curl_init($configUrl);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		$configVideo = curl_exec($curl);
		
		//echo preg_match("s/^.*'hd':{(.*?)}.*$/$1/g",$configVideo, $result);
		//print_r($result);
		$arr = ((array)json_decode($configVideo));
		$arr = (array) $arr['request']->files->h264;
		$this->videoTitle = $this->getVideoProperty('title');
		$this->image = $this->getVideoProperty('thumbnail_medium');
		foreach ($arr as $key => $value) {
			$size = get_size($value->url);
			$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/mp4&token='.urlencode($value->url);	
			$this->streamUrl[$key] = array("url" => $msg, "type" => "video/mp4", "size" => $size);
		}
		//$this->streamUrl = (array) $arr['request']->files->h264;

		

		if (isset($arr->request->files->h264->mobile->url)) {
			$this->mobileLink = $arr->request->files->h264->mobile->url;			
		}
		if (isset($arr->request->files->h264->sd->url)) {
			$this->sdLink = $arr->request->files->h264->sd->url;
		}
		if (isset($arr->request->files->h264->hd->url)) {
			$this->hdLink = $arr->request->files->h264->hd->url;
		}
	}
	
	// Gets a video property by name
	public function getVideoProperty($videoProperty) {
		return $this->videoData->video->$videoProperty;
	}
	
	// Trancates a description to be shorter than the DESCRIPTION_LENGTH
	public function getShortDescription() {
		return preg_replace('/\s+?(\S+)?$/', '', substr($this->getVideoProperty('description'), 0, $this->DESCRIPTION_LENGTH)) . '...';
	}

	
	
	// Prints all properties of a video
	public function printAllProperties() {
		print '<pre>';
		print_r($this->videoData->video);
		print '</pre>';
	}
	
	// Prints video data in a specific layout 
	public function printVideoBlock() {	
		print	'<section class="videoBlock">
					<a class="thumb" target="_blank" href="' . $this->getVideoProperty('url') . '">
						<img alt="' . $this->getVideoProperty('title') . '" src="' . $this->getVideoProperty('thumbnail_medium') . '" />
					</a>	
					<div class="description">
						<h2>' . $this->getVideoProperty('title') . '</h2>
						<p>' . $this->getShortDescription() . '</p>
						<p><a target="_blank" href="' . $this->getVideoProperty('url') . '">Watch on Vimeo</a></p>
					</div>	
				</section>';
	}

}
