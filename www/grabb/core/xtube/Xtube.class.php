<?php

class Xtube extends Video {
	
	function __construct($url) {
		$this->videoUrl = $url;
		$this->getVideoData();
	}

	function getVideoData() {
		$fullpage = curlGet($this->videoUrl);
		
		$dom = new DOMDocument();		
		@$dom->loadHTML($fullpage);
		$xpath = new DOMXPath($dom); 

		$tags = $xpath->query('//div[@class="sectionNoStyleHeader"]');
		$this->videoTitle = trim($tags->item(0)->firstChild->nodeValue); 
		$this->uploader = trim($tags->item(0)->childNodes->item(1)->nodeValue); 
		$this->uploader = trim(str_replace("By:", "", $this->uploader));
		
		$tags = $xpath->query('//div[@class="sectionHeader"]/p');
		$this->uploaded = $tags->item(0)->lastChild->nodeValue;

		$this->duration = $tags->item(1)->lastChild->nodeValue;
		$this->seen = $tags->item(2)->lastChild->nodeValue;

		$tags = $xpath->query('//p[@class="fieldsDesc"]');
		$this->description = $tags->item(0)->nodeValue;
		
		$tags = $xpath->query('//script[@type="text/javascript"]');

		$flashvars720 = get_js_string_var("flashvars.quality_720p", $tags->item(0)->ownerDocument->textContent);
		$flashvars480 = get_js_string_var("flashvars.quality_480p", $tags->item(0)->ownerDocument->textContent);
		$flashvars240 = get_js_string_var("flashvars.quality_240p", $tags->item(0)->ownerDocument->textContent);
		$flashvars180 = get_js_string_var("flashvars.quality_180p", $tags->item(0)->ownerDocument->textContent);
		$flashvarsVideoUrl = get_js_string_var("flashvars.video_url", $tags->item(0)->ownerDocument->textContent);

		if ($flashvarsVideoUrl != "") {
			$url = urldecode($flashvarsVideoUrl);
			$size = get_size($url);
			$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/mp4&token='.urlencode($url);
			$this->streamUrl['default'] = array("url" => $msg,
										"size" => $size,
										"type" => "video/mp4");
		}

		if ($flashvars720 != "") {
			$url = urldecode($flashvars720);
			$size = get_size($url);
			$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/mp4&token='.urlencode($url);
			$this->streamUrl['780p'] = array("url" => $msg,
										"size" => $size,
										"type" => "video/mp4");
		}

		if ($flashvars480 != "") {
			$url = urldecode($flashvars480);
			$size = get_size($url);
			$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/mp4&token='.urlencode($url);
			$this->streamUrl['480p'] = array("url" => $msg,
										"size" => $size,
										"type" => "video/mp4");
		}

		if ($flashvars240 != "") {
			$url = urldecode($flashvars240);
			$size = get_size($url);
			$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/mp4&token='.urlencode($url);
			$this->streamUrl['240p'] = array("url" => $msg,
										"size" => $size,
										"type" => "video/mp4");
		}

		if ($flashvars240 != "") {
			$url = urldecode($flashvars180);
			$size = get_size($url);
			$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/mp4&token='.urlencode($url);
			$this->streamUrl['180p'] = array("url" => $msg,
										"size" => $size,
										"type" => "video/mp4");
		}

		$tags = $xpath->query('//textarea/iframe');
		
		$iframepage = curlGet($tags->item(0)->getAttribute("src"));
		$thumb = substr($iframepage, strpos($iframepage, "flashvars.image_url =", 1000));
		$thumb = strstr($thumb, "\";", true);
		$thumb = urldecode(str_replace("flashvars.image_url = \"", "", $thumb));
		$this->image = $thumb;

		// $tags = $xpath->query('//embed[@id="flash-player-embed"]');
		// $this->image = $tags->item(0)->getAttribute("flashvars");

		// parse_str($this->image, $prs);
		// $this->image = $prs['url_bigthumb'];
		// $this->streamUrl['SD'] = array("url" => $prs['flv_url'],
		// 								"size" => get_size($prs['flv_url']),
		// 								"type" => "video/flv");

	}

}