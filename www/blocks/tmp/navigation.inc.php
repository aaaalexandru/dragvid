  	<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
<script>
window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return t;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
</script>
  	<div class="site-header">
		<div class="main-navigation">
			<div class="responsive_menu">
				<ul>
					<li><a class="show-1 templatemo_home" href="/<?=$lang;?>/about"><?=$strings->menu->about;?></a></li>
					<li><a class="show-2 templatemo_page2" href="/<?=$lang;?>/sites"><?=$strings->menu->sites;?></a></li>
					<!-- <li><a class="show-3 templatemo_page3" href="#">Services</a></li> -->
					<li><a class="show-5 templatemo_page5" href="/<?=$lang;?>/contacts"><?=$strings->menu->contact;?></a></li>
					<li>
                           		<a href="#" style="">
				                 	<div class="hex_footer">
									<span class="fa fa-facebook"></span>
									</div>
				                </a>
				          </li>
				          <li> 			            
				                <a href="#" style="">
				                    <div class="hex_footer">
									 <span class="fa fa-twitter"></span>
									</div>
				                </a>
                           	</li>
				</ul>
				<ul>
		            <li><a class="show-1 templatemo_home" href="/vi"><img src="/images/vn.png"/></a></li>

		            <li><a class="show-2 templatemo_page2" href="/en"><img src="/images/us.png"/></a></li>
		        </ul>
			</div>
			<div class="container">
				<div class="row templatemo_gallerygap">
					<div class="col-md-12 responsive-menu">
						<a href="#" class="menu-toggle-btn">
				            <i class="fa fa-bars"></i>
				        </a>
					</div> <!-- /.col-md-12 -->
                    <div class="col-md-3 col-sm-12">
                    	<a href="/<?=$lang;?>">
                    	<!-- <span class="logo">
                    		<span>O</span>ur<span>V</span>id
                    	</span> -->
                    	 <img src="/images/logo2.jpg" alt="OURVID" style='width:287px;margin-top:25px;'> 
                    	</a>
                    </div>
					<div class="col-md-7 main_menu" >
						<ul>
							<li><a class="show-1 templatemo_home" href="/<?=$lang;?>/about" id="home">
                            	<!-- <span class="fa fa-camera"></span> -->
                                <?=$strings->menu->about;?></a></li>
					<!-- 		<li><a class="show-2 templatemo_page2" href="/<?=$lang;?>/sites">
                            	<span class="fa fa-users"></span>
                          		   <?=$strings->menu->sites;?></a></li> -->
							<li><a class="show-3 templatemo_page3" href="/<?=$lang;?>/sites" id="sites">
                            	<!-- <span class="fa fa-cogs"></span> -->
                            	<?=$strings->menu->sites;?></a></li>

                           	<li>
                           		<a href="#" style="padding-right:0px; padding-left:0px">
				                 	<div class="hex_footer">
									<span class="fa fa-facebook"></span>
									</div>
				                </a>
				            </li>
				            <li style="padding-right:30px; padding-left:10px">
				                <a href="#" style="padding-right:0px; padding-left:0px">
				                    <div class="hex_footer">
									 <span class="fa fa-twitter"></span>
									</div>
				                </a>
                           	</li>

                           	<!-- <li class="show-3 templatemo_page3" style="width:115px; height:101px; ">
                            	<div style="width:115px; ">

	                                 <div class="fb-like" data-href="http://cristi.optimal.md/" data-layout="button_count" 
	                                 data-action="like" data-show-faces="true" data-share="false"></div>
	                            	 <a id="tweet" class="twitter-share-button" href="https://twitter.com/share">Tweet</a>
	                            </div>
	                        </li> -->
                            	
							<!-- <li style="background-color:#343536; width:124px; height:100px;">
                            	
                            	<div id="socialPlugin">
	                                 <div class="fb-like" data-href="http://cristi.optimal.md/" data-layout="button_count" 
	                                 data-action="like" data-show-faces="true" data-share="false"></div>
	                            	 <a class="twitter-share-button" href="https://twitter.com/share">Tweet</a>
	                            </div>
                            
                            </li> -->

						</ul>
					</div> <!-- /.col-md-12 -->

					<div class="col-md-2 main_menu">
			            <!-- <ul class="lang">
			              <li><a href="#">Ro</a><br>
			                  <a href="#">En</a>
			              </li>
			            </ul> -->
			            <ul class="lang">
			              <li>
				              <a class="show-1 templatemo_home" id="langItem" href="/vi">
				              	<img src="/images/vn.png"/>
				              </a>
			              </li>
			              <li>
				              <a class="show-1 templatemo_home" id="langItem" href="/es">
				              	<img src="/images/es.png"/>
				              </a>
			              </li>
			              <li>
				              <a class="show-2 templatemo_page2" id="langItem" href="/en">
				              	<img src="/images/us.png"/>
				              </a>
			              </li>
			            </ul>
			          </div>

				</div> <!-- /.row -->
			</div> <!-- /.container -->
		</div> <!-- /.main-navigation -->
	</div> <!-- /.site-header -->