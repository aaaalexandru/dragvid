<?php

class Video {

	protected $videoUrl;
	protected $streamUrl;
	protected $videoTitle;
	protected $image;
	protected $size;
	protected $description;
	protected $likes;

	protected $duration;
	protected $uploader;
	protected $uploaded;
	protected $seen;

	function getTitle() {
		return $this->videoTitle;
	}

	function getVideoUrl() {
		return $this->videoUrl;
	}

	function getStream() {
		return $this->streamUrl;
	}

	function getImage() {
		return $this->image;
	}

	function getSize() {
		return $this->size;
	}

	function getDescription() {
		return $this->description;
	}

	function getLikes() {
		return $this->likes;
	}

	function getDuration() {
		return $this->duration;
	}

	function getUploader() {
		return $this->uploader;
	}

	function getUploaded() {
		return $this->uploaded;
	}
	
	function getSeen() {
		return $this->seen;
	}
}



?>