<?php

class Twitch extends Video {

	protected $id;
	protected $char;
	protected $apiUrl;

	function __construct($url) {
		$this->videoUrl = $url;
		preg_match("/.*?(\\d+)/is", $url, $match);
		$id = $match[1];
		preg_match("/"."\/([a-z]{1})\/"."/is", $url, $match);
		$character = $match[1];
		$this->id = $id;
		$this->char = $character;
		$url = "https://api.twitch.tv/api/videos/$character$id?as3=t";		
		
		$this->apiUrl = $url;

		$this->getVideoData();
	}

	function getVideoData() {
		$fullpage = curlGet($this->apiUrl);
		$json = json_decode($fullpage);	
		//printout($json);
		$this->uploader = $json->channel;
		$this->duration = $json->duration;	
		$character = $this->char;
		$id = $this->id;
		if (!isset($json->chunks)) {
			foreach (range('a', 'z') as $c){
		        $fullpage = curlGet("https://api.twitch.tv/api/videos/$c$id?as3=t");
		        $json = json_decode($fullpage);
		        $character = $c;
		        if (isset($json->chunks)) break; //am gasit litera
		    }  
		}
		if (isset($json->preview))
			$this->image = $json->preview;

		$arr = (array)$json->chunks->live;
		$size = get_size($arr[0]->url);
		foreach ($arr as $key => $value) {
			$this->streamUrl['Part '.($key+1)] = 
			array("url" => $value->url, 
				"size" => $size, 
				"type" => "video/flv");		
		}
		
		$fullpage = curlGet("https://api.twitch.tv/kraken/videos/$character$id");
		$json = json_decode($fullpage);
		$this->description = $json->description;
		$this->videoTitle = $json->title;
		//$this->streamUrl = $arr;


	}

}


?>
