<?php
//load info sites
switch ($lang) {
  case 'en':
    $page = file_get_contents('resource/sitesEnglish.xml');
    break;

  case 'vi':
    $page = file_get_contents('resource/sitesVietnam.xml');
    break;

  case 'es':
    $page = file_get_contents('resource/sitesSpain.xml');
    break;

  case 'ro':
    $page = file_get_contents('resource/sitesRomanian.xml');
    break;

  case 'it':
    $page = file_get_contents('resource/sitesIt.xml');
    break;

  case 'ru':
    $page = file_get_contents('resource/sitesRussian.xml');
    break;
  
}


$sitesInfo = simplexml_load_string($page);

$siteInfo = "";

foreach ($sitesInfo->sites->site as $key => $value) {
  if (strpos($value->id, $module)) {
    $siteInfo = $value;
    break;
  }
}
//echo "test-----------------------------------------------------------4";

if ($siteInfo == "") {
  foreach ($sitesInfo->adultSites->site as $key => $value) {
    if (strpos($value->id, $module)) {
      $siteInfo = $value;
      break;
    }
  }
}

?>
  
    <div id="products" class="content-section container">
      <div class="row">
        <div class="col-md-12 sites">
          <div class="col-md-9">
            <h2 class="siteName"><?=$siteInfo->name;?></h2>
            <p class="descriereSites"><?=$siteInfo->intro;?></p>
          </div> <!--/col-sm-7-->
          <div class="col-md-3 logo-site-img">
            <img class="alignImg" src="/images<?=$siteInfo->id;?>.png" width="200">
          </div> <!--/col-sm-5-->
        </div>
        
        <?php
          foreach ($siteInfo->examples->example as $key => $value) {
            //printout($value);
          
        ?>

        <div class="col-md-12 exemple">
          
          <div class="col-md-1">
            <p><?=$value->name;?></p>
          </div> <!--/col-sm-11-->
          <div class="col-md-11">
            <p onclick='download("<?=$value->link;?>")'><?=$value->model;?><br>
              e.g. <a href="#" ><?=$value->link;?></a></p>  
          </div>
          
        
          <?php
            } // end foreach
          ?>
          <script>
              function download(link) {
                  document.getElementById("videoLink").value = link;       
                  document.getElementById("downloadForm").submit();
              }
          </script>

        </div> <!--/col-md-12-->
        
      
      
        <?php 
        if ($lang == 'en') {
        ?>
        
          <div class="col-md-12 steps">
            <h2 class="download-steps"><?=$strings->strings->htdwn;?> <?=$siteInfo->name;?>?</h2>
            <ol>
              <li>Choose any video you like</li>
              <li>Paste the link to the input on this page</li>
              <li>Select a format</li>
              <li>Wait until the video will be downloaded</li>
              <li> Play in your favorite player. If it doesn't work, use VLC </li>
            </ol>
          </div>
        
        <?php } ?>

         <?php 
        if ($lang == 'es') {
        ?>
        <div class="col-md-12">
          <div class="col-md-7 steps">
            <h2><?=$strings->strings->htdwn;?> <?=$siteInfo->name;?>?</h2>
            <ol>
              <li>Elija cualquier vídeo que te gusta</li>
              <li> Pegue el link para la entrada en esta página</li>
              <li>Seleccione un formato</li>
              <li>Espere hasta que se descargará el video</li>
              <li>Juega en tu reproductor favorito. Si esto no funciona, utilice VLC</li>
            </ol>
          </div>
        </div>
        <?php } ?>

        <?php 
        if ($lang == 'ro') {
        ?>
        <div class="col-md-12">
          <div class="col-md-7 steps">
            <h2><?=$strings->strings->htdwn;?> <?=$siteInfo->name;?>?</h2>
            <ol>
              <li>Alege video-ul care îți place</li>
              <li>Lipește URL-ul în câmpul pentru text</li>
              <li>Selectează un format</li>
              <li>Așteaptă ca video-ul să fie descărcat</li>
              <li>Folosește playerul tău preferat. Dacă nu merge, folosește VLC</li>
            </ol>
          </div>
        </div>
        <?php } ?>
        <?php 
        if ($lang == 'ru') {
        ?>
        <div class="col-md-12">
          <div class="col-md-7 steps">
            <h2><?=$strings->strings->htdwn;?> <?=$siteInfo->name;?>?</h2>
            <ol>
              <li>Выберите любое понравившее видео</li>
              <li>Вставьте ссылку на вход на этой странице</li>
              <li>Выберите формат</li>
              <li>Подождите, пока видеоданные будут загружены</li>
              <li>Проигрывание в вашем любимом плейлисте. Если это не работает, используйте VLC</li>
            </ol>
          </div>
        </div>
        <?php } ?>
        <?php 
        if ($lang == 'it') {
        ?>
        <div class="col-md-12">
          <div class="col-md-7 steps">
            <h2><?=$strings->strings->htdwn;?> <?=$siteInfo->name;?>?</h2>
            <ol>
              <li>Scegli qualsiasi video che ti piace</li>
              <li>Incolla il URL nel campo di testo</li>
              <li>Seleziona un formato</li>
              <li>Aspetta fino che il video verrà scaricato</li>
              <li>Riproduci nel tuo player preferito. Se non funziona, utilizza VLC</li>
            </ol>
          </div>
        </div>
        <?php } ?>
      </div> <!--/.row-->
    </div> <!--/#products-->

        
    
