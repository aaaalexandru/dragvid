<?php

class MetaCafe extends Video {
	

	public function __construct($videoUrl) {
		$this->videoUrl = $videoUrl;
		$this->streamUrl = $this->getVideoVars($videoUrl);		
	}

	function getVideoVars($url) {		
		$fullpage = curlGet($url);
		$dom = new DOMDocument();

		@$dom->loadHTML($fullpage);
		$xpath = new DOMXPath($dom); 
		//parsam imaginea
		$tags = $xpath->query('//meta[@property="og:image"]');
		$this->image = $tags->item(0)->attributes->item(1)->value;
		$tags = $xpath->query('//div[@id="Description"]/p');
		$this->description = $tags->item(0)->nodeValue;
		$tags = $xpath->query('//param[@id="flashVars"]');
		$flashVars = "";
		foreach ($tags as $tag) {
			$flashVars .= (trim($tag->getAttribute("value")));	
		}

		parse_str($flashVars, $vars);
		$arr = (array)json_decode($vars['mediaData']);	
		$title = $xpath->query('//hgroup[@id="ItemTitle"]');
		$title = $title->item(0)->nodeValue;
		$this->videoTitle = str_replace("[HD]", "", $title);
		foreach ($arr as $key => $value) {
			$size = get_size($value->mediaURL);
			$msg = '../download.php?title='.base64_encode(trim($this->videoTitle)).'&size='.$size.'&mime=video/mp4&token='.($value->mediaURL);
			$arr[$key] = array("url"=>$msg,
								"size" => $size,
								"type" => "video/mp4"
								);
		}
		return $arr;
	}

	function getVideo($url) {
		$videoData = curl_http_get($url);
		return $videoData;
	}






}

?>