<div id="wrapper">
 <div class="container download-block">
    <h1><?= $strings->strings->paste;?><br></h1>
    <div class="download-box">
        <form id="downloadForm" action="/<?=$lang ?>/download" method="POST" class="download-form">
            <input id="videoLink" name="link" type="text" placeholder="ex.https://youtube.com/watch?v=gxgvXeJ-PO0"/><button type="submit"><?= $strings->buttons->download;?> </button>
        </form>
    </div>
</div>