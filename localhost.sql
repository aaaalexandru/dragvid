-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 06, 2015 at 10:44 AM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `downloader`
--

-- --------------------------------------------------------

--
-- Table structure for table `api_keys`
--

CREATE TABLE IF NOT EXISTS `api_keys` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `secret` varchar(32) COLLATE utf8_bin NOT NULL,
  `key` varchar(32) COLLATE utf8_bin NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dumping data for table `api_keys`
--

INSERT INTO `api_keys` (`id`, `secret`, `key`, `name`) VALUES
(1, '28e336ac6c9423d946ba02d19c6a2631', '8996913fea2755b0b41302ac8d0bf0b0', 'android downloader');

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE IF NOT EXISTS `downloads` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `site` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `genre` int(1) NOT NULL DEFAULT '0' COMMENT '0 - normal video; 1 - only audio format; 2 - porno;',
  `link` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `counts` int(9) DEFAULT NULL,
  `seen` int(9) NOT NULL,
  `duration` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=303 ;

--
-- Dumping data for table `downloads`
--

INSERT INTO `downloads` (`id`, `site`, `genre`, `link`, `title`, `image`, `date`, `counts`, `seen`, `duration`) VALUES
(5, 'ted', 0, 'http://ted.com/talks/eddy_cartaya_my_glacier_cave_discoveries.html', 'My glacier cave discoveries', 'http://img.tedcdn.com/r/images.ted.com/images/ted/04121e1f2bce7f63597f27238ba13e1ac72fa146_1600x1200.jpg?quality=89&w=600', '2015-07-02 13:22:17', 20, 0, '0'),
(13, 'instagram', 0, 'https://instagram.com/p/fd2upnlRTd/', 'Biz Stone on Instagram: â€œMonster Jake (after watching Monster University).â€', 'https://igcdn-photos-a-a.akamaihd.net/hphotos-ak-xfp1/t51.2885-15/1389833_453392234769528_1653514253_n.jpg', '2015-07-06 10:15:50', 34, 0, '0'),
(21, 'twitch', 0, 'https://api.twitch.tv/api/videos/c3477778?as3=t', 'WPC - Ace Dota 2 League - Winner&#39;s Celebration', 'http://static-cdn.jtvnw.net/jtv.thumbs/archive-491782980-320x240.jpg', '2015-07-06 09:32:27', 18, 0, '0'),
(25, 'vimeo', 0, 'https://vimeo.com/channels/staffpicks/123564408', 'We&#39;re Gonna Be Lords', 'http://i.vimeocdn.com/video/512946836_200x150.jpg', '2015-07-06 09:31:56', 10, 120634, '0'),
(29, 'twitter', 0, 'https://twitter.com/twitter/status/560072509211410432', 'Twitter on Twitter', 'https://o.twimg.com/2/proxy.jpg?t=HBhXaHR0cHM6Ly9hbXAudHdpbWcuY29tL3Byb2QvZGVmYXVsdC8yMDE1LzAxLzI3LzAyL2I0YTYyNzNmLXVwbG9hZGVkdmlkZW9fcG9zdGVyLTYwMDAuanBnFMAHFJwEABYAEgA&amp;amp;s=QUl2goeHDUICd2aJtlm__PZ5l_x4Qkpt7n0bySS0Raw', '2015-07-06 09:32:17', 12, 0, '0'),
(56, 'facebook', 0, 'https://www.facebook.com/photo.php?v=10150710575260184', 'thank you for being a part of this community!', 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xaf1/v/t1.0-1/c1.0.32.32/p32x32/24164_373264832819_2980826_n.jpg?oh=5638f38573c62ad7ed632826edc94e0f&amp;oe=55E27624&amp;__gda__=1436435049_25b2ad6096719bce69cfb8bc680e699d', '2015-07-06 09:31:37', 38, 0, '0'),
(59, 'soundcloud', 1, 'https://soundcloud.com/killer1014/eminem-without-me', 'Eminem - Without Me', 'https://i1.sndcdn.com/artworks-000015684337-kzwmxu-t500x500.jpg', '2015-04-23 12:04:43', 32, 0, '0'),
(60, 'vevo', 0, 'http://www.vevo.com/watch/miley-cyrus/wrecking-ball/USRV81300400', 'Wrecking Ball', 'http://img.cache.vevo.com/Content/VevoImages/video/1F2BCF2B2ED5FC2F25412FAAB2C4DE6F201369114643878.jpg', '2015-07-02 13:22:02', 9, 0, '0'),
(61, 'vine', 0, 'https://vine.co/v/h2IeidnLbai', 'First time in snow', 'https://v.cdn.vine.co/r/thumbs/353AD30B261023717595121373184_16717e87698.4.5.10205179286726413017.mp4_eJ2ANEjV.AIj_7Xc4raflJCs1kBe1DT64eMrPvyolWPglZ2b9NLzdWQCYSfW4BSt.jpg?versionId=MLgJ5XpHmu3tOFyx8kUNVx86yjYC7gOZ', '2015-07-06 09:32:05', 18, 0, '0'),
(67, 'blip', 0, 'http://blip.tv/the-unemployed-life/sound-of-music-live-season-3-ep-48-6699937', 'Sound of Music Live - Season 3 Ep. 48', 'http://a.images.blip.tv/Kevincomedy-SoundOfMusicLiveSeason3Ep48728-814.jpg', '2015-07-06 09:32:37', 45, 0, '0'),
(71, 'collegehumor', 0, 'http://collegehumor.com/video/6942530/google-is-going-to-blackmail-you', 'Google is Going to Blackmail You', 'http://2.media.collegehumor.cvcdn.com/77/34/2c758c08f3f47d2824e9e3096784f49f-google-is-going-to-blackmail-you.jpg', '2015-07-06 09:32:48', 18, 0, '0'),
(76, 'vk', 0, 'https://vk.com/video-77521_169220528', 'Noize MC / 10 Ð»ÐµÑ‚: Ð®Ð±Ð¸Ð»ÐµÐ¹Ð½Ñ‹Ð¹ ÐºÐ¾Ð½Ñ†ÐµÑ€Ñ‚ / TEASER', 'https://pp.vk.me/c541200/u882151/video/l_12fd811b.jpg', '2015-07-02 13:22:34', 13, 0, '0'),
(87, 'mixcloud', 1, 'http://www.mixcloud.com/TechnoLiveSets/sashaofficial-last-night-on-earth-nye-2013-02-academy-brixton-london-31-12-2013/', '@sashaofficial - Last Night On Earth, NYE 2013, 02 Academy Brixton, London - 31-12-2013', 'https://images-mix.netdna-ssl.com/w/600/h/600/q/85/upload/images/extaudio/343a7cd3-9fc0-456f-a4cf-9e4f6af7239a.jpg', '2015-07-06 09:33:29', 24, 7, '0'),
(89, 'twitch', 0, 'https://api.twitch.tv/api/videos/3477778?as3=t', 'WPC - Ace Dota 2 League - Winner&#39;s Celebration', 'http://static-cdn.jtvnw.net/jtv.thumbs/archive-491782980-320x240.jpg', '2015-04-10 17:52:38', 2, 0, '0'),
(91, 'youtube', 0, 'https://www.youtube.com/watch?v=VjHMDlAPMUw', 'Lost Frequencies - Are You With Me (Official Music Video)', 'http://i1.ytimg.com/vi/VjHMDlAPMUw/0.jpg', '2015-04-17 20:40:37', 8, 0, '0'),
(92, 'youtube', 0, 'https://www.youtube.com/watch?v=M6t47RI4bns', 'Big Sean - Blessings (Explicit) ft. Drake, Kanye West', 'http://i1.ytimg.com/vi/M6t47RI4bns/0.jpg', '2015-04-15 09:30:33', 2, 0, '0'),
(102, 'youtube', 0, 'https://www.youtube.com/watch?v=NvyVlUHtCaU', 'ChiÈ™inÄƒu, eu te iubesc / ÐšÐ¸ÑˆÐ¸Ð½Ñ‘Ð², Ñ Ð»ÑŽÐ±Ð»ÑŽ Ñ‚ÐµÐ±Ñ / Chisinau, i love you.', 'http://i1.ytimg.com/vi/NvyVlUHtCaU/0.jpg', '2015-04-17 15:44:37', 5, 0, '0'),
(119, 'xhamster', 2, 'http://xhamster.com/movies/2620763/fiona_amateur_filipino_teen_deep_throat_specialist_expert.html', 'Fiona Amateur Filipino Teen Deep Throat Specialist Expert', 'http://et06.xhcdn.com/t/763/320/6_2620763.jpg', '2015-04-15 15:09:37', 36, 0, '0'),
(127, 'xtube', 2, 'http://www.xtube.com/watch.php?v=duWif-S690-', 'SAMANTHA COLOMBIANA SEXY WHASAP 3127811513 TWITER@hotchicabe', 'http://i8.cdn1.videothumbs.xtube.com/m=eaAaaEFb/videos/201406/02/duWif-S690-/original/1.jpg', '2015-04-17 20:25:58', 19, 0, '0'),
(128, 'xtube', 2, 'http://www.xtube.com/watch.php?v=sdV0i-S600-#.VS9j2uSlilM', 'Old Goddess Spring Lingerie Striptease (part1)', 'http://i2.cdn1.videothumbs.xtube.com/m=eaAaaEFb/videos/201504/13/sdV0i-S600-/original/12.jpg', '2015-04-16 10:26:21', 1, 0, '0'),
(129, 'xtube', 2, 'http://www.xtube.com/watch.php?v=YHBFq-S690-#.VS9kKOSlilM', 'SAMANTHA COLOMBIANA SEXY WHASA 0573127811513 TWITER@hotchica', 'http://i8.cdn1.videothumbs.xtube.com/m=eaAaaEFb/videos/201406/02/YHBFq-S690-/original/1.jpg', '2015-04-16 10:26:59', 2, 0, '0'),
(130, 'redtube', 2, 'http://www.redtube.com/300842', 'Seka & Friends', 'http://img.l3.cdn.redtubefiles.com/_thumbs/0000300/0300842/0300842_007m.jpg', '2015-04-16 10:28:00', 2, 0, '0'),
(131, 'redtube', 2, 'http://www.redtube.com/467011', 'wax that ass 4', 'http://img.l3.cdn.redtubefiles.com/_thumbs/0000467/0467011/0467011_015m.jpg', '2015-04-16 10:27:47', 1, 0, '0'),
(132, 'redtube', 2, 'http://www.redtube.com/623933', 'Classic GB', 'http://img.l3.cdn.redtubefiles.com/_thumbs/0000623/0623933/0623933_014m.jpg', '2015-04-16 10:28:14', 1, 0, '0'),
(133, 'xvideos', 2, 'http://www.xvideos.com/video10725771/xvideos.com_8fce3090be9f3ff7054e125fe61d468c', 'xvideos.com 8fce3090be9f3ff7054e125fe61d468c - 28 min', 'http://img-l3.xvideos.com/videos/thumbslll/2d/95/59/2d95591c115a621a13eb98aaaccbb961/2d95591c115a621a13eb98aaaccbb961.10.jpg', '2015-04-16 10:37:06', 11, 0, '0'),
(134, 'xvideos', 2, 'http://www.xvideos.com/video6120357/big_tit_handjob_slut', 'Big tit handjob slut - 5 min', 'http://img-l3.xvideos.com/videos/thumbslll/ee/4d/47/ee4d473e00811cae9636a9ee35bb18ac/ee4d473e00811cae9636a9ee35bb18ac.27.jpg', '2015-04-16 10:37:56', 2, 0, '0'),
(135, 'xvideos', 2, 'http://www.xvideos.com/video5993200/amateur_slut_handjob', 'Amateur slut handjob - 5 min', 'http://img-l3.xvideos.com/videos/thumbslll/59/76/b9/5976b947a05c1b461aa23c96660b937e/5976b947a05c1b461aa23c96660b937e.15.jpg', '2015-04-16 10:38:50', 2, 0, '0'),
(136, 'xvideos', 2, 'http://www.xvideos.com/video6621943/handjob_amateur_slut', 'Handjob amateur slut - 5 min', 'http://img-l3.xvideos.com/videos/thumbslll/1c/d5/f6/1cd5f62347c745e0f60306807b3d660f/1cd5f62347c745e0f60306807b3d660f.27.jpg', '2015-04-16 10:38:44', 2, 0, '0'),
(137, 'xvideos', 2, 'http://www.xvideos.com/video6001336/big_tit_handjob_slut', 'Big tit handjob slut ', 'http://img-l3.xvideos.com/videos/thumbslll/aa/c8/8c/aac88c7c7c26d2bdebc5d3b3d8a59898/aac88c7c7c26d2bdebc5d3b3d8a59898.10.jpg', '2015-04-16 10:42:18', 3, 0, '0'),
(138, 'redtube', 2, 'http://www.redtube.com/589287', 'Girls, into gangbang & cum swallowing 80', 'http://img.l3.cdn.redtubefiles.com/_thumbs/0000589/0589287/0589287_012m.jpg', '2015-04-16 10:39:11', 1, 0, '0'),
(139, 'xhamster', 2, 'http://xhamster.com/movies/2800693/my_kinky_love.html', 'My Kinky Love', 'http://et07.xhcdn.com/t/693/320/9_2800693.jpg', '2015-04-16 10:48:16', 12, 0, '0'),
(140, 'youporn', 2, 'http://www.youporn.com/watch/9509307/solo-japanese-girl-jilling-dreamroom-productions/?from=vbwn', 'Solo japanese girl jilling - Dreamroom Productions', 'http://cdn4.image.youporn.phncdn.com/201404/25/9509307/640x480/11.jpg', '2015-04-16 10:58:19', 10, 0, '0'),
(143, 'youporn', 2, 'http://www.youporn.com/watch/10542049/jynx-maze-sucks-big-dick-in-gloryhole/?from=vbwn', 'Jynx Maze Sucks Big Dick in Gloryhole', 'http://cdn4.image.youporn.phncdn.com/201410/31/10542049/640x480/10.jpg', '2015-04-16 11:01:28', 1, 0, '0'),
(144, 'youporn', 2, 'http://www.youporn.com/watch/377304/mckenzee-miles-fucked-by-hard-cock/?from=vbwn', 'McKenzee Miles Fucked By Hard Cock', 'http://cdn5.image.youporn.phncdn.com/200910/21/377304/640x480/8.jpg', '2015-04-16 11:02:44', 1, 0, '0'),
(145, 'youtube', 0, 'https://www.youtube.com/watch?v=XbEbeLWCbq8', 'MANI cu Omul Mihai - Eu-s TERMINATOR (VIDEO)', 'http://i1.ytimg.com/vi/XbEbeLWCbq8/0.jpg', '2015-06-29 14:42:37', 94, 105321, '0'),
(146, 'vimeo', 0, 'https://vimeo.com/81150158', 'THE BICYCLE', 'http://i.vimeocdn.com/video/457224619_200x150.jpg', '2015-04-16 11:28:27', 2, 0, '0'),
(147, 'vimeo', 0, 'https://vimeo.com/92372553', 'AWAKENING', 'http://i.vimeocdn.com/video/472085366_200x150.jpg', '2015-04-16 11:43:44', 12, 0, '0'),
(149, 'youtube', 0, 'https://www.youtube.com/watch?v=UWtep9hSYBs', 'Lada Granta - ÑˆÑƒÐ¼Ð¾Ð¸Ð·Ð¾Ð»ÑÑ†Ð¸Ñ Ð¿ÐµÑ€ÐµÐ´Ð½Ð¸Ñ… Ð°Ñ€Ð¾Ðº.', 'http://i1.ytimg.com/vi/UWtep9hSYBs/0.jpg', '2015-04-17 15:59:02', 3, 0, '0'),
(150, 'youporn', 2, 'http://www.youporn.com/watch/9219432/granny-with-hard-nipples-and-hairy-pussy-masturbates/', 'Granny with hard nipples and hairy pussy masturbates', 'http://cdn4.image.youporn.phncdn.com/201401/31/9219432/640x480/13.jpg', '2015-04-17 15:35:58', 2, 0, '0'),
(151, 'xhamster', 2, 'http://xhamster.com/movies/2822091/velicity_von_in_nasty_threesome.html', 'Velicity Von In Nasty Threesome', 'http://et11.xhcdn.com/t/091/320/7_2822091.jpg', '2015-04-24 12:15:17', 10, 0, '0'),
(152, 'xtube', 2, 'http://www.xtube.com/watch.php?v=he4y0_S330_', 'Toy play', 'http://i6.cdn1.videothumbs.xtube.com/m=eaAaaEFb/video_thumb/2009/20090620pm/6330Spz1oa1_0000.jpg', '2015-04-17 14:50:40', 1, 0, '0'),
(153, 'xvideos', 2, 'www.xvideos.com/video6699313/all_the_curves', 'All the Curves ', 'http://img-l3.xvideos.com/videos/thumbslll/d1/a8/92/d1a892f3bab40d12afd26166848d5678/d1a892f3bab40d12afd26166848d5678.27.jpg', '2015-04-23 15:09:39', 4, 0, '0'),
(154, 'redtube', 2, 'http://www.redtube.com/892019', 'BumsLuder aus Bottrop Gebumst', 'http://img.l3.cdn.redtubefiles.com/_thumbs/0000892/0892019/0892019_009m.jpg', '2015-04-22 18:37:38', 30, 0, '0'),
(155, 'xtube', 2, 'www.xtube.com/watch.php?v=u69ox-S674-#.VTDzp-SlilM', 'Homegrown Big Tits', 'http://i4.cdn1.videothumbs.xtube.com/m=eaAaaEFb/videos/201302/07/u69ox-S674-/original/1.jpg', '2015-04-23 14:54:33', 7, 0, '0'),
(158, 'redtube', 2, 'http://www.redtube.com/1050598', 'compilation of Dripping Wet Creamy Pussy', 'http://img.l3.cdn.redtubefiles.com/_thumbs/0001050/1050598/1050598_004m.jpg', '2015-04-17 15:24:37', 2, 0, '0'),
(159, 'xhamster', 2, 'http://xhamster.com/movies/2078351/jessie_volt_anal_creampie_vincevouyer.html', 'Jessie Volt Anal Creampie - VinceVouyer', 'http://et09.xhcdn.com/t/351/320/6_2078351.jpg', '2015-04-23 13:43:45', 8, 0, '0'),
(160, 'mixcloud', 1, 'http://www.mixcloud.com/LeFtOoO/show619-new-werkha-hackman-lido-canblaster-nosaj-thing-falty-dl/', 'Show#619 | New Werkha | Hackman | Lido &amp;amp; Canblaster | Nosaj Thing | Falty DL | ...', 'https://images-mix.netdna-ssl.com/w/600/h/600/q/85/upload/images/extaudio/c388f5d7-e476-4105-b736-a05355f7368a.jpg', '2015-04-17 20:35:57', 5, 0, '0'),
(161, 'redtube', 2, 'http://www.redtube.com/942846', 'BumsLuder aus Pforzheim Gebumst', 'http://img.l3.cdn.redtubefiles.com/_thumbs/0000942/0942846/0942846_010m.jpg', '2015-04-22 22:10:28', 6, 0, '0'),
(166, 'youtube', 0, 'https://www.youtube.com/watch?v=JMSykHQUpW4', 'Mercedes-Benz E320(W210) Ð¢ÐµÑÑ‚-Ð´Ñ€Ð°Ð¹Ð².Anton Avtoman.', 'http://i1.ytimg.com/vi/JMSykHQUpW4/0.jpg', '2015-04-19 20:30:38', 3, 0, '0'),
(167, 'youtube', 0, 'https://www.youtube.com/watch?v=vsHm25X4iQ4', 'Mercedes E W210 - Ð¡ÐµÐºÐ¾Ð½Ð´ Ð¢ÐµÑÑ‚', 'http://i1.ytimg.com/vi/vsHm25X4iQ4/0.jpg', '2015-04-18 11:09:12', 3, 0, '0'),
(168, 'youtube', 0, 'https://www.youtube.com/watch?v=yQlrPUJT-1M', 'BMW 7 ÑÐµÑ€Ð¸Ð¸ E65 - Ð¡ÐµÐºÐ¾Ð½Ð´ Ð¢ÐµÑÑ‚', 'http://i1.ytimg.com/vi/yQlrPUJT-1M/0.jpg', '2015-04-20 14:08:36', 6, 0, '0'),
(170, 'youtube', 0, 'https://www.youtube.com/watch?v=sZIgxA3LvS8', 'Very Funny Videos 2014 HD', 'http://i1.ytimg.com/vi/sZIgxA3LvS8/0.jpg', '2015-04-20 14:01:39', 9, 0, '0'),
(171, 'youtube', 0, 'https://www.youtube.com/watch?v=iNJdPyoqt8U', 'COSTA RICA IN 4K 60fps (ULTRA HD) w/ Freefly Movi', 'http://i1.ytimg.com/vi/iNJdPyoqt8U/0.jpg', '2015-04-17 22:31:48', 3, 0, '0'),
(173, 'youtube', 0, 'https://www.youtube.com/watch?v=Akzlze2eFHk', 'Din cirese-mi pun cercei TraLaLa', 'http://i1.ytimg.com/vi/Akzlze2eFHk/0.jpg', '2015-04-21 15:25:24', 3, 0, '0'),
(174, 'youtube', 0, 'https://www.youtube.com/watch?v=7xalcZyArsA', 'Apashe - No Twerk (feat. Panther &amp; Odalisk)', 'http://i1.ytimg.com/vi/7xalcZyArsA/0.jpg', '2015-04-18 15:18:47', 4, 0, '0'),
(175, 'youtube', 0, 'https://www.youtube.com/watch?v=aV2bHDynx58', 'Boas Novas-Cazuza', 'http://i1.ytimg.com/vi/aV2bHDynx58/0.jpg', '2015-04-18 16:54:38', 2, 0, '0'),
(176, 'youtube', 0, 'https://www.youtube.com/watch?v=zJT0LvxPbqw', 'ÐœÐ°ÑˆÐ° Ð¸ ÐœÐµÐ´Ð²ÐµÐ´ÑŒ - Ð’ÑÐµ ÑÐµÑ€Ð¸Ð¸ Ð¿Ð¾Ð´Ñ€ÑÐ´ (38-42 ÑÐµÑ€Ð¸Ð¸)', 'http://i1.ytimg.com/vi/zJT0LvxPbqw/0.jpg', '2015-04-19 20:31:33', 3, 0, '0'),
(177, 'youtube', 0, 'https://www.youtube.com/watch?v=F4iyIpEEp6k', 'ÐœÐ°ÑˆÐ° Ð¸ ÐœÐµÐ´Ð²ÐµÐ´ÑŒ - Ð’ÑÐµ ÑÐµÑ€Ð¸Ð¸ Ð¿Ð¾Ð´Ñ€ÑÐ´ (12-16 ÑÐµÑ€Ð¸Ð¸)', 'http://i1.ytimg.com/vi/F4iyIpEEp6k/0.jpg', '2015-04-19 20:31:38', 2, 0, '0'),
(178, 'youtube', 0, 'https://www.youtube.com/watch?v=c8kWRORUxC4', 'Funny Video Compilation Big Boobs - Sexy Funny Video - Funny Prank', 'http://i1.ytimg.com/vi/c8kWRORUxC4/0.jpg', '2015-04-21 19:36:41', 9, 0, '0'),
(179, 'youtube', 0, 'https://www.youtube.com/watch?v=Aj146yHvuWw', 'Hillclimb Drift Pure Sound Powerslides', 'http://i1.ytimg.com/vi/Aj146yHvuWw/0.jpg', '2015-04-21 13:43:53', 3, 0, '0'),
(180, 'youtube', 0, 'https://www.youtube.com/watch?v=KWZGAExj-es', 'Sia - Elastic Heart feat. Shia LaBeouf &amp; Maddie Ziegler (Official Video)', 'http://i1.ytimg.com/vi/KWZGAExj-es/0.jpg', '2015-04-23 15:27:15', 9, 0, '0'),
(181, 'youtube', 0, 'https://www.youtube.com/watch?v=3VAbku7vwQY', 'A Royal Night Out Official Trailer 1 (2015) - Emily Watson, Sarah Gadon Movie HD', 'http://i1.ytimg.com/vi/3VAbku7vwQY/0.jpg', '2015-04-21 13:44:44', 1, 0, '0'),
(182, 'youtube', 0, 'https://www.youtube.com/watch?v=u-qYMl9T9wQ', 'Cris Cab - Liar Liar', 'http://i1.ytimg.com/vi/u-qYMl9T9wQ/0.jpg', '2015-04-21 17:27:01', 4, 0, '0'),
(183, 'xvideos', 2, 'http://www.xvideos.com/video8077148/euro_girlnextdoor_pussyfucked_for_cash', 'Euro girlnextdoor pussyfucked for cash ', 'http://img-l3.xvideos.com/videos/thumbslll/50/37/d6/5037d69de821b7840cce3cd9859ec59f/5037d69de821b7840cce3cd9859ec59f.2.jpg', '2015-04-22 14:47:05', 9, 0, '0'),
(184, 'youtube', 0, 'https://www.youtube.com/watch?v=74cd5JaeCf8', 'Ð§Ð°ÑÐ½ Ð¤Ð°Ð¼Ð°Ð»Ð¸, Ð–Ð°Ñ€Ð° - Ð’Ñ€ÐµÐ¼ÐµÐ½Ð°Ð¼Ð¸', 'http://i1.ytimg.com/vi/74cd5JaeCf8/0.jpg', '2015-04-21 19:36:22', 2, 0, '0'),
(185, 'xtube', 2, 'http://www.xtube.com/watch.php?v=F4gHO-S163-', 'strapon slave serve', 'http://i4.cdn1.videothumbs.xtube.com/m=eaAaaEFb/videos/201203/13/F4gHO-S163-/original/1.jpg', '2015-04-21 19:30:14', 3, 0, '0'),
(186, 'twitch', 0, 'http://www.twitch.tv/crabcow/c/6558745', 'YOU OVERESTIMATE MY-', 'http://static-cdn.jtvnw.net/jtv.thumbs/archive-651098549-320x240.jpg', '2015-04-21 20:10:23', 1, 0, '0'),
(187, 'youtube', 0, 'https://www.youtube.com/watch?feature=player_embedded&amp;v=AWacMAes2dA', 'Should You Buy A Romper?', 'http://i1.ytimg.com/vi/AWacMAes2dA/0.jpg', '2015-04-22 18:03:33', 2, 0, '0'),
(188, 'vimeo', 0, 'https://vimeo.com/channels/staffpicks/125167921', 'Paris - NewYork', 'http://i.vimeocdn.com/video/515180793_200x150.jpg', '2015-04-21 23:35:10', 2, 0, '0'),
(189, 'vimeo', 0, 'https://vimeo.com/channels/staffpicks/125108525', 'SKYGLOW', 'http://i.vimeocdn.com/video/515088904_200x150.jpg', '2015-04-21 22:20:21', 1, 0, '0'),
(190, 'ted', 0, 'http://www.ted.com/talks/bel_pesce_5_ways_to_kill_your_dreams', '5 ways to kill your dreams', 'https://tedcdnpi-a.akamaihd.net/r/images.ted.com/images/ted/54a297f5353f48e9826999bf7ae3cd163b952053_2880x1620.jpg?quality=89&amp;w=600', '2015-06-29 14:18:46', 2, 0, '0'),
(191, 'flickr', 0, '', 'Video - Fields of Kamuela', 'https://c2.staticflickr.com/4/3179/2505980161_95057fba79.jpg', '2015-04-21 22:48:54', 4, 0, '0'),
(192, 'blip', 0, 'http://blip.tv/terror-obscura/terror-obscura-critters-2-7182289', 'Terror Obscura- Critters 2', 'http://a.images.blip.tv/FearFan-TerrorObscuraCritters2812.jpg', '2015-04-21 22:56:24', 1, 0, '0'),
(193, 'metacafe', 0, 'http://www.metacafe.com/watch/11374541/epic_meal_time_fast_food_meatloaf_lasagna/', '\r\n				Epic Meal Time: Fast Food Meatloaf Lasagna								\r\n							\r\n		', 'http://s6.mcstatic.com/thumb/11374541/29166309/4/flash_player/0/1/epic_meal_time_fast_food_meatloaf_lasagna.jpg?v=1', '2015-04-22 14:55:44', 7, 0, '0'),
(194, 'youporn', 2, 'http://www.youporn.com/watch/11229831/massage-rooms-horny-teen-has-her-tight-pussy-oiled-up-and-fucked/', 'Massage Rooms Horny teen has her tight pussy oiled up and fucked', 'http://cdn4.image.youporn.phncdn.com/201504/15/11229831/640x480/12.jpg', '2015-04-22 19:06:09', 4, 0, '0'),
(195, 'xhamster', 2, 'http://xhamster.com/movies/4479370/whats_on_tv.html', 'What&#39;s On TV?', 'http://et12.xhcdn.com/t/370/320/7_4479370.jpg', '2015-04-21 23:15:57', 1, 0, '0'),
(196, 'xvideos', 2, 'http://www.xvideos.com/video8552414/paying_the_plumber_with_her_ass', 'Paying The Plumber With Her Ass ', 'http://img-l3.xvideos.com/videos/thumbslll/c1/7d/21/c17d21f7be11d356491c46ec22584707/c17d21f7be11d356491c46ec22584707.4.jpg', '2015-04-23 17:06:12', 8, 0, '0'),
(197, 'redtube', 2, 'http://www.redtube.com/127059', 'Tory', 'http://img.l3.cdn.redtubefiles.com/_thumbs/0000127/0127059/0127059_006m.jpg', '2015-04-23 15:33:32', 6, 0, '0'),
(198, 'youtube', 0, 'https://www.youtube.com/watch?v=KLZAAx_R7tE', 'ÐŸÐ°Ð²ÐµÐ» Ð’Ð¾Ð»Ñ  ÐšÐ°Ð¼ÐµÐ´Ð¸ ÐºÐ»Ð°Ð± 2015  Ð’ÑÐµ Ð²Ñ‹Ð¿ÑƒÑÐºÐ¸ Ð¿Ð¾Ð´Ñ€ÑÐ´!', 'http://i1.ytimg.com/vi/KLZAAx_R7tE/0.jpg', '2015-04-22 09:39:16', 2, 0, '0'),
(199, 'soundcloud', 1, 'https://soundcloud.com/bigb-lezyg/jason-derulo-want-to-want-me-big-b-lezy-g-extended-1', 'Jason Derulo - Want To Want Me (Big B u0026 Lezy G Extended)', 'https://i1.sndcdn.com/artworks-000110776137-622e0g-t500x500.jpg', '2015-04-23 17:03:22', 3, 0, '0'),
(200, 'soundcloud', 1, 'https://soundcloud.com/bobatl/nick-jonas-jealous-feat-bob', 'Nick Jonas - Jealous - feat B.o.B', 'https://i1.sndcdn.com/avatars-000059365078-np1yml-t500x500.jpg', '2015-04-22 14:54:09', 5, 0, '0'),
(201, 'mixcloud', 1, 'http://www.mixcloud.com/juanramongimenezsanchez36/new-horizon-coffee-bar-chill-sounnds/', '* New Horizon - Coffee Bar Chill Sounnds *', 'https://images-mix.netdna-ssl.com/w/600/h/600/q/85/upload/images/extaudio/36f3e977-6178-4aa6-a919-993698936de6.jpg', '2015-07-01 11:47:11', 2, 2, '0'),
(202, 'vk', 0, 'http://vk.com/video-72665207_171153323?list=club72665207,album-72665207', 'Zankyou no Terror 11 ÑÐµÑ€Ð¸Ñ &amp;#092; Ð¢ÐµÑ€Ñ€Ð¾Ñ€ Ð² Ð¢Ð¾ÐºÐ¸Ð¾ 11 ÑÐµÑ€Ð¸Ñ &amp;#092; Ð¢Ð¾ÐºÐ¸Ð¹ÑÐºÐ¸Ð¹ Ð¢ÐµÑ€Ñ€Ð¾Ñ€ 11 ÑÐµÑ€Ð¸Ñ &amp;#092; Ð ÐµÐ·Ð¾Ð½Ð°Ð½Ñ Ð£Ð¶Ð°ÑÐ° 11 ÑÐµÑ€Ð¸Ñ', 'http://cs543303.vk.me/u215757174/video/x_7758faf2.jpg', '2015-04-22 14:56:50', 3, 0, '0'),
(203, 'vine', 0, 'https://vine.co/v/OQUQwBulQqF', '&quot;All that we see or seem is but a dream within a dream&quot; - Edgar Allan Poe', 'https://v.cdn.vine.co/r/videos/6712C4898A1181822896147177472_369d9a87f39.1.5.6400938655537138886.mp4.jpg?versionId=gsMK13Q2XqZMUJjqLXf2QHNpKlw.cYOA', '2015-04-22 18:00:41', 6, 0, '0'),
(204, 'youtube', 0, 'https://www.youtube.com/watch?v=F90Cw4l-8NY', 'Bastille - Pompeii', 'http://i1.ytimg.com/vi/F90Cw4l-8NY/0.jpg', '2015-04-22 20:02:21', 2, 0, '0'),
(205, 'youtube', 0, 'https://www.youtube.com/watch?v=HF3Mk_FZocY', 'ADDA - Canta Cucu (DJ NenZ Edit 2015)', 'http://i1.ytimg.com/vi/HF3Mk_FZocY/0.jpg', '2015-04-22 20:10:00', 1, 0, '0'),
(206, 'youtube', 0, 'https://m.youtube.com/watch?v=gpr65tv5QBs', 'REVENGE 12 - Sexy Surprise Turns Into Public Humiliation Prank', 'http://i1.ytimg.com/vi/gpr65tv5QBs/0.jpg', '2015-04-23 00:23:13', 4, 0, '0'),
(207, 'youtube', 0, 'https://m.youtube.com/watch?v=hTdQlEFnt7c', 'Extra Footage | REVENGE 12 - Sexy Surprise Turns Into Public Humiliation Prank', 'http://i1.ytimg.com/vi/hTdQlEFnt7c/0.jpg', '2015-04-23 00:23:57', 2, 0, '0'),
(208, 'youtube', 0, 'https://m.youtube.com/watch?v=CAEfBoxKxGg', 'OMG SHE IS NAKED AGAIN!!', 'http://i1.ytimg.com/vi/CAEfBoxKxGg/0.jpg', '2015-04-23 14:20:57', 14, 0, '0'),
(209, 'youtube', 0, 'https://m.youtube.com/watch?v=e2orZaoCcbs', 'WAXING CHALLENGE', 'http://i1.ytimg.com/vi/e2orZaoCcbs/0.jpg', '2015-04-23 00:34:41', 3, 0, '0'),
(210, 'soundcloud', 1, 'https://m.soundcloud.com/twaynemusic/nasty-freestyle-t-wayne', 'Nasty Freestyle - T-wayne', 'https://i1.sndcdn.com/avatars-000069086092-vk523z-t500x500.jpg', '2015-04-23 00:27:57', 1, 0, '0'),
(211, 'soundcloud', 1, 'https://m.soundcloud.com/majorlazer/major-lazer-dj-snake-lean-on-feat-mo', 'Major Lazer u0026 DJ Snake - Lean On (feat. MÃ˜)', 'https://i1.sndcdn.com/artworks-000108593302-0ek7n6-t500x500.jpg', '2015-04-23 00:29:14', 1, 0, '0'),
(212, 'soundcloud', 1, 'https://m.soundcloud.com/kygo/ed-sheeran-i-see-fire-kygo', 'Ed Sheeran - I See Fire (Kygo Remix)', 'https://i1.sndcdn.com/artworks-000064260922-w3cv4b-t500x500.jpg', '2015-04-23 00:30:46', 1, 0, '0'),
(213, 'soundcloud', 1, 'https://m.soundcloud.com/kygo/firestone-ft-conrad', 'Firestone (Ft. Conrad)', 'https://i1.sndcdn.com/artworks-000098992306-fkfnk8-t500x500.jpg', '2015-04-23 18:54:27', 2, 0, '0'),
(214, 'soundcloud', 1, 'https://m.soundcloud.com/kygo/marvin-gaye-sexual-healing', 'Marvin Gaye - Sexual Healing (Kygo Remix)', 'https://i1.sndcdn.com/artworks-000063355166-wja5au-t500x500.jpg', '2015-04-23 00:31:59', 1, 0, '0'),
(215, 'soundcloud', 1, 'https://m.soundcloud.com/kygo/ed-sheeran-passenger-no', 'Ed Sheeran u0026 Passenger - No Diggity vs. Thrift Shop (Kygo Remix)', 'https://i1.sndcdn.com/artworks-000063262291-knsk3a-t500x500.jpg', '2015-04-23 00:32:57', 1, 0, '0'),
(216, 'soundcloud', 1, 'https://m.soundcloud.com/kygo/seinabo-sey-younger-kygo-remix', 'Seinabo Sey - Younger (Kygo Remix)', 'https://i1.sndcdn.com/artworks-000066591433-nesygp-t500x500.jpg', '2015-04-27 10:51:26', 2, 0, '0'),
(217, 'soundcloud', 1, 'https://m.soundcloud.com/tiago-kaercher/the-xx-intro-vs-pendulum-the', 'The XX Intro vs Pendulum - The Island (Tomorrowland Mashup Bootleg) (by DJ TiagÃ¶)', 'https://i1.sndcdn.com/artworks-000030927867-0q3ssz-t500x500.jpg', '2015-04-23 01:31:11', 1, 0, '0'),
(218, 'soundcloud', 1, 'https://m.soundcloud.com/aktaridon/the-xx-intro-project-x', 'The XX  Intro Project X Soundtrack', 'https://i1.sndcdn.com/avatars-000076942467-9e41y0-t500x500.jpg', '2015-06-29 15:29:47', 2, 1176527, '0'),
(219, 'youtube', 0, 'https://m.youtube.com/watch?v=I-qTrxpqZRQ', 'Legendary Dribbling Skills', 'http://i1.ytimg.com/vi/I-qTrxpqZRQ/0.jpg', '2015-04-23 13:29:00', 3, 0, '0'),
(220, 'youtube', 0, 'https://www.youtube.com/watch?v=mGcHNnI2mh4', 'If People Left Parties Like They Leave Facebook', 'http://i1.ytimg.com/vi/mGcHNnI2mh4/0.jpg', '2015-04-23 14:23:11', 5, 0, '0'),
(221, 'youtube', 0, 'https://www.youtube.com/watch?v=HIPJrrQlxzY', 'The Six Girls You&#39;ll Date in College', 'http://i1.ytimg.com/vi/HIPJrrQlxzY/0.jpg', '2015-04-23 14:24:38', 2, 0, '0'),
(222, 'youtube', 0, 'https://www.youtube.com/watch?v=ArrWgQS51Yc', 'Top 10 Worst Movie Plot Twists', 'http://i1.ytimg.com/vi/ArrWgQS51Yc/0.jpg', '2015-04-23 14:25:16', 3, 0, '0'),
(223, 'youtube', 0, 'https://www.youtube.com/watch?v=Mh2ebPxhoLs', 'Southpaw Official Trailer #1 (2015) - Jake Gyllenhaal, Rachel McAdams Movie HD', 'http://i1.ytimg.com/vi/Mh2ebPxhoLs/0.jpg', '2015-04-23 18:55:24', 5, 0, '0'),
(224, 'youtube', 0, 'https://www.youtube.com/watch?v=47IDQcnTpqg', 'La multi Ani cu  Cleopatra Stratan - Commercial Ad Child Song', 'http://i1.ytimg.com/vi/47IDQcnTpqg/0.jpg', '2015-04-23 15:21:11', 4, 0, '0'),
(225, 'youtube', 0, 'https://www.youtube.com/watch?v=1N5siJLrQ5Y', 'peppa pig Full episode Mammy pig Daddy Pig george pig The Barbecue Peppa pig Story', 'http://i1.ytimg.com/vi/1N5siJLrQ5Y/0.jpg', '2015-04-23 15:21:35', 1, 0, '0'),
(226, 'youtube', 0, 'https://www.youtube.com/watch?v=BVm1ZVQWZX4', 'Ð¡Ð²Ð¸Ð½ÐºÐ° ÐŸÑÐ¿Ð¿Ð° 3 ÑÐµÐ·Ð¾Ð½ 26-50 ÑÐµÑ€Ð¸Ñ Ð±ÐµÐ· Ñ€Ð°Ð¼Ð¾Ðº Ð¿Ð¾Ð»Ð½Ñ‹Ð¹ ÑÐºÑ€Ð°Ð½', 'http://i1.ytimg.com/vi/BVm1ZVQWZX4/0.jpg', '2015-04-23 15:28:05', 1, 0, '0'),
(227, 'youtube', 0, 'https://www.youtube.com/watch?v=VZjxqxL9v1I', 'Versiunea originala &quot;LA MULTI ANI &quot;', 'http://i1.ytimg.com/vi/VZjxqxL9v1I/0.jpg', '2015-04-23 15:28:26', 1, 0, '0'),
(228, 'youtube', 0, 'https://www.youtube.com/watch?v=ALBwaO-rAsE', 'Robert Downey Jr full interview: star walks out when asked about past', 'http://i1.ytimg.com/vi/ALBwaO-rAsE/0.jpg', '2015-04-23 15:31:51', 1, 0, '0'),
(229, 'youtube', 0, 'https://www.youtube.com/watch?v=gVXXQaI7PGY', 'Asdfmovie 1-7 rus', 'http://i1.ytimg.com/vi/gVXXQaI7PGY/0.jpg', '2015-04-23 15:32:35', 2, 0, '0'),
(230, 'youtube', 0, 'https://www.youtube.com/watch?v=Un_DSu2EWM8', 'Asdf Movie 1-8 RUS', 'http://i1.ytimg.com/vi/Un_DSu2EWM8/0.jpg', '2015-04-23 15:33:05', 2, 0, '0'),
(231, 'youtube', 0, 'https://www.youtube.com/watch?v=cRm54SJxcao', 'Ð‘Ð°ÑÑ‚Ð° / Ð“ÑƒÑ„ -  Ð§ÐŸ', 'http://i1.ytimg.com/vi/cRm54SJxcao/0.jpg', '2015-04-23 15:38:57', 1, 0, '0'),
(232, 'youtube', 0, 'https://www.youtube.com/watch?v=JmNr3sPyuzs', 'Ð“ÑƒÑ„ - Ð¡ÐµÐ³Ð¾Ð´Ð½Ñ - Ð—Ð°Ð²Ñ‚Ñ€Ð°', 'http://i1.ytimg.com/vi/JmNr3sPyuzs/0.jpg', '2015-04-23 15:39:18', 1, 0, '0'),
(233, 'youtube', 0, 'https://www.youtube.com/watch?v=lWZ7O-RrATY', 'Tomorrowland Official Trailer #1 (2015) - George Clooney, Britt Robertson Movie HD', 'http://i1.ytimg.com/vi/lWZ7O-RrATY/0.jpg', '2015-04-23 15:39:35', 1, 0, '0'),
(234, 'youtube', 0, 'https://www.youtube.com/watch?v=se14RB6bK_4', '[Affaire Roches-Noires] Anishta, l&#39;ex Ã©pouse de Gooljaury, raconte ce qui sâ€™est vraiment passÃ©', 'http://i1.ytimg.com/vi/se14RB6bK_4/0.jpg', '2015-04-23 15:48:12', 4, 0, '0'),
(235, 'youtube', 0, 'https://www.youtube.com/watch?v=3gZZP-nBKRw', 'Ð“ÑƒÑ„ - ÐšÐ°Ðº ÑÑ‚Ð¾ Ð±Ñ‹Ð»Ð¾ Ð´Ð°Ð²Ð½Ð¾', 'http://i1.ytimg.com/vi/3gZZP-nBKRw/0.jpg', '2015-04-23 15:43:03', 1, 0, '0'),
(236, 'youtube', 0, 'https://www.youtube.com/watch?v=izp1L5JR6L0', 'Centr - Ð“Ð¾Ñ€Ð¾Ð´ Ð”Ð¾Ñ€Ð¾Ð³ (ÐºÐ»Ð¸Ð¿)', 'http://i1.ytimg.com/vi/izp1L5JR6L0/0.jpg', '2015-04-23 15:43:53', 2, 0, '0'),
(237, 'youtube', 0, 'https://www.youtube.com/watch?v=EUXRx1dgJSQ', 'Ð-Ð¡Ñ‚ÑƒÐ´Ð¸Ð¾ &amp; ÐžÑ‚Ð¿ÐµÑ‚Ñ‹Ðµ Ð¼Ð¾ÑˆÐµÐ½Ð½Ð¸ÐºÐ¸ - Ð¡ÐµÑ€Ð´Ñ†ÐµÐ¼ Ðº ÑÐµÑ€Ð´Ñ†Ñƒ', 'http://i1.ytimg.com/vi/EUXRx1dgJSQ/0.jpg', '2015-04-23 15:44:54', 2, 0, '0'),
(238, 'youtube', 0, 'https://www.youtube.com/watch?v=BGu5LQvGWJg', 'Ð Ð¡Ñ‚ÑƒÐ´Ð¸Ð¾   - ÐŸÐ¾Ð·Ð²Ð¾Ð½Ð¸ Ð¼Ð½Ðµ Ð¿Ð¾Ð·Ð²Ð¾Ð½Ð¸', 'http://i1.ytimg.com/vi/BGu5LQvGWJg/0.jpg', '2015-04-23 15:47:27', 2, 0, '0'),
(239, 'youtube', 0, 'https://www.youtube.com/watch?v=lrfkQnSKao0', 'Ð Ð¡Ñ‚ÑƒÐ´Ð¸Ð¾ - ÐšÑÑ‚Ð¸ Ð¢Ð¾Ð¿ÑƒÑ€Ð¸Ñ - Ð¢Ð°Ðº Ð¶Ðµ ÐºÐ°Ðº Ð²ÑÐµ', 'http://i1.ytimg.com/vi/lrfkQnSKao0/0.jpg', '2015-04-23 15:47:39', 1, 0, '0'),
(240, 'youtube', 0, 'https://www.youtube.com/watch?v=6MxWGUUyVcM', 'A`Studio - Ð¢Ð°ÐºÐ¶Ðµ ÐºÐ°Ðº Ð²ÑÐµ', 'http://i1.ytimg.com/vi/6MxWGUUyVcM/0.jpg', '2015-04-23 15:47:52', 1, 0, '0'),
(241, 'youtube', 0, 'https://www.youtube.com/watch?v=0baM5m36t-8', 'Ð¡Ð»Ð°Ð²Ð° - ÐžÐ´Ð¸Ð½Ð¾Ñ‡ÐµÑÑ‚Ð²Ð¾ (Official Video)', 'http://i1.ytimg.com/vi/0baM5m36t-8/0.jpg', '2015-04-23 15:48:03', 1, 0, '0'),
(242, 'youtube', 0, 'https://www.youtube.com/watch?v=mV5xJT7BnzE', 'ÐÐ»ÐºÐ° - ÐŸÑ€Ð¾Ð²Ð°Ð½Ñ', 'http://i1.ytimg.com/vi/mV5xJT7BnzE/0.jpg', '2015-04-23 15:48:14', 1, 0, '0'),
(243, 'youtube', 0, 'https://www.youtube.com/watch?v=xc3e_7qEAuA', 'Ð”Ð¶Ð¸Ð³Ð°Ð½ feat. Ð®Ð»Ð¸Ñ Ð¡Ð°Ð²Ð¸Ñ‡ÐµÐ²Ð° - ÐžÑ‚Ð¿ÑƒÑÑ‚Ð¸ (Official video)', 'http://i1.ytimg.com/vi/xc3e_7qEAuA/0.jpg', '2015-04-23 15:48:29', 1, 0, '0'),
(244, 'youtube', 0, 'https://www.youtube.com/watch?v=4m1EFMoRFvY', 'BeyoncÃ© - Single Ladies (Put a Ring on It)', 'http://i1.ytimg.com/vi/4m1EFMoRFvY/0.jpg', '2015-04-23 18:54:42', 2, 0, '0'),
(245, 'youtube', 0, 'https://www.youtube.com/watch?v=dNnnn84ebC4', 'Top 10 Nintendo DS Games', 'http://i1.ytimg.com/vi/dNnnn84ebC4/0.jpg', '2015-04-23 15:48:51', 1, 0, '0'),
(246, 'youtube', 0, 'https://www.youtube.com/watch?v=EGbOa65jOm0', 'Dan Balan Ð¸ Ð’ÐµÑ€Ð° Ð‘Ñ€ÐµÐ¶Ð½ÐµÐ²Ð° - Ð›ÐµÐ¿ÐµÑÑ‚ÐºÐ°Ð¼Ð¸ Ð¡Ð»ÐµÐ· (Official Video)', 'http://i1.ytimg.com/vi/EGbOa65jOm0/0.jpg', '2015-04-23 15:48:55', 1, 0, '0'),
(247, 'youtube', 0, 'https://www.youtube.com/watch?v=FS1p7W5dmBE', 'Game Theory: Wii U is the New Virtual Boy', 'http://i1.ytimg.com/vi/FS1p7W5dmBE/0.jpg', '2015-04-23 15:49:06', 1, 0, '0'),
(248, 'youtube', 0, 'https://www.youtube.com/watch?v=hIPQAVVaGOg', 'ÐšÐ˜ÐÐž ÐšÐ›ÐÐ¡Ð¡! ÐžÐ§Ð•ÐÐ¬ Ð¥ÐžÐ ÐžÐ¨Ð˜Ð™ Ð¤Ð˜Ð›Ð¬Ðœ - &quot;Ð”Ñ€ÑƒÐ³Ð°Ñ Ð¶ÐµÐ½Ñ‰Ð¸Ð½Ð°&quot; (Ð ÑƒÑÑÐºÐ¾Ðµ ÐºÐ¸Ð½Ð¾, Ð ÑƒÑÑÐºÐ¸Ðµ Ð¼ÐµÐ»Ð¾Ð´Ñ€Ð°Ð¼Ñ‹)', 'http://i1.ytimg.com/vi/hIPQAVVaGOg/0.jpg', '2015-04-23 18:55:01', 2, 0, '0'),
(249, 'youtube', 0, 'https://www.youtube.com/watch?v=JWHq1YWVEJY', 'Orhnvac Jur    (Charencavan)', 'http://i1.ytimg.com/vi/JWHq1YWVEJY/0.jpg', '2015-04-23 15:49:46', 1, 0, '0'),
(250, 'vimeo', 0, 'https://vimeo.com/23353956', 'Dubai 24h 2011', 'http://i.vimeocdn.com/video/151955642_200x150.jpg', '2015-04-23 19:03:40', 1, 0, '0'),
(251, 'blip', 0, 'http://blip.tv/nostalgiacritic/nc-and-avgn-teenage-mutant-ninja-turtles-2014-7182501', 'NC and AVGN Teenage Mutant Ninja Turtles 2014', 'http://a.images.blip.tv/NostalgiaCritic-NCAndAVGNTeenageMutantNinjaTurtles2014574.jpg', '2015-04-23 19:07:55', 1, 0, '0'),
(252, 'facebook', 0, 'https://www.facebook.com/primacool/videos/vb.97718196244/10152894258856245/?type=2&amp;theater', 'Za Ätvrt hodiny jsou tu dalÅ¡Ã­ Lovci zÃ¡Å¾itkÅ¯. TentokrÃ¡t se pojede do LitomyÅ¡le... za ÄokolÃ¡dou! &lt;i class=&quot;_4-k1 img sp_0FWTQ_K3bWk sx_72c37c&quot;&gt;&lt;u&gt;smile emoticon&lt;/u&gt;&lt;/i&gt;&lt;span class=&quot;fsm&quot;&gt;&lt;div cla', 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xaf1/v/t1.0-1/c12.12.155.155/s32x32/522511_10151315776556245_1734252634_n.jpg?oh=b9d5bbe0406a8dc9b8d71904b684bf47&amp;oe=559988A5&amp;__gda__=1436359013_9beb3dea7f28360fed46c25a39ce354a', '2015-04-23 19:09:41', 1, 0, '0'),
(253, 'ted', 0, 'https://www.ted.com/talks/chris_milk_how_virtual_reality_can_create_the_ultimate_empathy_machine', 'How virtual reality can create the ultimate empathy machine', 'https://tedcdnpi-a.akamaihd.net/r/images.ted.com/images/ted/ffcb1942eeea2e2abfe0c67ff3e96f07a6bd25d6_2880x1620.jpg?quality=89&amp;w=600', '2015-04-23 19:14:22', 1, 0, '0'),
(254, 'instagram', 0, 'https://instagram.com/p/10piemtz24/', 'IMGmodels on Instagram: â€œHAPPY BIRTHDAY @JessicaStamOfficial! ðŸ’ŽðŸ‘¸ðŸ’– #IMGirlsâ€', 'https://igcdn-photos-d-a.akamaihd.net/hphotos-ak-xfa1/t51.2885-15/11190926_883018005074379_1539593323_n.jpg', '2015-04-23 19:19:02', 1, 0, '0'),
(255, 'vine', 0, 'https://vine.co/v/Mb9TqYLHgqq', '#selfieswithstrangers #disneyworld wow she caught me ðŸ˜‚', 'https://v.cdn.vine.co/r/thumbs/202F511D4F1056663380036849664_1.4.8.6341957726197728277.mp4.jpg?versionId=SKbyi3pb5Hwwxv53hu8Jbg2wZsFp_Wn6', '2015-04-27 10:47:27', 3, 0, '0'),
(256, 'twitch', 0, 'http://www.twitch.tv/joex95/c/6570929', 'Call of Duty: Black Ops II', 'http://static-cdn.jtvnw.net/jtv.thumbs/archive-388353710-320x240.jpg', '2015-04-23 19:38:34', 2, 0, '0'),
(257, 'soundcloud', 1, 'https://soundcloud.com/wmb/ed-sheeran-the-a-team', 'Ed Sheeran - The A Team', 'https://i1.sndcdn.com/artworks-000022679501-jelgvy-t500x500.jpg', '2015-07-01 15:31:09', 3, 6019606, '0'),
(258, 'soundcloud', 1, 'https://soundcloud.com/highonmusic1/ed-sheeran-im-in-love-with-the-coco-hitimpulse-remix', 'Ed Sheeran - I&#39;m In Love With The Coco (Hitimpulse Remix)', 'https://i1.sndcdn.com/artworks-000111318948-mlyoa1-t500x500.jpg', '2015-06-29 15:33:20', 8, 26584532, '0'),
(259, 'metacafe', 0, 'http://www.metacafe.com/watch/11372577/epic_meal_time_fast_food_indian/', '\r\n				Epic Meal Time: Fast Food Indian								\r\n							\r\n		', 'http://s6.mcstatic.com/thumb/11372577/29164748/4/flash_player/0/1/epic_meal_time_fast_food_indian.jpg?v=1', '2015-06-29 09:23:16', 4, 0, '0'),
(260, 'metacafe', 0, 'http://www.metacafe.com/watch/9244297/deep_dish_fastfood_pizza_epic_meal_time/', '\r\n				Deep Dish Fastfood Pizza - Epic Meal Time								\r\n							\r\n		', 'http://s6.mcstatic.com/thumb/9244297/25851909/4/flash_player/0/1/deep_dish_fastfood_pizza_epic_meal_time.jpg?v=1', '2015-04-23 19:49:16', 2, 0, '0'),
(261, 'mixcloud', 1, 'http://www.mixcloud.com/chriscoco/melodica-20-april-2015/', 'Melodica 20 April 2015', 'https://images-mix.netdna-ssl.com/w/600/h/600/q/85/upload/images/extaudio/42aaba30-534c-46ab-a466-401894d854d2.png', '2015-06-29 15:26:50', 3, 6, '0'),
(262, 'youporn', 2, 'http://www.youporn.com/watch/9706469/faketaxi-red-head-takes-on-older-cock/?from=vbwn', 'FakeTaxi Red head takes on older cock', 'http://cdn4.image.youporn.phncdn.com/201406/05/9706469/640x480/4.jpg', '2015-04-23 19:54:23', 1, 0, '0'),
(263, 'xtube', 2, 'http://www.xtube.com/watch.php?v=L0usf-S200-', 'MyDirtyHobby', 'http://i6.cdn1.videothumbs.xtube.com/m=eaAaaEFb/videos/201504/21/L0usf-S200-/original/12.jpg', '2015-04-23 20:06:55', 2, 0, '0'),
(264, 'xvideos', 2, 'http://www.xvideos.com/video8756429/teasing_hitch_hiking_blonde_blows_driver', 'Teasing hitch hiking blonde blows driver ', 'http://img100-429.xvideos.com/videos/thumbslll/cf/53/97/cf5397bc4b6832528ae55f3e842c07e6/cf5397bc4b6832528ae55f3e842c07e6.7.jpg', '2015-04-24 13:30:07', 6, 0, '0'),
(265, 'redtube', 2, 'http://www.redtube.com/401357', 'lesbian 1', 'http://img.l3.cdn.redtubefiles.com/_thumbs/0000401/0401357/0401357_011m.jpg', '2015-04-23 20:11:07', 1, 0, '0'),
(266, 'youtube', 0, 'https://www.youtube.com/watch?v=3gxNW2Ulpwk', 'The xx - Intro', 'http://i1.ytimg.com/vi/3gxNW2Ulpwk/0.jpg', '2015-04-27 10:47:16', 5, 0, '0'),
(267, 'youtube', 0, 'https://www.youtube.com/watch?v=3Al9qYNPwXg', 'The Voice - BEST BLIND AUDITIONS 2015 HD', 'http://i1.ytimg.com/vi/3Al9qYNPwXg/0.jpg', '2015-04-27 10:47:00', 5, 0, '0'),
(268, 'metacafe', 0, 'http://www.metacafe.com/watch/11372514/furious_petes_surprise_proposal_workout/', '\r\n				Furious Pete&#39;s Surprise Proposal Workout!!								\r\n							\r\n		', 'http://s3.mcstatic.com/thumb/11372514/29164700/4/flash_player/0/1/furious_petes_surprise_proposal_workout.jpg?v=1', '2015-04-24 11:51:12', 1, 0, '0'),
(269, 'xhamster', 2, 'http://xhamster.com/movies/4195393/sylvia_smith_horny_hairy_girls_big_bush_armpit_and_leg_hair.html', 'Sylvia Smith Horny Hairy Girls Big Bush Armpit and Leg Hair', 'http://et01.xhcdn.com/t/393/320/1_4195393.jpg', '2015-04-29 17:19:30', 10, 0, '0'),
(270, 'xhamster', 2, 'http://xhamster.com/movies/4490450/meine_wichsvorlage_no._2.html', 'Meine Wichsvorlage No. 2', 'http://et12.xhcdn.com/t/450/320/2_4490450.jpg', '2015-04-24 11:52:25', 1, 0, '0'),
(271, 'xhamster', 2, 'http://xhamster.com/movies/3967545/4k_hd_tiny4k_hannah_hartmans_tiny_pussy_gets_slammed.html', '4K HD - Tiny4K Hannah Hartmans tiny pussy gets slammed', 'http://et02.xhcdn.com/t/545/320/10_3967545.jpg', '2015-04-29 17:20:13', 11, 0, '0'),
(272, 'xhamster', 2, 'http://xhamster.com/movies/4490411/jp_13.html', 'jp 13', 'http://et07.xhcdn.com/t/411/320/6_4490411.jpg', '2015-04-29 18:08:42', 31, 0, '0'),
(273, 'metacafe', 0, 'http://www.metacafe.com/watch/11372721/everything_wrong_with_resident_evil_apocalypse/', '\r\n				Everything Wrong with Resident Evil: Apocalypse								\r\n							\r\n		', 'http://s6.mcstatic.com/thumb/11372721/29165321/4/flash_player/0/1/everything_wrong_with_resident_evil_apocalypse.jpg?v=1', '2015-07-01 17:20:30', 5, 0, '0'),
(274, 'xhamster', 2, 'http://xhamster.com/movies/2994970/herzogvideos_heidi_lasst_sie_alle_jodeln_teil_6.html', 'HerzogVideos Heidi lasst sie alle jodeln Teil 6', 'http://et12.xhcdn.com/t/970/320/6_2994970.jpg', '2015-04-29 18:04:36', 3, 0, '0'),
(275, 'soundcloud', 1, 'https://soundcloud.com/strut/mulatu-astatke-the', 'Mulatu Astatke u0026 The Heliocentrics - Esketa Dance (Strut Africa)', 'https://i1.sndcdn.com/artworks-000065717372-iwacsj-t500x500.jpg', '2015-06-29 15:25:28', 4, 14394, '0'),
(276, 'xhamster', 2, 'http://xhamster.com/movies/4336407/yuki_tohma_erotic_japanese_star.html', 'Yuki Tohma - Erotic Japanese Star', 'http://et00.xhcdn.com/t/407/320/8_4336407.jpg', '2015-04-29 18:06:24', 40, 0, '0'),
(277, 'youtube', 0, 'https://www.youtube.com/watch?v=-CmadmM5cOk', 'Taylor Swift - Style', 'http://i1.ytimg.com/vi/-CmadmM5cOk/0.jpg', '2015-04-29 17:05:32', 3, 0, '0'),
(278, 'youtube', 0, 'https://www.youtube.com/watch?v=01ZoMrvT7HU', 'Disclosure - Help Me Lose My Mind (Mazde Remix) (VideoHUB)', 'http://i1.ytimg.com/vi/01ZoMrvT7HU/0.jpg', '2015-07-01 17:12:38', 4, 0, '0'),
(284, 'youtube', 0, 'https://www.youtube.com/watch?v=l8cL9Wag3TE', 'D12 - Rap Game', 'http://i1.ytimg.com/vi/l8cL9Wag3TE/0.jpg', '2015-06-29 14:58:16', 3, 944729, '0'),
(285, 'youtube', 0, 'https://www.youtube.com/watch?v=1axIFLTiQ_k', 'Eminem - Syllables feat. Jay-Z, Dr. Dre, 50 Cent, Stat Quo and Cashis', 'http://i1.ytimg.com/vi/1axIFLTiQ_k/0.jpg', '2015-06-26 17:15:54', 2, 453, '0'),
(286, 'youtube', 0, 'https://www.youtube.com/watch?v=kmJ6cJkkbg8', 'Eminem - Say My Name [MUSICVIDEO HD]', 'http://i1.ytimg.com/vi/kmJ6cJkkbg8/0.jpg', '2015-06-29 10:02:20', 2, 2685775, '0'),
(287, 'apple', 0, 'http://trailers.apple.com/trailers/wb/edgeoftomorrow/', 'Edge of Tomorrow ', '', '2015-07-06 09:31:27', 32, 0, '0'),
(288, 'soundcloud', 1, 'https://m.soundcloud.com/mmmusic/sam-smith-stay-with-me', 'Sam Smith -Stay With Me', 'https://i1.sndcdn.com/artworks-000075162259-15c2nd-t500x500.jpg', '2015-07-06 10:08:39', 5, 0, '0'),
(289, 'youtube', 0, 'https://www.youtube.com/watch?v=LCFz3isPdB4', 'D12 - My Band.', 'http://i1.ytimg.com/vi/LCFz3isPdB4/0.jpg', '2015-07-01 15:45:27', 6, 520690, '0'),
(290, 'youtube', 0, 'https://www.youtube.com/watch?v=QZXc39hT8t4', 'Dr. Dre - The Next Episode ft. Snoop Dogg, Kurupt, Nate Dogg', 'http://i1.ytimg.com/vi/QZXc39hT8t4/0.jpg', '2015-07-01 16:19:14', 5, 94547629, '0'),
(291, 'youtube', 0, 'https://www.youtube.com/watch?v=0WxDrVUrSvI', 'Lily Allen - Smile', 'http://i1.ytimg.com/vi/0WxDrVUrSvI/0.jpg', '2015-07-01 10:27:21', 4, 40006729, '0'),
(292, 'youtube', 0, 'https://youtube.com/watch?v=syRQ7iShpzY', 'Gordon Ramsey: How to treat a lady', 'http://i1.ytimg.com/vi/syRQ7iShpzY/0.jpg', '2015-07-06 09:31:18', 4, 1531227, '0'),
(293, 'youtube', 0, 'https://www.youtube.com/watch?v=oSqa7oe8tkA', 'Eminem Top 10 songs', 'http://i1.ytimg.com/vi/oSqa7oe8tkA/0.jpg', '2015-07-01 15:39:08', 2, 74498, '0'),
(294, 'vk', 0, 'http://vk.com/video?z=video-45960892_171214461', '', '', '2015-07-01 17:18:02', 1, 0, '0'),
(295, 'vevo', 0, 'http://www.vevo.com/watch/rihanna/Stay/USUV71300019', 'Stay', 'http://img.cache.vevo.com/Content/VevoImages/video/670DBA556C606BFFB454FCE989BF87B3.jpg', '2015-07-02 13:16:26', 1, 0, '0'),
(296, 'metacafe', 0, 'http://www.metacafe.com/watch/11238414/bacon_wrapped_sleigh_epic_meal_time/', '\r\n				Bacon-Wrapped Sleigh - Epic Meal Time								\r\n							\r\n		', 'http://s3.mcstatic.com/thumb/11238414/28947628/4/flash_player/0/1/bacon_wrapped_sleigh_epic_meal_time.jpg?v=1', '2015-07-06 10:43:17', 3, 0, '0'),
(297, 'adobe', 0, 'http://tv.adobe.com/embed/926/13308/', 'Basic workflow and terminology', 'https://thumbnails-tv.adobe.com//thumbs/13308/13308_generator_640x360.jpg', '2015-07-06 09:33:00', 2, 0, '0'),
(298, 'vevo', 0, 'http://www.vevo.com/watch/rita-ora/I-Will-Never-Let-You-Down-%28Teaser-2%29/TIVEV1429737', 'I Will Never Let You Down (Teaser #2)', 'http://img.cache.vevo.com/Content/VevoImages/video/230E445431D5B154AC4063A3BBCE14312014283916376.jpg', '2015-07-06 09:31:00', 2, 0, '0'),
(299, 'youtube', 0, 'https://www.youtube.com/watch?v=it_04dk_97E', 'Yelawolf - Best Friend ft. Eminem', 'http://i1.ytimg.com/vi/it_04dk_97E/0.jpg', '2015-07-02 16:18:18', 1, 17508344, '0'),
(300, 'youtube', 0, 'https://www.youtube.com/watch?v=ivjPQZ_3-xM', 'Eminem - Twisted ft (Explicit)  Skylar Grey &amp; Yelawolf', 'http://i1.ytimg.com/vi/ivjPQZ_3-xM/0.jpg', '2015-07-02 16:18:50', 1, 1238747, '0'),
(301, 'youtube', 0, 'https://www.youtube.com/watch?v=0AqnCSdkjQ0', 'Eminem - Guts Over Fear ft. Sia', 'http://i1.ytimg.com/vi/0AqnCSdkjQ0/0.jpg', '2015-07-02 16:19:23', 1, 73672035, '0'),
(302, 'youtube', 0, 'https://www.youtube.com/watch?v=JByDbPn6A1o', 'Eminem - Space Bound', 'http://i1.ytimg.com/vi/JByDbPn6A1o/0.jpg', '2015-07-02 16:19:54', 1, 150236651, '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
