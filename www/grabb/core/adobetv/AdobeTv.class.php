<?php

class AdobeTv extends Video {


	function __construct($url) {
		$this->videoLink = $url;
		$this->videoUrl = $url;
		$this->image = "http://tdc-images.s3.amazonaws.com/logo-adobe.jpg";
		$this->getVideoData();
	}

	function getVideoData() {
		$fullpage = curlGet($this->videoLink);
		$dom = new DOMDocument();

		@$dom->loadHTML($fullpage);
		$xpath = new DOMXPath($dom);
		$tags = $xpath->query('//script[@type="text/javascript"]');
		$arr = $tags->item(0)->nodeValue;

		$arr = str_replace("window.atv = {", "", $arr);	
		$arr = explode("\n,",$arr);
		$arr[8] = str_replace("html5player:", "", $arr[8]);		
		$arr = json_decode($arr[8]);	
		if ($arr->poster != "")
			$this->image = $arr->poster;
		$i = 0;
		
		$this->videoTitle = $arr->title;	
		foreach($arr->sources as $s) {
			if ($s->quality != "") {
				$size = get_size($s->src);
				$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/mp4&token='.($s->src);
				$this->sources[$s->quality] = array(
					"url" => $msg,
					"size" => $size,
					"type" => "video/mp4");				
			}		
		}			
	}

	function getTitle() {
		return $this->videoTitle;
	}

	function getSources() {
		return $this->sources;
	}

	function getStream() {
		return $this->sources;
	}

	function getImage() {
		return $this->image;
	}

}

?>