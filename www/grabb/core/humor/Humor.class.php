<?php

class Humor extends Video {

	function __construct($url) {
		$this->videoUrl = $url;
		$this->getVideoData();
	}

	function getVideoData() {
		//echo $this->videoUrl;
		preg_match("/(\d+)/", $this->videoUrl, $match);		
		$jsonfile = json_decode(curlGet("http://www.collegehumor.com/moogaloop/video/$match[0].json?context=&context_referer=&referer="));
		
		$jsonfile = $jsonfile->video;
		$this->videoTitle = $jsonfile->title;
		$this->image = $jsonfile->thumbnail;
		$this->streamUrl = array();
		$this->description = $jsonfile->description;
		$this->likes = $jsonfile->likes;
		if (isset($jsonfile->mp4)) {			
			foreach ($jsonfile->mp4 as $key => $value) {
				$size = get_size($value);
				$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/mp4&token='.($value);
				$this->streamUrl[$key] = array("url" => $msg, 
													"size" => get_size($value), 
														"type" => "video/mp4");
			}
		}
		if (isset($jsonfile->webm)) {
			foreach ($jsonfile->webm as $key => $value) {
				$size = get_size($value);
				$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/webm&token='.($value);
				$this->streamUrl[$key." "] = array("url" => $msg, 
													"size" => get_size($value), 
														"type" => "video/webm");
			}
		}
	}
}

?>