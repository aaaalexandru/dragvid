<?php

class Youporn extends Video {
	
	function __construct($url) {
		$this->videoUrl = $url;
		$this->getVideoData();

	}

	function getVideoData() {
		$fullpage = curlGet($this->videoUrl);
		//$fullpage = file_get_contents($this->videoUrl);
		$dom = new DOMDocument();
		$fullpage = str_replace("g:plusone", "gplusone", $fullpage);
		
		@$dom->loadHTML($fullpage);
		$xpath = new DOMXPath($dom); 
		$tags = $xpath->query('//ul[@class="downloadList"]/li');
		$arr = array();
		foreach ($tags as $key => $value) {
			//printout($value);
			$size = $tags->item($key)->lastChild->textContent;
			$size = str_replace("(", "", $size);
			$size = str_replace("MB)", "", $size);
			$size = trim($size);
			$type = substr($value->childNodes->item(2)->nodeValue, 0, 4);
			$arr[$value->childNodes->item(2)->nodeValue] = array("url" => $value->childNodes->item(2)->attributes->item(0)->value,
																 "size" => $size * 1024 *1024,
																 "type" => $type
																 );

		}

		$tags = $xpath->query('//img[@class="player-html5"]');
		$this->image = ($tags->item(0)->attributes->item(0)->value);

		$tags = $xpath->query('//h1[@class="grid_9"]');
		$this->videoTitle = ($tags->item(0)->nodeValue);

		$tags = $xpath->query('//div[@id="stats-views"]');
		$this->seen = ($tags->item(0)->lastChild->nodeValue);

		$tags = $xpath->query('//div[@id="stats-date"]');
		$this->uploaded = ($tags->item(0)->lastChild->nodeValue);
		
		$tags = $xpath->query('//div[@class="author-block--line"]/a');
		$this->uploader = ($tags->item(0)->nodeValue);
		
		$this->streamUrl = $arr;
	}


}