<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8"/>
<?php
 
$title = (empty($action) || $action == "/") ? "home" : $action;
//echo  $strings->meta->$action->title; 
?>  
<title><?=$strings->meta->$title->title;  ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link href="/owl-carousel/owl.theme.css" rel="stylesheet"/>
    
    <link href="/assets/css/custom.css" rel="stylesheet"/>
    
    <!-- Owl Carousel Assets -->
    <link href="/owl-carousel/owl.carousel.css" rel="stylesheet"/>
    
    <link rel="stylesheet" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/css/normalize.min.css"/>
    <link rel="stylesheet" href="/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/css/animate.css"/>
    <link rel="stylesheet" href="/css/templatemo_misc.css"/>
    
    <script src="//raw.github.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script>
    <script src="/js/jquery.twbsPagination.js"></script>
    <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
    <link rel="stylesheet" href="/css/main.css"/>
    <link rel="stylesheet" href="/css/templatemo_style.css"/>
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/js/ajax.js"></script>
</head>

   
<body>

    <!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
    
    

    

    
   