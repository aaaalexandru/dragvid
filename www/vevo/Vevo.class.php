<?php

class Vevo extends Video {

	function __construct($url) {
		$this->videoUrl = $url;
		$this->getVideoData();
	}
	//http://www.vevo.com/watch/sia/Chandelier-Official-Video/USRV81400135
	function getVideoData() {
		$token = $this->getToken();
		$xurl = explode("/", $this->videoUrl);
		$videoId = $xurl[6];
		$json = json_decode(curlGet("https://apiv2.vevo.com/video/$videoId?token=".$token));

		$this->image = $json->thumbnailUrl;
		$this->videoTitle = $json->title;
		//printout($json->versions);
		$arr = array();
		foreach ($json->versions as $key => $value) {
			if (($value->type != "Youtube") && ($value->type != "Smooth Streaming"))
				foreach ($value as $keys => $data) {
					//echo $data->type . $data->version."<br/>"; 
					if (is_object($data)|| is_array($data)) {
						$arr[$value->type.$value->version] = array(); 
						foreach ($data as $k => $v) {
							//printout($v);
							if ($value->type.$value->version == "Android3") {
								$size = get_size($v->url);
								$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/mp4&token='.($v->url);
								$arr[$value->type.$value->version][$v->renditionType] = array("url" => $msg,
									"size" => $size, "type" => "video/mp4");

								}
								//array_push($arr[$value->type.$value->version] , (array($v->renditionType =>
								//array("url" => "test"))));
						}
					}
					//printout($data);
				}
		}

		//printout($json->versions);
		//printout($arr);
		$this->streamUrl = $arr['Android3'];
	}

	function getToken() {
		$url = "https://stg-apiv2.vevo.com/oauth/token";                                                                    
		$data_string = '{"client_id":"e962a4ae0b634065b774729ee601a82b","client_secret":"9794fb3bcd4b47488380c2bc9e5ef618","grant_type":"client_credentials","country":"US","locale":"en-us"}';    
		$data = array('post_data' => $data_string);
		$ch = curl_init('https://stg-apiv2.vevo.com/oauth/token');  
		curl_setopt($ch,CURLOPT_POST, 1);                                                                    
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',
		    'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:37.0) Gecko/20100101 Firefox/37.0',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                       
		);
		$result = json_decode(curl_exec($ch));
		//printout($result);  	
		return $result->access_token;
	}
}

?>