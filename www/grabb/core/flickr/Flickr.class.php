<?php


class Flickr extends Video {
	// private $videoLink;
	// private $videoTitle;
	// private $streamUrl;
	 protected $videoId;
	// private $image;


	function __construct($url) {
		$this->videoLink = $url;
		$this->getVideoData();
	}

	function getVideoData() {
		$fullpage = curlGet($this->videoLink);
		$dom = new DOMDocument();
		@$dom->loadHTML($fullpage);
		$xpath = new DOMXPath($dom); 

		$tags = $xpath->query('//meta[@property="og:description"]');
		$this->description = $tags->item(0)->attributes->item(1)->value;

		

		$tags = $xpath->query('//span[@class="full-text"]');
		$this->seen = $tags->item(0)->nodeValue;

		$tags = $xpath->query('//div[@id="photo-story-story"]/ul/li');
		$this->uploaded = $tags->item(0)->childNodes->item(2)->nodeValue;
		
		//echo $fullpage;
		$secret = strstr($fullpage, "photo_secret:");
		$secret = str_replace("photo_secret: '", "", $secret);
		$secret = strstr($secret, "'", true);

		//scoatem id-u
		$id = strstr($fullpage, "id: ");
		$id = str_replace("id: ", "", $id);
		$this->videoId = substr($id, 0, strpos($id, ","));

		//thumbul
		$thumb = strstr($id, "baseURL: '");
		$thumb = str_replace("baseURL: '", "", $thumb);
		$this->image = strstr($thumb, "'", true);
		
		//apiKey
		$apiKey = stristr($fullpage, "api_key");
		$apiKey = str_replace('api_key":"', "", $apiKey);
		$apiKey = substr($apiKey, 0, strpos($apiKey, '"'));		

		//titlu
		$title = strstr($fullpage, 'title":"');
		$title = str_replace('title":"', "", $title);
		$this->videoTitle = strstr($title, '"', true);	

		$re1='.*?';	# Non-greedy match on filler
		$re2='(\\d+)';	# Integer Number 
		preg_match_all("/".$re1.$re2."/is", $this->videoLink, $matches);		
		$jsonurl = "https://api.flickr.com/services/rest?photo_id=".$matches[1][0]."&secret=$secret&viewerNSID=&method=flickr.video.getStreamInfo&csrf=&api_key=$apiKey&format=json&hermes=1&hermesClient=1&reqId=bbarlkt&nojsoncallback=1";
		$jsondata = json_decode(curlGet($jsonurl));
		$jsondata = $jsondata->streams->stream;
		
		for($i=0;$i<count($jsondata);$i++) {
			$size = get_size($jsondata[$i]->_content);
			$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/mp4&token='.($jsondata[$i]->_content);
			$arr[$jsondata[$i]->type] = array("url" => $jsondata[$i]->_content, 
													"size" => $size,
													"type" => "video/mp4" 
												);

			
		}
		$this->streamUrl = $arr;
		unset($id);
		unset($fullpage);
		unset($thumb);
		unset($title);
	}

	function getStream() {
		return $this->streamUrl;
	}

	function getLink() {
		return $this->videoLink;
	}

	function getTitle() {
		return $this->videoTitle;
	}

	function getImage() {
		return $this->image;
	}

	function getId() {
		return $this->videoId;
	}


}

?>