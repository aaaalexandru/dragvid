<?php
include "inc/simple_html_dom.php";
class Twitter extends Video {


	function __construct($url) {
		$this->videoUrl = $url;		
		$this->videoTitle = "videoFromTwitter";	
		$this->getVideoData();
	}

	function getVideoData() {
		$fullpage = curlGet($this->videoUrl);
		$dom = new DOMDocument();
		@$dom->loadHTML($fullpage);
		$xpath = new DOMXPath($dom); 		

		//echo $fullpage;
		$html = str_get_html($fullpage);
		$datalink = $html->find("div[data-amplify-playlist-url]",0);
		$datalink = $datalink->attr['data-amplify-playlist-url'];
		$framelink = $html->find("div[data-full-card-iframe-url]",0);
		$framelink = "https://twitter.com".$framelink->attr['data-full-card-iframe-url'];
		$this->description = trim($html->find("p[class=js-tweet-text tweet-text]",0)->plaintext);
		//printout($html->find("p[class=js-tweet-text tweet-text]",0));

		$framepage = curlGet($framelink);
		$framepage = str_get_html($framepage);

		$image = $framepage->find("div[data-player-config]",0);
		$image = $image->attr['data-player-config'];
		$image = explode(",", $image);
		$image = substr($image[0], 34, strlen($image)-6);	
		$image = str_replace("\\", "", $image);
		//$image = substr($image, 0)
		$this->image = $image;	
		$this->videoTitle = $html->find("meta[property=og:title]",0)->getAttribute('content');		
		
		$xml = curlGet($datalink);
		//echo htmlspecialchars($xml);
		$xml = str_replace(":", "", $xml);
		$xml = str_replace("<![CDATA[", "", $xml);
		$xml = str_replace("]]>", "", $xml);
		$xml = simplexml_load_string($xml);
		$videoLink = trim(($xml->vmapExtensions->vmapExtension->twamplify->twcontent->MediaFiles->MediaFile));
		$videoLink = str_replace("http//", "http://", $videoLink);
		$this->size = get_size($videoLink);
		$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$this->size.'&mime=video/mp4&token='.($videoLink);
		$this->streamUrl = array( "Norm" => array(
											"url" => $msg,
											"size" => $this->size,
											"type" => "video/mp4")
										);		
	
	}


	function getTitle() {
		return $this->videoTitle;
	}

	function getVideoUrl() {
		return $this->videoUrl;
	}

	function getStream() {
		return $this->streamUrl;
	}

	function getImage() {
		return $this->image;
	}

	function getSize() {
		return $this->size;
	}

}

?>