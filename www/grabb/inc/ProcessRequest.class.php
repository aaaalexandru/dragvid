<?php
/*

intro pagina goala, fara careva informatie tiparita, se va initializa obiectului acestei clase.
initializarea se face cu 2 parametri, url la video si keya publica a aplicatiei

in momentul initializarii automat se va afisa rezultatul, o structura json codificata dupa keya secreta, 
care este extrasa din baza de date dupa keya publica.

p.s: pe pagina unde este initializata aceasta clasa, ascundeti toate erorile care este posibil sa apara.

algoritmul de criptare MCRYPT_RIJNDAEL_256
keya publica si secreta trebu sa fie din 32 de caractere, de genu MD5

*/

class ProcessRequest {

	protected $key;
	private $secret;
	public $url;
	public $json;
	protected $video;
	
	function __construct($url, $key) {
		$this->url = $url;
		$this->key = $key;
		$this->getSecret();

		if ($this->secret) {
			
			$this->getData();

			$this->makeJson();
		}
	}

	function getSecret() {
		global $db;
		$sql = "SELECT *
	              FROM `api_keys`
	                  WHERE `key` = '".$this->key."';";
	  
	    if(!$result = $db->query($sql)){
	    	die('There was an error running the query [' . $db->error . ']');
	    }
	    $row = $result->fetch_assoc();	
	    $this->secret = $row['secret'];
	    unset($row);
	}

	function getData() {
		$this->video = getObject($this->url);		
	}

	function makeJson() {

		$json = "{";
			$json .= '"title":"'.$this->video->getTitle().'",';
			$json .= '"url":"'.$this->video->getVideoUrl().'",';
			$json .= '"image":"'.$this->video->getImage().'",';
			$json .= '"description":"'.$this->video->getDescription().'",';
			$json .= '"uploaded":"'.$this->video->getUploaded().'",';
			$json .= '"uploader":"'.$this->video->getUploader().'",';
			$json .= '"seen":"'.$this->video->getSeen().'",';
			$json .= '"likes":"'.$this->video->getLikes().'",';
			$json .= '"stream":{}';
		$json .= "}";
		$json = json_decode($json);
		$json->stream = $this->video->getStream();
		//echo(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->secret, json_encode($json), MCRYPT_MODE_ECB)));
		
		echo(json_encode($json));


		//printout(json_decode(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, '28e336ac6c9423d946ba02d19c6a2632', base64_decode($encrypted), MCRYPT_MODE_ECB)));

	}




}
?>

