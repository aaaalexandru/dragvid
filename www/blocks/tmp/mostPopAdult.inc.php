<script>
    function download(link) {
        document.getElementById("videoLink").value = link;       
        document.getElementById("downloadForm").submit();
    }
</script>

<div class="row ">
  <div class="col-sm-12 adultbutton">
      <a href="/<?=$lang;?>/" ><?=$strings->strings->notshowforadults;?></a>
  </div>
</div>

<div class="row" id="left-column">
      <div class="col-sm-5 granita">
        <h2 class="latest"><?=$strings->strings->last;?> video</h2>
        <ul>
          <?php
            //lastest download
            $sql = "SELECT * FROM `downloads` WHERE `genre` = 2 ORDER BY id DESC LIMIT 0,10;";
            if(!$result = $db->query($sql)){
              die('There was an error running the query [' . $db->error . ']');
            }




            while($row = $result->fetch_assoc()) {  


              $minutes = round((time() - strtotime($row['date'])) / 62.2);
              
               if ($minutes == 0) {
                  $time = $strings->strings->lessThenMinuteAgo;
                } else

                if ($minutes == 1) {
                  $time = $strings->strings->aboutAMinuteAgo;
                } else
                
                if (($minutes > 1) && ($minutes < 60)) {
                  $time = $minutes .$strings->strings->minutesAgo;
                } else 

                if ($minutes > 60) {
                  $minutes = round($minutes / 60);
                  if ($minutes == 1) {
                    $time = $strings->strings->aboutAHourAgo;
                  } else 

                  if ($minutes > 1) {
                    $time = $strings->strings->about." ".$minutes . " " .$strings->strings->hoursAgo;
                  }
              }


          ?>
          <li>
            <ul class="video-hover">
              <li class="content-description">
                <a class="last" href="#">
                  <div class="img-clip" id="adltimg" onclick='download("<?=$row['link'];?>")'><img src="<?=$row['image'];?>">
                    <div class="title-site" id="titlsiteadlt"><?=trim($row['site']);?></div>
                  </div>
                  
                  <div> 
                    <strong class="box">
                      <span class="description-video">
                        <p onclick='download("<?=$row['link'];?>")'><i class="fa fa-film film"></i> <?=trim($row['title']);?></p>
                      </span>
                      <em class="time">
                        <p>
                          <i class="fa fa-download"></i><?=$row['counts'];?>
                          <i class="fa fa-long-arrow-right"></i><?=$time;?>
                        </p>
                        <!-- <p><i class="fa fa-tachometer"></i> 3min 50sec</p> -->
                      </em>
                    </strong>
                  </div>
                </a>    
              </li>
            </ul>       
          </li>
          <?php
            }//end while

          ?>
   

        </ul>
      </div>


      <div class="col-sm-5" >
        <h2 class="latest"><?=$strings->strings->mostpopular;?></h2>
        <ul>

          <?php
            $sql = "SELECT * FROM `downloads` 
                        WHERE `date` > 
                        now() - INTERVAL 1 DAY and `genre` = 2 
                          ORDER BY `counts` DESC
                          LIMIT 0, 10
                        ";
            if(!$result = $db->query($sql)){
              die('There was an error running the query [' . $db->error . ']');
            }            
            while($row = $result->fetch_assoc()) {

               $minutes = round((time() - strtotime($row['date'])) / 62.2);
              
               if ($minutes == 0) {
                  $time = $strings->strings->lessThenMinuteAgo;
                } else

                if ($minutes == 1) {
                  $time = $strings->strings->aboutAMinuteAgo;
                } else
                
                if (($minutes > 1) && ($minutes < 60)) {
                  $time = $minutes .$strings->strings->minutesAgo;
                } else 

                if ($minutes > 60) {
                  $minutes = round($minutes / 60);
                  if ($minutes == 1) {
                    $time = $strings->strings->aboutAHourAgo;
                  } else 

                  if ($minutes > 1) {
                    $time = $strings->strings->about." ".$minutes . " " .$strings->strings->hoursAgo;
                  }
              }
          ?>
          <li>
            <ul class="video-hover">
              <li class="content-description">
                <a class="last"  href="#" >
                  <div class="img-clip" id="adltimg" onclick='download("<?=$row['link'];?>")'><img src="<?=$row['image'];?>">
                    <div class="title-site" id="titlsiteadlt"><?=$row['site'];?></div>
                  </div>
                  
                  <strong class="box">
                    <span class="description-video">
                      <p onclick='download("<?=$row['link'];?>")'><i class="fa fa-film film" ></i> <?=$row['title'];?></p>
                    </span>
                    <em class="time">
                      <p>
                        <i class="fa fa-download"></i><?=$row['counts'];?>
                        <i class="fa fa-long-arrow-right"></i><?=$time;?>
                      </p>
                      <!-- <p><i class="fa fa-tachometer"></i> 8min 55sec</p> -->
                    </em>
                  </strong>
                </a>    
              </li>
            </ul>       
          </li>
          <?php
            }//end while
          ?>
        </ul>
      </div>

      
    </div>