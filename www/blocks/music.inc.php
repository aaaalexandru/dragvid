<div class="content-section container sites-block">
	<div class="aside-block col-md-2 col-sm-2">
		<div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#dropdown-navigation" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div> <!--/navbar-header-->
        <nav id="dropdown-navigation"class="collapse navbar-collapse" aria-expanded="true" style="height:200px;">
    		<ul class="sites-list nav navbar-nav sm sm-collapsible">
    			<li class="site-name active"><a onclick="getDownloadedMusic('')">All</a></li><!-- 
    			 --><li class="site-name"><a onclick="getDownloadedMusic('soundcloud')">soundcloud</a></li><!-- 	        			 
    			 --><li class="site-name"><a onclick="getDownloadedMusic('mixcloud')">mixcloud</a></li>
    		</ul>
    	</nav>
	</div>
	<div class="right-block col-md-10 col-sm-10">
		
		<div id="gallery" class="row">
			<div class="col-md-12">
				<div id="response"></div>
        		
            </div> <!--/col-md-12-->
			<!--<div  class="col-md-12 pagination-block">
	            <ul class="pagination" >
				   <li><a href="#">&laquo;</a></li>
				   <li><a href="#">1</a></li>
				   <li><a href="#">2</a></li>
				   <li><a href="#">3</a></li>
				   <li><a href="#">4</a></li>
				   <li><a href="#">&raquo;</a></li>
				</ul>
        	</div> -->
			
        </div> <!--/row-->
        <div id="gallery-image"></div>

	</div> <!--/right-block-->
</div> <!--/content-section-->