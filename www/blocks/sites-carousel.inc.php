 <!-- CAROUSEL -->

        <div id="services" class="content-section container">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="supported-sites"><?=$strings->strings->available;?></h5>

                        <div id="myCarousel" class="carousel slide" data-ride="">

                            <div class="carousel-inner container" role="listbox">
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                     <a class="col-md-3 col-sm-3" href="/<?php echo ($defaultLang == $lang)? "" : $lang."/"; ?>sites/youtube">
                                            <img src="/images/youtube.png" alt="">
                                        </a> <!--/col-md-3 col-sm-3-->
                                        
                                        <a class="col-md-3 col-sm-3" href="/<?php echo ($defaultLang == $lang)? "" : $lang."/"; ?>sites/apple">
                                            <img src="/images/apple.png" alt="">
                                        
                                        </a> <!--/col-md-3 col-sm-3-->

                                        <a class="col-md-3 col-sm-3" href="/<?php echo ($defaultLang == $lang)? "" : $lang."/"; ?>sites/facebook">
                                            <img src="/images/facebook.png" alt="">
                                            
                                        </a> <!--/col-md-3 col-sm-3-->

                                        <a class="col-md-3 col-sm-3" href="/<?php echo ($defaultLang == $lang)? "" : $lang."/"; ?>sites/instagram">
                                            <img src="/images/instagram.png" alt="">
                                        </a> <!--/col-md-3 col-sm-3-->

                                        
                                        
                                    </div> <!--/item-->

                                    <div class="item">

                                        <a class="col-md-3 col-sm-3" href="/<?php echo ($defaultLang == $lang)? "" : $lang."/"; ?>sites/vimeo">
                                            <img src="/images/vimeo.png" alt="">
                                        </a> <!--/col-md-3 col-sm-3-->
                                        
                                        <a class="col-md-3 col-sm-3" href="/<?php echo ($defaultLang == $lang)? "" : $lang."/"; ?>sites/vine">
                                            <img src="/images/vine.png" alt="">
                                        </a> <!--/col-md-3 col-sm-3-->

                                        <a class="col-md-3 col-sm-3" href="/<?php echo ($defaultLang == $lang)? "" : $lang."/"; ?>sites/twitter">
                                            <img src="/images/twitter.png" alt="">
                                        </a> <!--/col-md-3 col-sm-3-->

                                        <a class="col-md-3 col-sm-3" href="/<?php echo ($defaultLang == $lang)? "" : $lang."/"; ?>sites/twitch">
                                            <img src="/images/twitch.png" alt="">
                                        </a> <!--/col-md-3 col-sm-3-->
                                    </div> <!--/item-->
                                    
                                    <div class="item">

                                        <a class="col-md-3 col-sm-3" href="/<?php echo ($defaultLang == $lang)? "" : $lang."/"; ?>sites/blip">
                                            <img src="/images/blip.png" alt="">
                                        </a> <!--/col-md-3 col-sm-3-->
                                        
                                        <a class="col-md-3 col-sm-3" href="/<?php echo ($defaultLang == $lang)? "" : $lang."/"; ?>sites/collegehumor">
                                            <img src="/images/collegehumor.png" alt="">
                                        </a> <!--/col-md-3 col-sm-3-->

                                        <a class="col-md-3 col-sm-3" href="/<?php echo ($defaultLang == $lang)? "" : $lang."/"; ?>sites/adobe">
                                            <img src="/images/adobe.png" alt="">
                                        </a> <!--/col-md-3 col-sm-3-->

                                        <a class="col-md-3 col-sm-3" href="/<?php echo ($defaultLang == $lang)? "" : $lang."/"; ?>sites/metacafe">
                                            <img src="/images/metacafe.png" alt="">
                                        </a> <!--/col-md-3 col-sm-3-->
                                    </div> <!--/item-->
                                    <div class="item">
                                        <a class="col-md-3 col-sm-3" href="/<?php echo ($defaultLang == $lang)? "" : $lang."/"; ?>sites/mixcloud">
                                            <img src="/images/mixcloud.png" alt="">
                                            
                                        </a> <!--/col-md-3 col-sm-3-->
          
                                        
                                    </div> <!--/item-->


                                </div> <!--/.carousel-inner-->

                                <!-- Left anf Right controls -->
                                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    
                                </a>
                                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                </a>
                            </div> <!--/carousel-inner-->
                            
                        </div><!--/myCarouse-->
                    </div> <!--col-md-12-->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /#services -->
       <!-- /CAROUSEL -->