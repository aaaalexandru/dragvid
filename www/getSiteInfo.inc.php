<?php
//load info sites
switch ($lang) {
  case 'en':
    $page = file_get_contents('resource/sitesEnglish.xml');
    break;

  case 'vi':
    $page = file_get_contents('resource/sitesVietnam.xml');
    break;

  case 'es':
    $page = file_get_contents('resource/sitesSpain.xml');
    break;
  
  default:
    $page = file_get_contents('resource/sitesEnglish.xml');
    break;
}

$sitesInfo = simplexml_load_string($page);
$siteInfo = "";

foreach ($sitesInfo->sites->site as $key => $value) {
  if (strpos($value->id, $module)) {
    $siteInfo = $value;
    break;
  }
}

if ($siteInfo == "") {
  foreach ($sitesInfo->adultSites->site as $key => $value) {
    if (strpos($value->id, $module)) {
      $siteInfo = $value;
      break;
    }
  }
}

?>

<div class="row sites">
        <div class="col-sm-7">
          <h2><?=$siteInfo->name;?></h2>
          <p class="descriereSites"><?=$siteInfo->intro;?></p>
        </div>
        <div class="col-sm-5">
          <img class="alignImg" src="/images/logo<?=$siteInfo->id;?>.png" width="200">
        </div>
        </div>

        <?php
          foreach ($siteInfo->examples->example as $key => $value) {
            //printout($value);
          
        ?>

        <div class="url hr">
          <div class="row">
          <div class="col-sm-1">
            <p><?=$value->name;?></p>
          </div>
          <div class="col-sm-11">
            <p onclick='download("<?=$value->link;?>")'><?=$value->model;?><br>
              e.g. <a href="#" ><?=$value->link;?></a></p>  
          </div>
          </div>
      </div> 
      <?php
        } // end foreach
      ?>
<script>
    function download(link) {
        document.getElementById("videoLink").value = link;       
        document.getElementById("downloadForm").submit();
    }
</script>
  <div id="products" class="content-section container">
    <div class="row">
      <div class="container-fluid">
        <div class="row hr description">
          <div class="col-sm-6">
            <h2><?=$strings->strings->htdwn;?> <?=$siteInfo->name;?>?</h2>
            
            <?php 
            if ($lang == 'en') {
            ?>
              <ol>
                <li>Choose any video you like</li>
                <li>Paste the link to the input on this page</li>
                <li>Select a format</li>
                <li>Wait until the video will be downloaded</li>
                <li> Play in your favorite player. If it doesn't work, use VLC </li>
              </ol>
            <?php } ?>

             <?php 
            if ($lang == 'es') {
            ?>
              <ol>
                <li>Choose any video you like</li>
                <li>Paste the link to the input on this page</li>
                <li>Select a format</li>
                <li>Wait until the video will be downloaded</li>
                <li> Play in your favorite player. If it doesn't work, use VLC </li>
              </ol>
            <?php } ?>

             <?php 
            if ($lang == 'vi') {
            ?>
              <ol>
                <li>Choose any video you like</li>
                <li>Paste the link to the input on this page</li>
                <li>Select a format</li>
                <li>Wait until the video will be downloaded</li>
                <li> Play in your favorite player. If it doesn't work, use VLC </li>
              </ol>
            <?php } ?>


          </div> <!--/col-sm-6-->        
        </div> <!--/row hr description-->
      </div> <!--/container-fluid-->
    </div> <!--/row-->
  </div> <!--/#products-->