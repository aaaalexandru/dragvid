<?php

include "curl.inc.php";
include "Video.class.php";
include "dbConnection.inc.php";



function getMB($size) {
	return number_format(($size / 1024 / 1024) , 1, '.', '');
}

function getObject($url) {
	
		$object = null;
		if (strpos($url, "blip.tv")) {			
			include "blip.tv/Blip.class.php";
			$object = new Blip($url);
			return $object;
		}

		if (strpos($url, "vevo")) {			
			include "vevo/Vevo.class.php";
			$object = new Vevo($url);
			return $object;
		}

		if (strpos($url, "collegehumor.com")) {
			include "humor/Humor.class.php";
			$object = new Humor($url);
			return $object;
		}

		if (strpos($url, "twitch.tv")) {
			include "twitch/Twitch.class.php";
			$object = new Twitch($url);
			return $object;
		}

		if (strpos($url, "vk.com")) {
			include "vk/Vk.class.php";
			$object = new Vk($url);
			return $object;
		}

		if (strpos($url, "instagram.com")) {
			include "instagram/Instagram.class.php";
			$object = new Instagram($url);
			return $object;
		}

		if (strpos($url, "twitter.com")) {
			include "twitter/Twitter.class.php";
			$object = new Twitter($url);
			return $object;
		}
		
		if (strpos($url, "facebook")) {
			include "facebook/Facebook.class.php";
			$object = new Facebook($url);
			return $object;
		}

		if (strpos($url, "flick")) {
			include "flickr/Flickr.class.php";
			$object = new Flickr($url);
			return $object;
		}

		if (strpos($url, "mixcloud.com")) {
			include "mixcloud/Mixcloud.class.php";
			$object = new Mixcloud($url);
			return $object;
		}

		if (strpos($url, "ted.com")) {
			include "ted/Ted.class.php";
			$object = new Ted($url);
			return $object;
		}

		if (strpos($url, "adobe.com")) {
			include "adobetv/AdobeTv.class.php";
			$object = new AdobeTv($url);
			return $object;
		}

		if (strpos($url, "vine.co")) {
			include "vine.co/Vine.class.php";
			$object = new Vine($url);
			return $object;
		}

		if (strpos($url, "metacafe.com")) {
			include "metacafe/MetaCafe.class.php";
			$object = new MetaCafe($url);
			return $object;
		}

		if (strpos($url, "apple.com")) {
			include "apple/Apple.class.php";
			$object = new Apple($url);
			return $object;
		}

		if (strpos($url, "soundcloud")) {
			include "soundcloud/Soundcloud.class.php";
			$object = new Soundcloud($url);
			return $object;
		}

		if (strpos($url, "vimeo.com")) {
			include "vimeo/Vimeo.class.php";
			$object = new Vimeo($url);
			return $object;
		}

		if (strpos($url, "youtube.com")) {
			include "core/ytd/Youtube.class.php";
			$object = new Youtube($url);
			return $object;
		}

		if (strpos($url, "youporn.com")) {
			include "youporn/Youporn.class.php";
			$object = new Youporn($url);
			return $object;
		}

		if (strpos($url, "xhamster.com")) {
			include "xhamster/Xhamster.class.php";
			$object = new Xhamster($url);
			return $object;
		}

		if (strpos($url, "xvideos.com")) {
			include "xvideos/Xvideos.class.php";
			$object = new Xvideos($url);
			return $object;
		}

		if (strpos($url, "xtube.com")) {
			include "xtube/Xtube.class.php";
			$object = new Xtube($url);
			return $object;
		}

		if (strpos($url, "redtube.com")) {
			include "redtube/Redtube.class.php";
			$object = new Redtube($url);
			return $object;
		}


	
}




?>