<div class="content-section container sites-block">
	<div class="aside-block col-md-2 col-sm-2">
		<div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#dropdown-nav" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div> <!--/navbar-header-->
        <nav id="dropdown-nav"class="collapse navbar-collapse" aria-expanded="true" style="height:auto; width:100%;">
    		<ul class="sites-list nav navbar-nav sm sm-collapsible">
    			<li class="site-name active"><a onclick="getDownloaded('')">All</a></li><!-- 
    			 --><li class="site-name"><a onclick="getDownloaded('youtube')">youtube</a></li><!-- 
    			 --><li class="site-name"><a onclick="getDownloaded('vimeo')">vimeo</a></li><!-- 
    			 --><li class="site-name"><a onclick="getDownloaded('facebook')">facebook</a></li><!-- 
    			 --><li class="site-name"><a onclick="getDownloaded('twitter')">twitter</a></li><!-- 
    			 --><li class="site-name"><a onclick="getDownloaded('instagram')">instagram</a></li><!-- 
    			  
    			 --><li class="site-name"><a onclick="getDownloaded('vine')">vine</a></li><!-- 
    			 --><li class="site-name"><a onclick="getDownloaded('twitch')">twitch</a></li><!-- 
    			 
    			
    			 --><li class="site-name"><a onclick="getDownloaded('trailers')">trailers</a></li><!-- 
    			 --><li class="site-name"><a onclick="getDownloaded('blip')">blip</a></li><!-- 
    			 --><li class="site-name"><a onclick="getDownloaded('collegehumor')">collegehumor</a></li><!-- 
    			  
    			 --><li class="site-name"><a onclick="getDownloaded('metacafe')">metacafe</a></li>


    		</ul>
    	</nav>
	</div> <!--/.aside-block-->
	<div class="right-block col-md-10 col-sm-10">
		
		<div id="gallery" class="row col-md-12">
			
				<div id="response">
				</div>
				<?php
					// include "ajax/getDownloadedUrls.php";
              	?>

        </div>        
            
      <!--  <div  class="col-md-12 pagination-block">
            <ul class="pagination" >
			   <li><a href="#">&laquo;</a></li>
			   <li><a href="#">1</a></li>
			   <li><a href="#">2</a></li>
			   <li><a href="#">3</a></li>
			   <li><a href="#">4</a></li>
			   <li><a href="#">&raquo;</a></li>
			</ul>
        </div> -->
			
        
        <div id="gallery-image"></div>

	</div> <!--/right-block-->
</div> <!--/content-section-->
