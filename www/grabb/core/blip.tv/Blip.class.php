<?php
class Blip extends Video {

	function __construct($url) {
		$this->videoUrl = $url;
		$this->getVideoData();
	}

	function getVideoData() {
		$fullpage = curlGet($this->videoUrl);
		$dom = new DOMDocument();
		@$dom->loadHTML($fullpage);
		$xpath = new DOMXPath($dom); 

		//parsam linku la rss
		$tags = $xpath->query('//link[@type="application/rss+xml"]');
		if ($tags->item(0)->attributes->item(3)->name != 'href') {
			exit("Video is not available for download.");
		}
		$rssUrl = "http://blip.tv".$tags->item(0)->attributes->item(3)->nodeValue;
		//trajim rss-u
		$fullpage = 
					str_replace("@", "", 
					str_replace("http//", "http://", 
					str_replace(":", "", curlGet($rssUrl))));
		
		$xml = simplexml_load_string($fullpage);
		$this->description = str_replace("<br />", "", strstr($xml->channel->item->description, "<br />"));
		$this->uploader = $xml->channel->title;
		//channel item mediagroup mediacontent
		$this->videoTitle = $xml->channel->item->title;
		$this->image = $xml->channel->item->mediathumbnail['url'];
		$this->streamUrl = $xml->channel->item->mediagroup;
		$arr = array();
		//printout($xml);
		$i = 0;
		foreach ($this->streamUrl->mediacontent as $key => $value) {
			$array['url'] = (array)$value['url'][0];
			$array['url'][0] .= "?showplayer=20140930271112420150211135531&referrer=http://blip.tv&mask=7&skin=flashvars&view=url";
			$url = $array['url'][0];
			$size = (array) $value['fileSize'];
			$type = (array) $value['type'];
			$msg = str_replace("message=", "", curlGet($url));	
			$quality = (array) $value['bliprole'];	
			$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size[0].'&mime='.$type[0].'&token='.($msg);	
			$arr[$quality[0]] = array("url" =>  $msg, "size" => $size[0], "type" => $type[0]);			
			$i++;
		}
		$this->streamUrl = $arr;		
		
	}

}

?>