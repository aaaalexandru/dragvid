<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

set_error_handler('myErrorHandler');
register_shutdown_function('fatalErrorShutdownHandler');

$arrayLanguage  = array('en' => 'English','ro' => 'Română', 'it' => 'Italiano', 'es' => 'Español', 'ru' => 'Русский');
$validLanguage  = array('en','ro','it','es','ru');
$validAction = array('download','video','music','sites','api','404');
function validLanguage($lang)
{
   global $validLanguage;
   foreach( $validLanguage as $val ){
      if($val == $lang) return true;
         //echo "<br>lang = ".$lang;
   }
   return false;
}


function validAction($action)
{
   global $validAction;
   foreach( $validAction as $val ){
      if($val == $action) return true;
         //echo "<br>lang = ".$lang;
   }
   return false; 
}



function fullLanguage($shortLang){

	global $arrayLanguage;
	return $arrayLanguage[$shortLang];
}

function error_404()
{
global $lang, $defaultLang,$action;
//header("HTTP/1.0 404 Not Found");
 
$link = ($lang != $defaultLang) ? (validAction($action) ? "Location: /".$lang."/".$action : "Location: /".$lang ) : 
   ((validAction($action)) ?
    ($action == 'download') ? 
      "Location: /".$lang."/".$action :
(($module != '' &&  (in_array($module, $availableSites))) ?
  
 "Location: /".$action."/".$module  : 
 "Location: /"   ) :  
"Location: /"   );

   //header($link);
   //include "blocks/404.inc.php";
   exit;
}

function myErrorHandler($code, $message, $file, $line) {

	if($code != 2) {
      global $lang, $defaultLang,$action;
		//echo $code . $message . $file . $line;
		//error_404();
      //if($action != 'download')   //e necesar de cizelat pagina download cind nu se descarca nimic
$link = ($lang != $defaultLang) ? (validAction($action) ? "Location: /".$lang."/".$action : "Location: /".$lang ) : 
   ((validAction($action)) ?
    ($action == 'download') ? 
      "Location: /".$lang."/".$action :
(($module != '' &&  (in_array($module, $availableSites))) ?
  
 "Location: /".$action."/".$module  : 
 "Location: /"   ) :  
"Location: /"   );
         //echo "<script> window.location.pathname = '".$link."';</script>";
      
		exit;
		//header("Location: /$lang/404");  
		//echo "<meta http-equiv='refresh' content='0; url=/$lang/404' />";
				
	}
}

function fatalErrorShutdownHandler()
{
  $last_error = error_get_last();
  if ($last_error['type'] === E_ERROR) {
    // fatal error
    myErrorHandler(E_ERROR, $last_error['message'], $last_error['file'], $last_error['line']);
  }
}
//print_r($_POST);
include "inc/util.inc.php";
$defaultLang = $lang = 'en';

$action = '';
// Массив параметров из URI запроса.
$params = array();

//toate siteurile suportabile
global $availableSites;

$availableSites = array(						
						"youporn",
						"xhamster",
						"xvideos",
						"xtube",
						"redtube",
						"youtube",
						"vimeo",
						"facebook",
						"twitter",
						"ted",
						"instagram",
						"flickr",
						"vevo",
						"vine",
						"twitch",
						"blip",
						"collegehumor",
						"apple",
						"adobe",
						"soundcloud",
						"metacafe",
						"mixcloud"
					);
$link = "";
$siteIsset = false;
$site = "";

if (isset($_POST['link'])) {
	$link = $_POST['link'];	
	foreach ($availableSites as $value) {
		if (strpos($link, $value)) {
			$site = $value;
			$siteIsset = true;
			break;
		}
	}
	
}



// Если запрошен любой URI, отличный от корня сайта.
if ($_SERVER['REQUEST_URI'] != '/') 
{
	try {
		$url_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
		
		// Разбиваем виртуальный URL по символу "/"
		$uri_parts = explode('/', trim($url_path, ' /'));
      //print_r($uri_parts);
      if($uri_parts[0] == null) error_404();
      if (strlen($uri_parts[0]) == 2 &&  validLanguage($uri_parts[0])){
      $lang = array_shift($uri_parts); // limba
      $url_lang = $lang;
      } else $url_lang = null;
      
      $action = array_shift($uri_parts); // actiunea
		$module = array_shift($uri_parts); // primim modulu
		// Получили в $params параметры запроса
		for ($i=0; $i < count($uri_parts); $i++) {
			$params[$uri_parts[$i]] = $uri_parts[++$i];
		}
      
      
	} catch (Exception $e) {
		
		$module = '404';
		$action = '404';
	}
} else {

   $action = "";
   $module = "";
   
   
}


switch ($lang) {
	case 'en':
		$fullpage = file_get_contents('resource/english.xml');
		break;

	case 'vi':
		$fullpage = file_get_contents('resource/vietnam.xml');
		break;

	case 'es':
		$fullpage = file_get_contents('resource/spain.xml');
		break;

	case 'it':
		$fullpage = file_get_contents('resource/italian.xml');
		break;

	case 'ro':
		$fullpage = file_get_contents('resource/romana.xml');
		break;

	case 'ru':
		$fullpage = file_get_contents('resource/rusa.xml');
		break;
	
	default:
		$fullpage = file_get_contents('resource/english.xml');
		break;
}


$strings = simplexml_load_string($fullpage); //incarcam stringurile traduse

//echo $action."<br>";
if(!empty($url_lang) && $defaultLang == $lang){
 
 //echo "<br> null";
if($action != 'download')   
   error_404();
}
elseif($module != "" && !(in_array($module, $availableSites))){
      error_404();
   //echo 'eroor module';
} elseif( $action != null) {
   if(!validAction($action) ) {
      error_404();
      //echo 'eroor action'.$action;
   }
   
}

//echo "lang - ".$lang." action - ".$action." module - ".$module;
  



if ($action != "api") {
	include "blocks/head.inc.php";
	include "blocks/header.inc.php";
	include "blocks/search.inc.php";
}

 
   
   //echo "action -- ".$action;
   switch ($action) {
      case 'download':
            $object = getObject($link);
            if ($siteIsset) {
               if ($site != "apple")
                  include "blocks/download.inc.php";
               else if ($site == "apple") {
                  include "blocks/downloadApple.inc.php";
               }

            } else { // in caz ca o bagat un link rau
               trigger_error("Error parsing youtube", E_USER_ERROR);
               include "blocks/indexContainer.inc.php";
            }				
         break;

      case 'video':			
            include "blocks/video.inc.php";
         break;

      case 'music':
            //include "blocks/search.inc.php";
            include "blocks/music.inc.php";
         break;

      case 'sites':
      //echo "<br>sites---------------------";
         if($module != '' && (in_array($module, $availableSites))) {		
            include "infoSites.inc.php";
            //echo "<br>sites---------------------------------------------------1";
         } else {
            include "blocks/sitesContainer.inc.php";
            //echo "<br>sites---------------------------------------------------------2";
         }
      break;

      case 'api':
         include "inc/api.inc.php";
      break;

   //	case 'about':
   //			include "blocks/about.inc.php";
   //		break;

      case '404':
            include "blocks/404.inc.php";
         break;


      case '':
             include "blocks/latest.inc.php";
             include "blocks/sites-carousel.inc.php";
         break;




      default:

      break;
   }


if ($action != "api") {
	include "blocks/footer.inc.php";
}

?>
