<?php

class Xvideos extends Video {
	
	function __construct($url) {
		$this->videoUrl = $url;
		$this->getVideoData();
	}

	function getVideoData() {
		$fullpage = curlGet($this->videoUrl);
		$dom = new DOMDocument();
		@$dom->loadHTML($fullpage);
		$xpath = new DOMXPath($dom); 
		
		$tags = $xpath->query('//div[@id="main"]/h2');
		$this->videoTitle = ($tags->item(0)->firstChild->nodeValue); 
		
		$tags = $xpath->query('//span[@class="nb_views"]');
		$this->seen = trim($tags->item(0)->firstChild->textContent); 

		$tags = $xpath->query('//li[@class="profile_name"]');
		$this->uploader = trim($tags->item(0)->firstChild->nodeValue); 

		$tags = $xpath->query('//span[@class="duration"]');
		$this->duration = str_replace("-", "", trim($tags->item(0)->firstChild->textContent)); 


		$tags = $xpath->query('//embed[@id="flash-player-embed"]');
		$this->image = $tags->item(0)->getAttribute("flashvars");

		parse_str($this->image, $prs);
		$this->image = $prs['url_bigthumb'];
		$this->streamUrl['SD'] = array("url" => $prs['flv_url'],
										"size" => get_size($prs['flv_url']),
										"type" => "video/flv");

	}

}