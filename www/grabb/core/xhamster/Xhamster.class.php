<?php

class Xhamster extends Video {
	
	function __construct($url) {
		$this->videoUrl = $url;
		$this->getVideoData();
	}

	function getVideoData() {
		$fullpage = curlGet($this->videoUrl);
		$flashvars = strstr($fullpage, "&srv");
		$flashvars = strstr($flashvars, "&image", true);
		$flashvars = strstr($flashvars, "http");
		$flashvars = str_replace("&srv=", "", $flashvars);
		$flashvars = str_replace("&file=", "/key=", $flashvars);
		$flvFile = urldecode($flashvars);
		
		$dom = new DOMDocument();
		
		@$dom->loadHTML($fullpage);
		$xpath = new DOMXPath($dom); 
		// $tags = $xpath->query('//ul[@class="downloadList"]/li');
		// $arr = array();
		$tags = $xpath->query('//div[@class="head gr"]/h1');
		$this->videoTitle = ($tags->item(0)->nodeValue);
		
		$tags = $xpath->query('//td[@class="desc"]/h2');
		if (isset($tags->item(0)->nodeValue)) {
			$this->description = ($tags->item(0)->nodeValue);
		}

		$tags = $xpath->query('//div[@class="item"]');
		$this->duration = ($tags->item(1)->lastChild->nodeValue);
		$this->seen = ($tags->item(2)->lastChild->nodeValue);
		$this->uploader = ($tags->item(0)->childNodes->item(3)->nodeValue);
		$this->uploaded = ($tags->item(0)->childNodes->item(5)->nodeValue);
	
		$tags = $xpath->query('//div[@id="player"]/video');
		$this->image = $tags->item(0)->attributes;

		$this->image = $this->image->item(0)->value;	

		$url = $tags->item(0)->attributes->item(2)->value;
		$new_url = get_new_location($url);

		$size = get_size_301($url);

		if ($size == 'no size') {
			$size = get_size_301($url);
		}
		$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime='.$tags->item(0)->attributes->item(1)->value.'&token='.urlencode($url);	
		$this->streamUrl['HD'] = array("url" => $msg,
										"size" => $size,
										"type" => $tags->item(0)->attributes->item(1)->value);
		$size = get_size_301($flvFile);
		if ($size == 'no size') {
			$size = get_size_301($flvFile);
		}
		$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/flv&token='.urlencode($flvFile);	
		$this->streamUrl['SD'] = array("url" => $msg,
										"size" => $size,
										"type" => "video/flv");

		
	}

}