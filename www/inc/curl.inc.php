<?php
/*
 * function to get via cUrl 
 * From lastRSS 0.9.1 by Vojtech Semecky, webmaster @ webdot . cz
 * See      http://lastrss.webdot.cz/
 */
 
function curlGet($URL) {
    $ch = curl_init();
    $timeout = 3;
    curl_setopt( $ch , CURLOPT_URL , $URL );
    curl_setopt( $ch , CURLOPT_RETURNTRANSFER , 1 );
    curl_setopt( $ch , CURLOPT_CONNECTTIMEOUT , $timeout );
	/* if you want to force to ipv6, uncomment the following line */ 
	//curl_setopt( $ch , CURLOPT_IPRESOLVE , 'CURLOPT_IPRESOLVE_V6');
    $tmp = curl_exec( $ch );
    curl_close( $ch );
    return $tmp;
}  

/* 
 * function to use cUrl to get the headers of the file 
 */ 
function get_location($url) {
	$my_ch = curl_init();
	curl_setopt($my_ch, CURLOPT_URL,$url);
	curl_setopt($my_ch, CURLOPT_HEADER,         true);
	curl_setopt($my_ch, CURLOPT_NOBODY,         true);
	curl_setopt($my_ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($my_ch, CURLOPT_TIMEOUT,        10);
	$r = curl_exec($my_ch);
	 foreach(explode("\n", $r) as $header) {
		if(strpos($header, 'Location: ') === 0) {
			return trim(substr($header,10)); 
		}
	 }
	return '';
}

function get_size($url, $showHeaders = false) {
	$my_ch = curl_init();
	curl_setopt($my_ch, CURLOPT_URL, $url);

	$header = array(
			"User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36",
			"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
			"Accept-Language: en-us;q=0.5,en;q=0.3",
			"Keep-Alive: 115",
			"Connection: keep-alive"
		);

	curl_setopt($my_ch, CURLOPT_HTTPHEADER, $header);
	curl_setopt($my_ch, CURLOPT_HEADER,         true);
	curl_setopt($my_ch, CURLOPT_NOBODY,         true);
	curl_setopt($my_ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($my_ch, CURLOPT_TIMEOUT,        10);

	$r = curl_exec($my_ch);
	 foreach(explode("\n", $r) as $headers) {
	 	if ($showHeaders) {
		 	echo $headers . "<br/>";
	 	}
		if(strpos($headers, 'Content-Length:') === 0) {
			return trim(substr($headers,16)); 
		}
	 }
	return 0;
}

function get_size_301($url, $showHeaders = false) {
	$my_ch = curl_init();
	curl_setopt($my_ch, CURLOPT_URL,$url);
	$headers = array(
			"User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:36.0) Gecko/20100101 Firefox/36.0",
			"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
			"Accept-Language: en-us;q=0.5,en;q=0.3",
			"Keep-Alive: 115",
			"Connection: keep-alive"
		);
	curl_setopt($my_ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($my_ch, CURLOPT_HEADER,         true);
	curl_setopt($my_ch, CURLOPT_NOBODY,         true);
	curl_setopt($my_ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($my_ch, CURLOPT_TIMEOUT,        10);

	$r = curl_exec($my_ch);
	 foreach(explode("\n", $r) as $header) {
	 	if ($showHeaders) {
		 	echo $header . "<br/>";
	 	}
		if(strpos($header, 'Location:') === 0) {
			return get_size(trim(substr($header,10)), $showHeaders); 
		}
	 }
	return 'no size';
}

function get_new_location($url, $showHeaders = false) {
	$my_ch = curl_init();
	curl_setopt($my_ch, CURLOPT_URL,$url);
	$headers = array(
			"User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:36.0) Gecko/20100101 Firefox/36.0",
			"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
			"Accept-Language: en-us;q=0.5,en;q=0.3",
			"Keep-Alive: 115",
			"Connection: keep-alive"
		);
	curl_setopt($my_ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($my_ch, CURLOPT_HEADER,         true);
	curl_setopt($my_ch, CURLOPT_NOBODY,         true);
	curl_setopt($my_ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($my_ch, CURLOPT_TIMEOUT,        10);

	$r = curl_exec($my_ch);
	 foreach(explode("\n", $r) as $header) {
	 	if ($showHeaders) {
		 	echo $header . "<br/>";
	 	}
		if(strpos($header, 'Location:') === 0) {
			return trim(substr($header,10), $showHeaders); 
		}
	 }
	return 'no new location';
}

function get_description($url) {
	$fullpage = curlGet($url);
	$dom = new DOMDocument();
	@$dom->loadHTML($fullpage);
	$xpath = new DOMXPath($dom); 
	$tags = $xpath->query('//div[@class="info-description-body"]');
	foreach ($tags as $tag) {
		$my_description .= (trim($tag->nodeValue));
	}	
	
	return utf8_decode($my_description);
}


function  curl_http_get($url, $ssl = false) {
		$ch = curl_init($url);
		$headers = array(
			"User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:36.0) Gecko/20100101 Firefox/36.0",
			"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
			"Accept-Language: en-us;q=0.5,en;q=0.3",
			"Keep-Alive: 115",
			"Connection: keep-alive"
		);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

		if ($ssl) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		}
		
		$response = curl_exec($ch);
		$response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if($response_code != 200 && $response_code != 302 && $response_code != 304) {
			$response = false;
		}
		return $response;
}

function  curl_http_get_All($url, $ssl = false) {
		$ch = curl_init($url);
		$headers = array(
			"User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36",
			"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
			"Accept-Language: en-us;q=0.5,en;q=0.3",
			"Accept-Encoding: gzip, deflate, sdch"
		);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER,         true);
		curl_setopt($ch, CURLOPT_NOBODY,         true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT,        10);;
		if ($ssl) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		}

		$response = curl_exec($ch);
		echo "<pre>";
		print_r($response);
		echo "</pre>";
		
		$response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if($response_code != 200 && $response_code != 302 && $response_code != 304) {
			$response = false;
		}
		return $response;
}

function getVideo($url) {
	$videoData = curl_http_get($url);
	return $videoData;
}

function printout($str) {
	echo "<pre>";
	print_r($str);
	echo "</pre>";
}

function get_js_string_var($var, $script) {
	$center = "";
	$start = strpos($script, $var);
	if ($start) {
		$after = substr($script, $start);
		unset($script);
		$center = strstr($after, "\";", true);
		unset($after);	
		$center = substr($center, strpos($center, "\"")+1);
		return $center;
	} else {
		return "";
	}
}

function getStringTime($minutes, $strings) {
	  
	  

}

?>