	<div class="container details">
	<?php 
$genre = 0;
  $adultSites = array(
            "youporn",
            "xhamster",
            "xvideos",
            "xtube",
            "redtube"
          );
  
  $audioSites = array(
              "soundcloud",
              "mixcloud"
              );

  if (in_array($site, $adultSites)) {
    $genre = 2; //adult sites
  } else if (in_array($site, $audioSites)) {
    $genre = 1; //music
  }

  //$video = null;  
  //$video = getObject($link); //obtinem obiectul   
  $title =  str_replace(array("'","\"","'",'"'), array("&#39;","&quot;","&#39;","&quot;"), htmlspecialchars(stripslashes($object->getTitle())));
  $link = str_replace(array("'","\"","'",'"'), array("&#39;","&quot;","&#39;","&quot;"),htmlspecialchars(stripslashes($object->getVideoUrl())));
  $image = str_replace(array("'","\"","'",'"'), array("&#39;","&quot;","&#39;","&quot;"),htmlspecialchars(stripslashes($object->getImage())));
  $seen = str_replace(array("'","\"","'",'"'), array("&#39;","&quot;","&#39;","&quot;"),htmlspecialchars(stripslashes($object->getSeen())));
  
  $sql = "SELECT `id`,`counts`
              FROM `downloads`
                  WHERE `link` = '".$object->getVideoUrl()."' or `title` = '".$title."';";
  
  if(!$result = $db->query($sql)){
    die('There was an error running the query [' . $db->error . ']');
  }

  $t = time();
  if($result->num_rows == 0) {
   
    $sql = '
            INSERT INTO `downloads`
            (
              `link`,
              `site`,
              `genre`,
              `title`,
              `image`,
              `date`,
              `counts`,
              `seen`, 
              `duration`)
            VALUES
              (
              "'.$link.'",
              "'.$site.'",
              '.$genre.',
              "'.$title.'",
              "'.$image.'",
              "'.date("Y:m:d H:i:s", $t).'",
              1,
			  "'.$seen.'",
              0
            );';
  } else {
      $row = $result->fetch_assoc();
     // printout($row);
      $sql = "
        UPDATE `downloads`
            SET            
            `counts` = ".($row['counts']+1).",
            `seen` = '".$seen."',
            `date` = '".date("Y:m:d H:i:s", $t)."'         
             WHERE `id` = ".$row['id'].";";
  }

  if(!$result = $db->query($sql)){
    die('There was an error running the query [' . $db->error . ']');
  }

?>
		<div class="row">
			<div class="col-md-12">
					<h2 class="item-title">
						<?=$object->getTitle();?> 
					</h2>				
			</div> <!--/col-md-12-->
			<div class="col-md-4">
				<img src="<?=$object->getImage();?>" alt="">
				<p class="item-details">
					<span><?=$strings->strings->site;?></span> <?=$site;?>
					</br>
					<span><?=$strings->strings->duration;?></span> <?=$object->getDuration();?>
					</br>
					<span><?=$strings->strings->uploader;?></span> <?=$object->getUploader();?>
					</br>
					<span><?=$strings->strings->uploaded;?></span> <?=$object->getUploaded();?>
					</br>
					<span><?=$strings->strings->seen;?></span> <?=$object->getSeen();?> 
					</br></br>
          <?php $jora = str_replace(',', ', ', $object->getDescription()) ?>
					<span><?=$strings->strings->description;?></span><?=$jora;?>
					
				</p>
			</div> <!--/col-md-4-->
			<div class="col-md-8">
				<div class="col-md-12 list-header">
					<h5>Download Links for</br><a href="<?=$link;?>" target="_blank"> <?=$link;?> </a><i class="fa fa-external-link"></i></h5>
				</div> <!--/col-md-12 list header-->
				<ul class="col-md-12 col-sm-12 col-xs-12 file-list">
   				 <?php
 		            //start foreach
		            foreach ($object->getStream() as $key => $value) {			            
		          ?>
					<li>
						<div class="col-md-5 col-sm-5 col-xs-5 key">
							<i class="fa fa-film"></i> <?=$key;?>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-2 type">
							<?php
		                        if (strpos($value['type'], "/")) {
		                          echo substr(strstr($value['type'], "/"), 1);
		                        } else {
		                          echo $value['type'];
		                        }
		                      ?>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-5 dwn-btn">
							<div class="col-md-9 col-sm-7 col-xs-6 dwn-btn"><i class="fa fa-download"></i><a href="<?=$value['url'];?>"> <?=$strings->buttons->download;?></a></div><div class="col-md-3 col-sm-4 col-xs-6 size"> <?=getMB($value['size']);?> MB</div> 
						</div>						
					</li>

					<?php			            
			          //endfreach
			          }
			        ?>
				</ul>
			</div> <!--/col-md-8-->

		</div> <!--/row-->
	</div> <!--/container-->
  <?php
        include "blocks/sites-carousel.inc.php";
  ?>