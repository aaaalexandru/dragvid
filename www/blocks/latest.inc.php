<div id="products" class="content-section container">
    <div class="row">
        <div class="container-fluid">
            <div class="col-md-12">
                <h1 class="section-title"><?=$strings->strings->popular;?></h1>
                <div id="popular-carousel-block" class="carousel slide" data-ride="">
                    <div id="owl-demo" class="owl-carousel">
                    <?php
                        //lastest download
                        $sql = "SELECT * FROM `downloads` WHERE `genre` = 0 ORDER BY `counts` DESC LIMIT 0,10;";
                        if(!$result = $db->query($sql)){
                          die('There was an error running the query [' . $db->error . ']');
                        }

                        while($row = $result->fetch_assoc()) {  
                          $minutes = round((time() - strtotime($row['date'])) / 62.2);
                          
                           if ($minutes == 0) {
                              $time = $strings->strings->lessThenMinuteAgo;
                            } else

                            if ($minutes == 1) {
                              $time = $strings->strings->aboutAMinuteAgo;
                            } else
                            
                            if (($minutes > 1) && ($minutes < 60)) {
                              $time = $minutes .$strings->strings->minutesAgo;
                            } else 

                            if ($minutes > 60) {
                              $minutes = round($minutes / 60);
                              if ($minutes == 1) {
                                $time = $strings->strings->aboutAHourAgo;
                              } else 

                              if ($minutes > 1) {
                                $time = $strings->strings->about." ".$minutes . " " .$strings->strings->hoursAgo;
                              }
                          }   


                      ?>
                        <div class="col-md-12">
                            <div class="product-item">
                                <div class="item-thumb">
                                    <div class="overlay" onclick='download("<?=$row['link'];?>")'>
                                        <div class="overlay-inner">
                                            <a href="#" class="view-detail">
                                              <?=$strings->buttons->download;?>
                                            </a>
                                        </div>
                                    </div> <!-- /.overlay -->
                                    <img src="<?=$row['image'];?>" alt=""/>
                                </div> <!-- /.item-thumb -->
                                <div class="ellipsis">
                                  <a onclick='download("<?=$row['link'];?>")'>                                
                                  <?=trim($row['title']);?>
                                  </a>
                                </div> <!--/ellipsis-->
                                <span><i class="fa fa-eye"></i> <em class=""><?=$row['seen'];?></em> - <i class="fa fa-download"></i><em class="price"><?=$row['counts'];?></em></span>
                            </div> <!-- /.product-item -->
                        </div> <!--/col-md-12-->
                    
                         <?php
                           $time = "";
                            }//end while
                          ?>

                        
                    </div> <!--/owl-demo-->
                </div>  <!--/music-carousel-block-->  
            </div> <!--/col-md-12-->      

            <div class="col-md-12">
                <h1 class="section-title"><?=$strings->strings->latest;?></h1>
                <div id="latest-carousel-block" class="carousel slide" data-ride="">
                    <div id="demo-latest">
                        <div id="popular-demo" class="owl-carousel">
                          <?php
                        //lastest download
                        $sql = "SELECT * FROM `downloads` WHERE `genre` = 1 ORDER BY id DESC LIMIT 0,10;";
                        if(!$result = $db->query($sql)){
                          die('There was an error running the query [' . $db->error . ']');
                        }

                        while($row = $result->fetch_assoc()) {  
                          $minutes = round((time() - strtotime($row['date'])) / 62.2);
                          
                           if ($minutes == 0) {
                              $time = $strings->strings->lessThenMinuteAgo;
                            } else

                            if ($minutes == 1) {
                              $time = $strings->strings->aboutAMinuteAgo;
                            } else
                            
                            if (($minutes > 1) && ($minutes < 60)) {
                              $time = $minutes .$strings->strings->minutesAgo;
                            } else 

                            if ($minutes > 60) {
                              $minutes = round($minutes / 60);
                              if ($minutes == 1) {
                                $time = $strings->strings->aboutAHourAgo;
                              } else 

                              if ($minutes > 1) {
                                $time = $strings->strings->about." ".$minutes . " " .$strings->strings->hoursAgo;
                              }
                          }   


                      ?>
                          <div class="col-md-12">
                            <div class="product-item">
                                <div class="item-thumb">
                                    <div class="overlay">
                                        <div class="overlay-inner" onclick='download("<?=$row['link'];?>")'>
                                            <a href="#"  class="view-detail"><?=$strings->buttons->download;?></a>
                                        </div>
                                    </div> <!-- /.overlay -->
                                    <img src="<?=$row['image'];?>" alt=""/>
                                </div> <!-- /.item-thumb -->
                                <div class="ellipsis">
                                  <a class="" onclick='download("<?=$row['link'];?>")'>
                                
                                        <?=trim($row['title']);?>
                                  </a>
                                </div> <!--/ellipsis-->
                                <span><i class="fa fa-eye"></i> <em class=""><?=$row['seen'];?></em> - <i class="fa fa-download"></i><em class="price"><?=$row['counts'];?></em></span>
                            </div> <!-- /.product-item -->
                        </div> <!--/col-md-12-->
                        <?php
                           $time = "";
                            }//end while
                        ?>

                        </div> <!--/owl-demo-->
                    </div> <!--/#demo-popular-->
                </div> <!--/popular-carousel-block-->
            </div> <!--col-md-12-->


            <div class="col-md-12">
                <h1 class="section-title"><?=$strings->strings->video;?></h1>
                <div id="music-carousel-block" class="carousel slide" data-ride="">
                    <div id="demo-music">
                        <div id="latest-demo" class="owl-carousel">
                            
                        <?php
                        //lastest download
                        $sql = "SELECT * FROM `downloads` WHERE `genre` = 0 ORDER BY id DESC LIMIT 0,10;";
                        if(!$result = $db->query($sql)){
                          die('There was an error running the query [' . $db->error . ']');
                        }

                        while($row = $result->fetch_assoc()) {  
                          $minutes = round((time() - strtotime($row['date'])) / 62.2);
                          
                           if ($minutes == 0) {
                              $time = $strings->strings->lessThenMinuteAgo;
                            } else

                            if ($minutes == 1) {
                              $time = $strings->strings->aboutAMinuteAgo;
                            } else
                            
                            if (($minutes > 1) && ($minutes < 60)) {
                              $time = $minutes .$strings->strings->minutesAgo;
                            } else 

                            if ($minutes > 60) {
                              $minutes = round($minutes / 60);
                              if ($minutes == 1) {
                                $time = $strings->strings->aboutAHourAgo;
                              } else 

                              if ($minutes > 1) {
                                $time = $strings->strings->about." ".$minutes . " " .$strings->strings->hoursAgo;
                              }
                          }   


                      ?>
                          <div class="col-md-12">
                            <div class="product-item">
                                <div class="item-thumb">
                                    <div class="overlay">
                                        <div class="overlay-inner" onclick='download("<?=$row['link'];?>")'>
                                            <a href="#" class="view-detail"><?=$strings->buttons->download;?></a>
                                        </div>
                                    </div> <!-- /.overlay -->
                                    <img src="<?=$row['image'];?>" alt=""/>
                                </div> <!-- /.item-thumb -->
                                <div class="ellipsis">
                                  <a onclick='download("<?=$row['link'];?>")'>
                                
                                        <?=trim($row['title']);?>
                                  </a>
                                </div>
                                <span><i class="fa fa-eye"></i> <em class=""><?=$row['seen'];?></em> - <i class="fa fa-download"></i><em class="price"><?=$row['counts'];?></em></span>
                            </div> <!-- /.product-item -->
                          </div> <!--/col-md-12-->
                        <?php
                           $time = "";
                            }//end while
                          ?>


                        </div> <!--/owl-demo-->
                    </div> <!--/#demo-latest-->
                </div> <!--/latest-carousel-block-->
            </div> <!--col-md-12-->
        </div> <!--/container-fluid-->
    </div> <!--/row-->
</div> <!--/#products-->