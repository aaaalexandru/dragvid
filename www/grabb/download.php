<?php

//include_once('config.php');
// Check download token
include_once('inc/curl.inc.php');

if (empty($_GET['mime']) OR empty($_GET['token']))
{
	exit('Invalid download token 8{');
}

// Set operation params
$mime = filter_var($_GET['mime']);
$ext  = str_replace(array('/', 'x-'), '', strstr($mime, '/'));
if (strstr($ext,';')) {
	$ext = substr($ext, 0, 3);
}
$url  = $_GET['token'];
$name = base64_decode($_GET['title']). '.' .$ext; 
$url = preg_replace("/ /", "%20", $url);
// Fetch and serve
if ($url)
{
	if (!isset($_GET['size'])) 
		$size=get_size($url);
	else
		$size = $_GET['size'];
	// Generate the server headers
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE)
	{
		header('Content-Type: "' . $mime . '"; charset=utf-8');
		header('Content-Disposition: attachment; filename="' . $name . '"');
		header('Expires: 0');
		header('Content-Length: '.$size);
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header("Content-Transfer-Encoding: binary");
		header('Pragma: public');
	}
	else
	{
		header('Content-Type: "' . $mime . '"; charset=utf-8');
		header('Content-Disposition: attachment; filename="' . $name . '"');
		header("Content-Transfer-Encoding: binary");
		header('Expires: 0');
		header('Content-Length: '.$size);
		header('Pragma: no-cache');
	}

	readfile($url);
	exit;
}

// Not found
exit('File not found 8{');

?>