<?php

class Vine extends Video {
	
	private $mime;

	function __construct($url) {		
		$this->videoUrl = $url;
		$this->getVideoData($url);
	}

	function getVideoData($url) {
		$fullpage = curlGet($url);
		$dom = new DOMDocument();

		@$dom->loadHTML($fullpage);
		$xpath = new DOMXPath($dom); 
		//parsam streamUrl
		$tags = $xpath->query('//meta[@property="twitter:player:stream"]');
		$this->streamUrl = $tags->item(0)->attributes->item(1)->value;

		//parsam mime
		$tags = $xpath->query('//meta[@property="twitter:player:stream:content_type"]');
		$this->mime = $tags->item(0)->attributes->item(1)->value;

		//parsam titlu 
		$tags = $xpath->query('//meta[@property="og:title"]');
		$this->videoTitle = $tags->item(0)->attributes->item(1)->value;		

		//parsam imaginea 
		$tags = $xpath->query('//meta[@property="og:image"]');
		$this->image = $tags->item(0)->attributes->item(1)->value;

		$tags = $xpath->query('//meta[@name="description"]');
		$this->description = $tags->item(0)->attributes->item(1)->value;

		$tags = $xpath->query('//article[@class="post"]/a/span');
		$this->uploader = $tags->item(0)->nodeValue;
		
		//$tags = $xpath->query('//span[@class="time"]');
		//$this->uploader = $tags->item(0);
		//printout($this->uploader);

		$size = get_size($this->streamUrl);
		$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/mp4&token='.($this->streamUrl);
		$this->streamUrl = array("Norm" => array("url" => $msg,
								"size" => $size,
								"type" => "video/mp4")
								);

	}

	function getTitle() {
		return $this->videoTitle;
	}

	function getMime() {
		return $this->mime;
	}

	function getStreamUrl() {
		return $this->streamUrl;
	}

	function getImage() {
		return $this->image;
	}

	function getStream() {
		return $this->streamUrl;
	}
}


?>