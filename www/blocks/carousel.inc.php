<!-- CAROUSEL -->
    <div id="services" class="content-section">
    
        <div class="row">
            <div class="col-md-12">

                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- indicators -->

                    <div class="carousel-inner container" role="listbox">
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <div class="row">
                                    <div class="col-sm-3"><img src="images/apple.png" alt="">
                                    <h4>Easy customization</h4>
                                    <p>Cras congue orci at diam condimentum, in dignissim tellus elementum.</p>
                                
                                    </div>
                                    <div class="col-sm-3">
                                        <img src="images/facebook.png" alt="">
                                        <h4>Web design</h4>
                                        <p>Cras congue orci at diam condimentum, in dignissim tellus elementum.</p>

                                    </div>
                                    <div class="col-sm-3">
                                        <img src="images/instagram.png" alt="">
                                        <h4>High Quality</h4>
                                        <p>Cras congue orci at diam condimentum, in dignissim tellus elementum.</p>
                                    </div>
                                    <div class="col-sm-3">
                                        <img src="images/mixcloud.png" alt="">
                                        <h4>Professional Design</h4>
                                        <p>Cras congue orci at diam condimentum, in dignissim tellus elementum.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="col-sm-3">
                                    <img src="images/youtube.png" alt="">
                                    <h4>Easy customization</h4>
                                    <p>Cras congue orci at diam condimentum, in dignissim tellus elementum.</p>
                                </div>
                                
                                <div class="col-sm-3">
                                    <img src="images/vevo.png" alt="">
                                    <h4>Web design</h4>
                                    <p>Cras congue orci at diam condimentum, in dignissim tellus elementum.</p>

                                </div>
                                <div class="col-sm-3">
                                    <img src="images/ted.png" alt="">
                                    <h4>High Quality</h4>
                                    <p>Cras congue orci at diam condimentum, in dignissim tellus elementum.</p>
                                </div>
                                <div class="col-sm-3">
                                    <img src="images/vk.png" alt="">
                                    <h4>Professional Design</h4>
                                    <p>Cras congue orci at diam condimentum, in dignissim tellus elementum.</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-sm-3">
                                    <img src="images/vimeo.png" alt="">
                                    <h4>Easy customization</h4>
                                    <p>Cras congue orci at diam condimentum, in dignissim tellus elementum.</p>
                                </div>
                                
                                <div class="col-sm-3">
                                    <img src="images/vine.png" alt="">
                                    <h4>Web design</h4>
                                    <p>Cras congue orci at diam condimentum, in dignissim tellus elementum.</p>

                                </div>
                                <div class="col-sm-3">
                                    <img src="images/twitter.png" alt="">
                                    <h4>High Quality</h4>
                                    <p>Cras congue orci at diam condimentum, in dignissim tellus elementum.</p>
                                </div>
                                <div class="col-sm-3">
                                    <img src="images/twitch.png" alt="">
                                    <h4>Professional Design</h4>
                                    <p>Cras congue orci at diam condimentum, in dignissim tellus elementum.</p>
                                </div>
                            </div>

                        </div>

                    <!-- Left anf Right controls -->
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    </a>

                    
                </div><!--/myCarouse-->
            </div> <!--col-md-12-->
        
        </div>
    </div> <!-- /#services -->
<!-- /CAROUSEL -->