
<div id="front">
    <div class="site-header">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-6 logo">
                    
                    <div id="templatemo_logo">
                        <h1><a href="/<?php echo  ($defaultLang == $lang)? "" : $lang;?>">Dragvid</a></h1>
                    </div> <!-- /.logo -->
                </div> <!-- /.col-md-4 -->
                <div class="col-md-8 col-sm-6 col-xs-6 drop-menu">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div> <!--/navbar-header-->
                    <nav id="bar" class="collapse navbar-collapse" style="height:200px;">
                        <ul class="nav navbar-nav">
                            <li><a href="/<?php echo ($defaultLang == $lang)? "" : $lang;?>"><?= $strings->menu->home;?></a></li> 
                            <li><a href="/<?php echo ($defaultLang == $lang)? "" : $lang."/"; ?>video"><?= $strings->menu->video;?></a></li>
                            <li><a href="/<?php echo ($defaultLang == $lang)? "" : $lang."/";?>music"><?= $strings->menu->music;?></a></li>
                           
                            
                            <li class="dropdown"><a href="/en" id="drop1" class="dropdown-toggle" role="button" aria-expanded="false" aria-hospup="false" data-toggle="dropdown"><?php echo fullLanguage($lang); ?> <span class="caret"></span></a>
                                <ul class="dropdown-menu" aria-labelledby="drop1" role="menu">
          <?php
                                   
            foreach($arrayLanguage as $key => $language) {
               if($lang != $key)
                  echo "<li ><a href='/".$key."' >".$language."</a></li>";
            }

         ?>
                                    
                                    
                                </ul> <!--/.dropdown-menu-->
                            </li> <!--/.dropdown-->
                        </ul> <!--/.navbar-nav-->
                    </nav> <!--/#bar-->
                </div> <!-- /.col-md-8 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->    
    </div> <!-- /.site-header -->
</div> <!-- /#front -->
