<?php

include "inc/simple_html_dom.php";

class Ted extends Video {
	
	protected $filmed;
	
	function __construct($url) {
		$this->videoUrl = $url;
		$this->getVideoData();
	}	

	function getVideoData() {
		$html = file_get_html($this->videoUrl);
		$html = $html->find("div[class=main talks-main]",0);
		$htmldata = $html->find("script",5)->outertext;
		$htmldata = substr($htmldata, 26, strlen($htmldata)-36);
		$htmldata = json_decode($htmldata);
		$this->streamUrl = $htmldata->talks[0]->nativeDownloads; //linkurile de download 
		$this->image = $htmldata->talks[0]->thumb; //imaginea thumbail
		$this->uploader = $htmldata->talks[0]->speaker; //imaginea thumbail
		$this->uploaded = date("Y-M-d H:i", $htmldata->talks[0]->published);
		$this->duration = date("i:s", $htmldata->talks[0]->duration);
		$this->videoTitle = $htmldata->talks[0]->title; // titlul
		$this->description = $html = $html->find("p[class=talk-description]",0)->innertext;
		$arr = (array) $this->streamUrl; 			
		foreach ($arr as $key => $link) {
			$size = get_size_301($link);
			$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/mp4&token='.($link);
			$arr[$key] = array("url" => $msg, 
								"size" => $size, 
								"type" => "video/mp4"
								);
		}
		$this->streamUrl = $arr;
	}

	function getData() {
		return $this->streamUrl;
	}

	function getTitle() {
		return $this->videoTitle;
	}

	function getImage() {
		return $this->image;
	}

	function getStream() {
		return $this->streamUrl;
	}


}

?>