    <div id="menu-container">
    <!-- gallery start -->
    <div class="content homepage" id="menu-1">
    <div class="container">
      <div class="helpForm">
       
        <div class="cloud-download"><i class="fa fa-arrow-circle-down"></i></div>
        <div class="cloud-text">
         <h2>
          <?=$strings->strings->helper;?>
          </h2>
        </div>
             </div>
        <?php include "blocks/searchForm.inc.php"; ?>


<!-- =====================================   carousel      ==================================  -->
    <?php 
          include "carousel.inc.php";


          if ( $action != "adult")
            include "blocks/mostPop.inc.php";
          else if ($action == "adult")
            include "blocks/mostPopAdult.inc.php";

     ?>





	  </div>

  
    <!-- <div class="container">
    	<div class="row">
        	<div class="templatemo_loadmore">
			 <button class="gallery_more" id="button" onClick="showhide()">Load More</button>
            </div>
        </div>
    </div> -->
    </div>
      <!-- gallery end -->
    
    <!--team end-->
    <div class="clear"></div>
    <!-- service start -->
    <div class="content services" id="menu-3">
    <div class="container">
    	<div class="row templatemo_servicerow">
        	<div class="templatemo_hexservices col-sm-6">
            	<div class="blok text-center">
                  <div class="hexagon-a">
                     <a class="hlinktop" href="#">
                     	 <div class="hexa-a">
                         	<div class="hcontainer-a">
                          <div class="vertical-align-a">
                            <span class="texts-a"><i class="fa fa-bell"></i></span>
                          </div>
                        </div>
                         </div>
                     </a>
                  </div>  
                     <div class="hexagonservices">
                     	<a class="hlinkbott" href="#">
                        <div class="hexa">
                      	  <div class="hcontainer">
                          <div class="vertical-align">
                          <span class="texts"></span>
                          </div>
                        </div>
                      </div>
                        </a>
                     </div>
                     </div>
                     <div class="templatemo_servicetext">
                    <h3>Etiam vulputate</h3>
                    <p>Vestibulum diam lorem, aliquet et sagittis ac, facilisis nec massa. Suspendisse sagittis leo diam, sed dapibus eros vehicula eu. Aenean nulla magna, gravida at dui in, fringilla vestibulum massa.</p>
                    </div>
              </div>
              <div class="templatemo_hexservices col-sm-6">
            	<div class="blok text-center">
                  <div class="hexagon-a">
                     <a class="hlinktop" href="#">
                     	 <div class="hexa-a">
                         	<div class="hcontainer-a">
                          <div class="vertical-align-a">
                            <span class="texts-a"><i class="fa fa-briefcase"></i></span>
                          </div>
                        </div>
                         </div>
                     </a>
                  </div>  
                     <div class="hexagonservices">
                     	<a class="hlinkbott" href="#">
                        <div class="hexa">
                      	  <div class="hcontainer">
                          <div class="vertical-align">
                          <span class="texts"></span>
                          </div>
                        </div>
                      </div>
                        </a>
                     </div>
                     </div>
                     <div class="templatemo_servicetext">
                    <h3>Aliquam faucibus</h3>
                    <p>Vivamus a purus vel ante fermentum bibendum. Sed laoreet, enim quis euismod egestas, risus tortor tincidunt lacus, in iaculis mauris lectus at augue. Donec luctus nibh nec ullamcorper feugiat.</p>
                    </div>
              </div>
              <div class="templatemo_hexservices col-sm-6">
            	<div class="blok text-center">
                  <div class="hexagon-a">
                     <a class="hlinktop" href="#">
                     	 <div class="hexa-a">
                         	<div class="hcontainer-a">
                          <div class="vertical-align-a">
                            <span class="texts-a"><i class="fa fa-mobile"></i></span>
                          </div>
                        </div>
                         </div>
                     </a>
                  </div>  
                     <div class="hexagonservices">
                     	<a class="hlinkbott" href="#">
                        <div class="hexa">
                      	  <div class="hcontainer">
                          <div class="vertical-align">
                          <span class="texts"></span>
                          </div>
                        </div>
                      </div>
                        </a>
                     </div>
                     </div>
                     <div class="templatemo_servicetext">
                    <h3>Donec sagittis</h3>
                    <p>Phasellus sodales magna orci, id scelerisque lectus faucibus a. Vivamus varius tincidunt sem. Etiam ultricies orci sit amet sem egestas varius vitae at lacus. Nunc blandit elit in mauris semper, id iaculis felis condimentum.</p>
                    </div>
              </div>
              <div class="templatemo_hexservices col-sm-6">
            	<div class="blok text-center">
                  <div class="hexagon-a">
                     <a class="hlinktop" href="#">
                     	 <div class="hexa-a">
                         	<div class="hcontainer-a">
                          <div class="vertical-align-a">
                            <span class="texts-a"><i class="fa fa-trophy"></i></span>
                          </div>
                        </div>
                         </div>
                     </a>
                  </div>  
                     <div class="hexagonservices">
                     	<a class="hlinkbott" href="#">
                        <div class="hexa">
                      	  <div class="hcontainer">
                          <div class="vertical-align">
                          <span class="texts"></span>
                          </div>
                        </div>
                      </div>
                        </a>
                     </div>
                     </div>
                     <div class="templatemo_servicetext">
                    <h3>Integer tempus</h3>
                    <p>Maecenas porttitor erat et enim dapibus, sit amet accumsan velit tincidunt. Etiam sapien urna, suscipit ut purus ac, convallis pulvinar tellus. In eu neque purus. Donec cursus dictum pulvinar.</p>
                    </div>
              </div>
              <div class="templatemo_hexservices col-sm-6">
            	<div class="blok text-center">
                  <div class="hexagon-a">
                     <a class="hlinktop" href="#">
                     	 <div class="hexa-a">
                         	<div class="hcontainer-a">
                          <div class="vertical-align-a">
                            <span class="texts-a"><i class="fa fa-thumb-tack"></i></span>
                          </div>
                        </div>
                         </div>
                     </a>
                  </div>  
                     <div class="hexagonservices">
                     	<a class="hlinkbott" href="#">
                        <div class="hexa">
                      	  <div class="hcontainer">
                          <div class="vertical-align">
                          <span class="texts"></span>
                          </div>
                        </div>
                      </div>
                        </a>
                     </div>
                     </div>
                     <div class="templatemo_servicetext">
                    <h3>Aliquam pellentesque</h3>
                    <p>Nam auctor elementum dolor. Donec euismod, justo sed convallis blandit, ipsum erat mattis lectus, vel pharetra neque enim tristique risus. Ut consequat nisi vel justo. Curabitur in orci vel enim congue cursus.</p>
                    </div>
              </div>
              
        </div>
    </div>
    </div>
    <!-- service end -->
    <!-- contact start -->
    <div class="content contact" id="menu-5">
    <div class="container">
     	<div class="row">
        	<div class="col-md-4 col-sm-12">
            	<div class="templatemo_contactmap">
    			<div id="templatemo_map"></div>
                <img src="/images/templatemo_contactiframe.png" alt="contact map">
                </div>
                </div>
            <div class="col-md-3 col-sm-12 leftalign">
            	<div class="templatemo_contacttitle">Contact Information</div>
                <div class="clear"></div>
                <p>Integer eu neque sed mi fringilla pellentesque a eget leo. Duis ornare diam lorem, sit amet tempor mauris fringilla in. Etiam semper tempus augue, at vehicula metus. Nam vestibulum tortor nec congue ornare.</p>
                <div class="templatemo_address">
                	<ul>
                	<li class="left fa fa-map-marker"></li>
                    <li class="right">Nulla ut tellus, sit amet urna, <br>scelerisque pretium 10560</li>
                    <li class="clear"></li>
                    <li class="left fa fa-phone"></li>
                    <li class="right">010-040-0260</li>
                    <li class="clear"></li>
                    <li class="left fa fa-envelope"></li>
                    <li class="right">info@company.com</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5 col-sm-12">
            	<form role="form">
              	<div class="templatemo_form">
                	<input name="fullname" type="text" class="form-control" id="fullname" placeholder="Your Name" maxlength="40">
              	</div>
              	<div class="templatemo_form">
                	<input name="email" type="text" class="form-control" id="email" placeholder="Your Email" maxlength="40">
              	</div>
               	<div class="templatemo_form">
                	<input name="subject" type="text" class="form-control" id="subject" placeholder="Subject" maxlength="40">
              	</div>
              	<div class="templatemo_form">
	            	<textarea name="message" rows="10" class="form-control" id="message" placeholder="Message"></textarea>
              	</div>
              	<div class="templatemo_form"><button type="button" class="btn btn-primary">Send Message</button></div>
            </form>
            </div>
        </div>
    	
    </div>
    </div>
    </div>
    <!-- contact end -->