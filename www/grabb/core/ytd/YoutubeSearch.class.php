<?php
include "Youtube.class.php";

class YoutubeSearch extends Youtube {
	
	public $queryString;
	public $page;
	
	function __construct($str, $page) {
		$this->queryString = urlencode($str);
		$this->page = $page;
	}
	
	function getData() {
		$url = "https://www.youtube.com/results?search_query=".$this->queryString."&page=".$this->page;
		$data = curlGet($url);
		
		$dom = new DOMDocument();
		
		@$dom->loadHTML($data);
		$xpath = new DOMXPath($dom);
		//data-context-item-id
		$tags = $xpath->query('//ol[@class="item-section"]/li/div[@data-context-item-id]');
		
		$videoElements = array();
		
		foreach ($tags as $key => $value) {
			$data = file_get_contents('http://i.ytimg.com/vi/'.$value->getAttribute("data-context-item-id").'/mqdefault.jpg');
			$base64 = base64_encode($data);
			$videoElements[$value->getAttribute("data-context-item-id")] = array(
				"title" => utf8_decode($value->firstChild->childNodes->item(1)->firstChild->firstChild->nodeValue),
				"uploader" => utf8_decode($value->firstChild->childNodes->item(1)->childNodes->item(1)->childNodes->item(1)->nodeValue),
				"uploaded" => $value->firstChild->childNodes->item(1)->childNodes->item(2)->firstChild->firstChild->nodeValue,
				"seen" => $value->firstChild->childNodes->item(1)->childNodes->item(2)->firstChild->childNodes->item(1)->nodeValue,
				"description" => utf8_decode($value->firstChild->childNodes->item(1)->childNodes->item(3)->nodeValue),
				"duration" => trim($value->firstChild->childNodes->item(0)->childNodes->item(0)->lastChild->nodeValue),
				"image" => $base64,
				"id" => $value->getAttribute("data-context-item-id")					
					//"http://i.ytimg.com/vi/".$value->getAttribute("data-context-item-id")."/default.jpg"//'<img src="'.$value->firstChild->childNodes->item(0)->firstChild->firstChild->firstChild->getAttribute("src").'"/>'					
			);
			
			//echo $videoElements[$value->getAttribute("data-context-item-id")]['title']."</br>";
			
// 			if ($value->firstChild->firstChild->firstChild->firstChild->firstChild->getAttribute("src")) {
// 				//printout($value->firstChild->firstChild->firstChild->firstChild->firstChild->getAttribute("src"));
// 			}
			
		}		
		$videoElements = json_encode($videoElements);
		echo $videoElements;
		
		
		
	}
 	
}

/*
 *    Player Background Thumbnail (480x360px) :    http://i1.ytimg.com/vi/VIDEO_ID/0.jpg
 *    Normal Quality Thumbnail (120x90px) :    http://i1.ytimg.com/vi/VIDEO_ID/default.jpg
 *    Medium Quality Thumbnail (320x180px) :    http://i1.ytimg.com/vi/VIDEO_ID/mqdefault.jpg
 *    High Quality Thumbnail (480x360px) :    http://i1.ytimg.com/vi/VIDEO_ID/hqdefault.jpg
 *    Start Thumbnail (120x90px) :   http://i1.ytimg.com/vi/VIDEO_ID/1.jpg
 *    Middle Thumbnail (120x90px) :   http://i1.ytimg.com/vi/VIDEO_ID/2.jpg
 *    End Thumbnail (120x90px) :    http://i1.ytimg.com/vi/VIDEO_ID/3.jpg
 */

?>
