<?php
class Vk extends Video {
	private $mime;

	function __construct($url) {
		parse_str(urldecode($url), $parameters);
		if (isset($parameters['z'])) {
			if ($partOfUrl = explode("/", $parameters['z'])) {
				$parameters['z'] = $partOfUrl[0];
			} 
			$this->videoUrl = "http://vk.com/".$parameters['z'];
		} else {
			$this->videoUrl = urldecode($url);
			$this->videoUrl = str_replace("https://m.", "http://", $this->videoUrl);
	    	$this->videoUrl = str_replace("http://m.", "http://", $this->videoUrl);
		}		
		$this->videoTitle = "videoFromVk";	
		$this->getVideoData();
	}

	function getVideoData() {
		$fullpage = curlGet($this->videoUrl);		
		$dom = new DOMDocument();
		@$dom->loadHTML($fullpage);
		$xpath = new DOMXPath($dom); 
		//parsam linku la stream
		$tags = $xpath->query('//div[@id="mv_date_wrap"]');
		$this->uploaded = $tags->item(0);
		
		$tags = $xpath->query('//script[@type="text/javascript"]');
		
		if(strpos($tags->item(13)->nodeValue, "nvar vars")) {
			$script = $tags->item(13);
		} else {
			foreach ($tags as $value) {
				if (strpos($value->nodeValue, "nvar vars")) {
					$script = $value->nodeValue;
					break;
				}				
			}
		}

		$script = $script->nodeValue;
		$script = strstr($script, "nvar vars");
		$script = strstr($script, ";\\n\\nif", true);
		$script = str_replace("nvar vars = ", "", $script);
		$script = json_decode(str_replace("\\", "", $script));
		$this->uploader = $script->md_author;
		$this->duration = gmdate("h:i:s", $script->duration);
		$this->image = $script->jpg;
		$this->videoTitle = $script->md_title;	
		$title = 'DQpOb2l6ZSBNQyAvIDEwINC70LXRgjog0K7QsdC40LvQtdC50L3Ri9C5INC60L7QvdGG0LXRgNGCIC8gVEVBU0VSDQo=';	
		foreach ($script as $key => $value) {			
			if (preg_match("/^url/", $key, $match)) {
				$size = get_size($value);
				$msg = '../download.php?title='.base64_encode(substr($this->videoTitle,0, strlen($this->videoTitle)/4)).'&size='.$size.'&mime=video/mp4&token='.($value);
				$this->streamUrl[str_replace("url", "",$key).""] = array("url" => $msg, "size" => $size, "type" => "video/mp4");
			}
		}	
	}


	function getTitle() {
		return $this->videoTitle;
	}

	function getVideoUrl() {
		return $this->videoUrl;
	}

	function getStream() {
		return $this->streamUrl;
	}

	function getImage() {
		return $this->image;
	}

	function getSize() {
		return $this->size;
	}

	function getMime() {
		return $this->mime;
	}

}

?>