<?php

define("MIXCLOUD_FIRST_SERVER", 13);
define("MIXCLOUD_LAST_SERVER", 22);


class Mixcloud extends Video {

	function __construct($url) {

		$this->videoUrl = str_replace("https", "http", $url);
		$this->videoUrl = str_replace("https://m.", "http://", $this->videoUrl);
	    $this->videoUrl = str_replace("http://m.", "http://", $this->videoUrl);
		$this->getVideoData();
	}

	function getVideoData() {

		$urlprefix = "http://www.mixcloud.com/";
		$alturls = array(
			"https://www.mixcloud.com/",
			"http://www.mixcloud.com/",
			"https://mixcloud.com/",
			"http://mixcloud.com/",
			"https://x.mixcloud.com/",
			"http://x.mixcloud.com/",
		);

		foreach($alturls as $alturl) {
			if (substr($this->videoUrl, 0, strlen($alturl)) === $alturl) {
				$this->videoUrl = substr($this->videoUrl, strlen($alturl));
				break;
			}
		}

		$this->videoUrl = $urlprefix . $this->videoUrl;

		$content = file_get_contents($this->videoUrl);

		$dom = new DOMDocument();

		@$dom->loadHTML($content);
		$xpath = new DOMXPath($dom); 
		//parsam imaginea
		$tags = $xpath->query('//meta[@property="og:image"]');
		$this->image = $tags->item(0)->attributes->item(1)->value;
 
		$tags = $xpath->query('//meta[@property="music:duration"]');
		$this->duration = gmdate("h:i:s",$tags->item(0)->attributes->item(1)->value);
	
		$tags = $xpath->query('//span[@m-tooltip="Listens"]/a');
		$this->seen = $tags->item(0)->nodeValue;

		$tags = $xpath->query('//div[@class="cloudcast-description"]');
		$this->description = $tags->item(0)->nodeValue;

		if(preg_match('/\"profile\"\: \"(.*?)\"\,/', $content, $title)) {
					$this->uploader = $title[1];
		}

		if (!preg_match("/m-preview=\"([^\"]*)\"/", $content, $m)) {
			trigger_error("Error parsing mixcloud", E_USER_ERROR);
		} else {
			$result = str_replace("previews", "c/originals", $m[1]);
			$result = preg_replace("/stream[0-9][0-9]/", "streamXX", $result);

			if (preg_match("/meta property=\"og:title\" content=\"([^\"]*)\" \/>/", $content, $m)) {
				$title = $m[1];
			} else {
				$title = substr($result, strrpos($result, "/") + 1);
			}

			$xreturn = "No server found for download";
			$return = $xreturn;
			for ($i = MIXCLOUD_FIRST_SERVER; $i <= MIXCLOUD_LAST_SERVER; $i++) {
				$testUrl = str_replace("streamXX", "stream" . $i, $result);
				$headers = get_headers($testUrl, 1);

				if ($headers[0] === "HTTP/1.1 200 OK") {
					$return = "<a href=\"" . $testUrl . "\" download=\"" . $title . "\".mp3\">" . $title . "</a>";
					break;
				}
			}
			for ($i = MIXCLOUD_FIRST_SERVER; $i <= MIXCLOUD_LAST_SERVER; $i++) {

				if ($xreturn !== $return) {
					break;
				}

				$testUrl = str_replace("streamXX", "stream" . $i, $result);
				$testUrl = str_replace(".mp3", ".m4a", $testUrl);
				$testUrl = str_replace("originals/", "m4a/64/", $testUrl);
				$headers = get_headers($testUrl, 1);

				if ($headers[0] === "HTTP/1.1 200 OK") {
					$return = "<a href=\"" . $testUrl . "\" download=\"" . $title . ".mp3\">" . $title . "</a>";
					break;
				}
			}
			$this->videoTitle = $title;
			//echo $return;
			$size = get_size($testUrl);
			$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=audio/mp3&token='.urlencode($testUrl);	
			$this->streamUrl = array("Audio" => array("url"=>$msg, "size" => $size, "type" => "audio/mp3"));
		}
	}

}

?>