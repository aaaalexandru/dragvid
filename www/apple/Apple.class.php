<?php
include "inc/simple_html_dom.php";

class Apple extends Video {

	function __construct($url) {
		$this->videoUrl = $url;
		$this->getVideoData();
	}

	function getVideoData() {
		// $page = curlGet($this->videoUrl. "/includes/trailer/large.html");		
		
		// $dom = new DOMDocument();
		// @$dom->loadHTML($page);
		// $xpath = new DOMXPath($dom); 
		// $tags = $xpath->query('//a[@class="movieLink"]');	
		// $webLink = str_replace("_", "_h", $tags->item(0)->attributes->item(1)->value);

		//printout($webLink);
		//printout(get_size($tags->item(0)->attributes->item(1)->value));


		$page = curlGet($this->videoUrl. "/includes/playlists/itunes.inc");
		//printout(htmlspecialchars($page));
		//printout($html->find("ul[class=dropdown thumb-strip-dropdown dropdown-download]"), 0);
	//	curl_http_get_All("http://movietrailers.apple.com/movies/independent/riseandshine/riseandshine-tlr1_h720p.mov");
		//printout(htmlspecialchars($page));
		$dom = new DOMDocument();
		@$dom->loadHTML($page);
		$xpath = new DOMXPath($dom); 

		$blocks = $xpath->query("//div[@class='col right']");
		$imgs = $xpath->query("//img");

		$tags = $xpath->query("//ul[@class='list-trailer-actions']");
		foreach ($tags as $key => $value) {
			$this->duration[$key] = $tags->item($key)->childNodes->item(0)->lastChild->nodeValue;
		}

		if(preg_match_all('/\"posted\"\:\"(.*?)\"\,/', $page, $title)) {
			$this->uploaded = array_unique($title[1]);
		}

		foreach ($blocks as $key => $containers) {
			$title = $containers->getElementsByTagName("h3");
			$arr[$title->item(0)->nodeValue] = array();

			$this->image[$title->item(0)->nodeValue] = $imgs->item($key)->attributes->item(0)->value;

			$ul = $containers->getElementsByTagName("ul");
			$li = $ul->item(1)->getElementsByTagName("li");
			foreach ($li as $k => $v) {
				$a = $li->item($k)->getElementsByTagName("a");
				$size = get_size($a->item(0)->attributes->item(0)->value);
				$arr[$title->item(0)->nodeValue][$v->nodeValue] = array(
						"url" => $a->item(0)->attributes->item(0)->value,
						"size" => $size,
						"type" => "video/mov"
					);				
			}
		}
		$page = curlGet($this->videoUrl);	
		@$dom->loadHTML($page);
		$xpath = new DOMXPath($dom); 
		$title = $xpath->query("//h3[@class='title']");
		
		$this->videoTitle = $title->item(0)->childNodes->item(0)->nodeValue;
		$this->streamUrl = $arr;

	}

}

?>