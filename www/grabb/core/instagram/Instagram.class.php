<?php

class Instagram extends Video {


	function __construct($url) {
		$this->videoUrl = $url;		
		$this->videoTitle = "videoFromInstagram";	
		$this->getVideoData();
	}

	function getVideoData() {
		$fullpage = curlGet($this->videoUrl);		
		$dom = new DOMDocument();
		@$dom->loadHTML($fullpage);
		$xpath = new DOMXPath($dom); 
		//parsam linku la stream
		$tags = $xpath->query('//meta[@property="og:video"]');
		$url = $tags->item(0)->attributes->item(1)->value . "";		
		$size = get_size($url);	
		//parsam mime
		$tags = $xpath->query('//meta[@property="og:video:type"]');
		$this->mime = $tags->item(0)->attributes->item(1)->value;
		//parsam titlu
		$tags = $xpath->query('//meta[@property="og:title"]');
		$this->videoTitle = trim($tags->item(0)->attributes->item(1)->value);
		//parsam imaginea
		$tags = $xpath->query('//meta[@property="og:image"]');
		$this->image = $tags->item(0)->attributes->item(1)->value;

		$tags = $xpath->query('//meta[@property="og:description"]');
		$this->description = $tags->item(0)->attributes->item(1)->value; 

		$jsondata = json_decode(curlGet("https://api.instagram.com/oembed/?url=".$this->videoUrl));
		$this->uploader = $jsondata->author_name;
		//printout($this->uploader);
			
		$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime='.$this->mime.'&token='.($url);	
		$this->streamUrl = array("Norm" => array("url" => $msg,
					"size" => $size,
					"type" => "video/mp4"));

		
	}


	function getTitle() {
		return $this->videoTitle;
	}

	function getVideoUrl() {
		return $this->videoUrl;
	}

	function getStream() {
		return $this->streamUrl;
	}

	function getImage() {
		return $this->image;
	}

	function getSize() {
		return $this->size;
	}

	function getMime() {
		return $this->mime;
	}
}


?>