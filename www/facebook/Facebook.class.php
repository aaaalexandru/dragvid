<?php
include "inc/simple_html_dom.php";

class Facebook  extends Video {
	


	function __construct($url) {
		$this->videoUrl = $url;
		$this->videoTitle = "videoFromFacebook";
		$this->getVideoData();
		
	
	}

	function getVideoData() {
		$fullpage = $this->getPage($this->videoUrl);
		$html = str_get_html($fullpage);
		//parsam scripturile
		$scripts = $html->find("script");
		//parsam titlu
		if (isset($html->find("span.hasCaption",0)->innertext)) { 
			$this->videoTitle = str_replace("<br />", "", $html->find("span.hasCaption",0)->innertext);
		}
		$this->image = $html->find("img[class=_s0 _54ru img]",0)->src;
		$this->image = str_replace("&amp;", "&", $this->image);
		$this->description = trim($html->find("span[class=hasCaption]",0)->innertext);
		$this->description = str_replace("<br >", "", $this->description);
		$this->description = str_replace("<br />", "", $this->description);
		$this->uploader = trim($html->find("div[id=fbPhotoPageAuthorName] a",0)->innertext);
		$this->uploaded = trim($html->find("abbr",0)->innertext);
		$i=0;
		foreach ($scripts as $value) {
			if(strpos($value, "JSCC.init")) {
				break;
			}
			$i++;
		}
		$jscode = strstr($scripts[$i]->innertext,"});");
		$jscode = str_replace("}); ", "", $jscode);
		$firstArray = strstr($jscode, ".forEach", true);
		$json = json_decode($firstArray);		
		$arr = (array)json_decode(urldecode($json[0][1]))->video_data[0];

		if (isset($arr['hd_src'])) {
			$size = get_size($arr['hd_src']);
			$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/mp4&token='.($arr['hd_src']);
			$this->streamUrl['HD'] = array("url" => $msg,
											"type" => "video/mp4",
											"size" => $size);
		}
		if (isset($arr['sd_src'])) {
					$size = get_size($arr['sd_src']);
					$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime=video/mp4&token='.($arr['sd_src']);
					$this->streamUrl['SD'] = array("url" => $msg,
													"type" => "video/mp4",
													"size" => $size);
		}
		

	}

	function getPage() {
		$curl = curl_init();
	    $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
		$header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
		$header[] = "Cache-Control: max-age=0";
		$header[] = "Connection: keep-alive";
		$header[] = "Keep-Alive: 300";
		$header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
		$header[] = "Accept-Language: en-us,en;q=0.5";
		$header[] = "Pragma: "; // browsers keep this blank.	 
		curl_setopt($curl, CURLOPT_URL, $this->videoUrl);
		curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla');
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($curl, CURLOPT_REFERER, '');
		curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
		curl_setopt($curl, CURLOPT_AUTOREFERER, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);	
		$data = curl_exec($curl); // execute the curl command
		curl_close($curl); // close the connection
		return $data;
	}

	function getTitle() {
		return $this->videoTitle;
	}

	function getVideoUrl() {
		return $this->videoUrl;
	}

	function getStream() {
		return $this->streamUrl;
	}

	function getImage() {
		return $this->image;
	}

}

?>