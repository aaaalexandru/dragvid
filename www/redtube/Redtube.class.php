<?php

class Redtube extends Video {
	
	function __construct($url) {
		$this->videoUrl = $url;
		$this->getVideoData();
	}

	function getVideoData() {
		$fullpage = curlGet($this->videoUrl);
		
		$dom = new DOMDocument();		
		@$dom->loadHTML($fullpage);
		$xpath = new DOMXPath($dom); 

		$tags = $xpath->query('//h1[@class="videoTitle"]');
		$this->videoTitle = $tags->item(0)->nodeValue;

		$tags = $xpath->query('//meta[@property="og:image"]');
		$this->image = $tags->item(0)->getAttribute("content");

		$tags = $xpath->query('//table/tr');
		$this->seen = $tags->item(1)->childNodes->item(2)->childNodes->item(0)->nodeValue;
		$this->uploaded = trim(str_replace("ADDED", "", $tags->item(1)->childNodes->item(2)->childNodes->item(1)->nodeValue ));		
		$this->uploader = trim($tags->item(0)->childNodes->item(2)->nodeValue);

		$tags = $xpath->query('//param[@name="FlashVars"]');
		parse_str($tags->item(0)->getAttribute("value"), $prs);

		if (isset($prs['quality_480p'])) {
			$url = $prs['quality_480p'];
			$size = get_size($url);
			$type = "video/flv";
			$this->streamUrl['480p'] = array("url" => $url,
											 "size" => $size,
											 "type" => $type);
		}

		if (isset($prs['quality_720p'])) {
			$url = $prs['quality_720p'];
			$size = get_size($url);				
			$type = "video/mp4";
			$msg = '../download.php?title='.base64_encode($this->videoTitle).'&size='.$size.'&mime='.$type.'&token='.urlencode($url);
			$this->streamUrl['720p'] = array("url" => $msg,
											 "size" => $size,
											 "type" => $type);
		}

		
	}

}